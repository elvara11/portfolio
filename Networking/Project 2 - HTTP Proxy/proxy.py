import socket
import re # TODO: Can we use regex library?
import threading

RECV_BUF_SIZE = 2048

def entry_point(sock, addr):
    # print('Client socket connected: {}'.format(addr))
    header = b''
    while header.find(b'\r\n\r\n') == -1 and header.find(b'\n\n') == -1:
        chunk = sock.recv(RECV_BUF_SIZE)
        if chunk == b'':
            # print('Client socket disconnected: {}', addr)
            sock.close()
            return
        
        #print('{}'.format(chunk))
        header += chunk

    lines = header.splitlines()

    host = None
    port = None
    get_url, sep, protocol = lines[0].rpartition(b' ')
    new_request_line = get_url + sep + b'HTTP/1.0'
    get, sep, url = get_url.strip().rpartition(b' ')
    url = url.strip()
    
    if url.startswith(b'http://'):
        port = 80
    elif url.startswith(b'https://'):
        port = 443
    # else:
    #     print (url)
    #     print("Unsupported protocol") # We do not need to handle this case!
    #     return

    url_string = url.decode()
    match = re.search(r":(\d+)", url_string)
    if match != None:
        port = int(match.group(1))

    for line in lines:
        # print(line)
        # Case-insensitively recognize Host line in the header
        if line.lower().startswith(b'host:'):
            before_sep, sep, host_port = line.rpartition(b' ')
            host_port = host_port.strip()
            # TODO: Do we need to support IPv6?
            if host_port.find(b':') != -1:
                host, sep, port = host_port.rpartition(b':')
                port = int(port.decode())
            else:
                host = host_port

    if host == None or port == None:
        # We can't continue
        # print("Insufficient information.")
        sock.close()
        return

    print('>>> {}'.format(lines[0].decode()))

    # print('Host: {}'.format(host))
    # print('Port: {}'.format(port))

    # Make TCP Connection
    sock_TCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock_TCP.connect((host.decode(), port))
    except:
        message = b'HTTP/1.0 502 Bad Gateway\r\n\r\n'
        sock.sendall(message)
        sock.close()
        return

    if get.strip().lower() != b'connect':
        message = new_request_line + b'\r\n'
        for line in lines[1:]:
            if line.find(b'Connection:') != -1:
                message += b'Connection: close'
            elif line.find(b'Proxy-connection:') != -1:
                message += b'Proxy-connection: close'
            else:
                message += line

            # Lines of the header are terminated by CRLF
            message += b'\r\n'

        sock_TCP.sendall(message)
    else: # Connect request
        message = b'HTTP/1.0 200 OK\r\n\r\n'
        sock.sendall(message)

    t = threading.Thread(target=unidirectional_pipe, args=[sock, sock_TCP])
    t.start()
    unidirectional_pipe(sock_TCP, sock)

def unidirectional_pipe(sock1, sock2):
    while True:
        try:
            chunk = sock1.recv(RECV_BUF_SIZE)
            if chunk == b'':
                # print('Socket disconnected')
                sock1.close()
                sock2.close()
                return
            sock2.sendall(chunk)
        except:
            return

