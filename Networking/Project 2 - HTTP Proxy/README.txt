Project Members:
Adam Anderson (adamand2)
Ramya Ravichandran Asha (ramya5)

Instructions:
Run `$ ./run <port>` where port is the port number the 
proxy's server socket should bind to. Use python 3.6.8,
or anything newer should probably work too.

Execution is terminated by end-of-file on stdin or by ctrl-C.