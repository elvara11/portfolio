import socket
import threading
import sys
import os

import proxy

def check_eof():
    for line in sys.stdin:
        # Do nothing
        pass

    # End of file
    os._exit(0)

def check_interrupt_proxy(sock_client, client_address):
    try:
        proxy.entry_point(sock_client, client_address)
    except KeyboardInterrupt:
        os._exit(0)

HOST_NAME = ''

port_str = sys.argv[1]
port_num = int(port_str)

sock_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST_NAME, port_num)
sock_server.bind(server_address)
sock_server.listen(1)

eof_thread = threading.Thread(target=check_eof, args=[])
eof_thread.start()

try:
    while True:
        sock_client, client_address = sock_server.accept()
        t = threading.Thread(target=check_interrupt_proxy, args=[sock_client, client_address])
        t.start()
except KeyboardInterrupt:
    os._exit(0)

