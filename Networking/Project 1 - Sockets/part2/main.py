import socket
import sys
import struct
import math
import threading
import random

"""
 0               1               2               3
 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          payload_len                          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                            psecret                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|              step             |   last 3 digits of student #  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
"""

HEADER_SIZE = 12

STUDENT_NUM = 805
STEP_NUM = 2
SERVER_ADDRESS = 'attu2.cs.washington.edu' #'localhost'
# SERVER_ADDRESS = 'localhost'
SERVER_PORT = 12235

A_HEADER_FMT = '!iihh'
A_RESP_FMT = '!iiii'

MESSAGE_A = b'hello world\0'

# Header and marshalling helper functions
def four_byte_align(payload_length):
    return math.ceil(float(payload_length) / 4) * 4

def add_header(payload, psecret):
    payload_aligned = payload.ljust(four_byte_align(len(payload)))
    header = struct.pack(A_HEADER_FMT, len(payload), psecret, STEP_NUM, STUDENT_NUM)
    return header + payload_aligned

def check_and_strip_packet(data, expected_psecret, expected_payload_len):
    expected_data_length = four_byte_align(expected_payload_len + HEADER_SIZE)
    if len(data) != expected_data_length:
        return None
    unpacked_header = struct.unpack(A_HEADER_FMT, data[:HEADER_SIZE]) 
    actual_payload_len = unpacked_header[0]
    actual_psecret = unpacked_header[1]
    if actual_payload_len != expected_payload_len:
        return None
    if actual_psecret != expected_psecret:
        return None
    # Removes header and trailing padding to get payload
    return data[HEADER_SIZE : expected_payload_len + HEADER_SIZE]

def generate_secret():
    return random.randint(0, 255)

sock_a = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock_a.bind((SERVER_ADDRESS, SERVER_PORT))

B_ID_FMT = '!i'
B_ACK_FMT = '!i'
B_RESP_FMT = '!ii'

C_RESP_FMT = '!iiibxxx'

D_RESP_FMT = '!i'

def timeout(sock):
    sock.close()
    print('Client failed protocol or progress')

def stage_d(client_sock, num_c, len_c, secret_c, c_c):
    timer = threading.Timer(3.0, timeout, args=[client_sock])
    timer.start()

    for i in range(num_c):
        try:
            data_d = client_sock.recv(four_byte_align(len_c) + HEADER_SIZE)
        except:
            client_sock.close()
            return

        timer.cancel()
        timer = threading.Timer(3.0, timeout, args=[client_sock])
        timer.start()

        payload_d = check_and_strip_packet(data_d, secret_c, len_c)

        if payload_d != c_c * len_c:
            # This includes the None case
            client_sock.close()
            return
    
    timer.cancel()
    secret_d = generate_secret()

    print('Secret D: ', secret_d)

    resp_d = add_header(struct.pack(D_RESP_FMT, secret_d), secret_c)
    client_sock.sendall(resp_d)
    client_sock.close()
    

def stage_c(tcp_sock_server, secret_b):
    timer = threading.Timer(3.0, timeout, args=[tcp_sock_server])
    timer.start()

    # Listen for client connection
    tcp_sock_server.listen()
    try:
        (client_sock, client_addr) = tcp_sock_server.accept()
        tcp_sock_server.close()
    except:
        # Client timed out, timer closed connection while we were listening
        return

    # Cancel timer
    timer.cancel()

    num_c = random.randint(1, 50)
    len_c = random.randint(1, 50)
    secret_c = generate_secret()
    c_c = random.randint(97, 122)

    print('Secret C: ', secret_c)

    resp_c = add_header(struct.pack(C_RESP_FMT, num_c, len_c, secret_c, c_c), secret_b)
    client_sock.sendall(resp_c)
    stage_d(client_sock, num_c, len_c, secret_c, c_c.to_bytes(1, 'little'))

def stage_b(sock_client, num_a, length_a, secret_a):
    timer = threading.Timer(3.0, timeout, args=[sock_client])
    timer.start()

    # Listen for clients packets, and send acks
    # After we recieve all of the packets, set up new tcp socket

    # 0   0 - 1
    #   -       1 0
    # NOTE: No real guarantees that things arrive in order just because they were sent in order
    # This is just what we were told to assume in the spec
    id_size = struct.calcsize(B_ID_FMT)
    
    last_acked = -1
    client_address = None
    while last_acked < num_a - 1:
        try:
            (data_b, address_b) = sock_client.recvfrom(id_size + four_byte_align(length_a) + HEADER_SIZE)
        except:
            sock_client.close()
            return

        # Reset the timer
        timer.cancel()
        timer = threading.Timer(3.0, timeout, args=[sock_client])
        timer.start()

        client_address = address_b
        payload_b = check_and_strip_packet(data_b, secret_a, length_a + id_size)
        if payload_b == None:
            sock_client.close()
            return
        
        (packet_id,) = struct.unpack(B_ID_FMT, payload_b[:id_size])

        if packet_id != last_acked and packet_id != last_acked + 1:
            # this is illegal!
            sock_client.close()
            return
        
        if payload_b[id_size:] != bytes(length_a):
            sock_client.close()
            return

        # This is an arbitrary random chance of packet loss (50% loss rate)
        if random.randint(0, 1) == 0:
            sock_client.sendto(add_header(struct.pack(B_ACK_FMT, packet_id), secret_a), address_b)
            last_acked = packet_id
    
    # Set up the new socket and reply with the secret
    tcp_server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server_sock.bind((SERVER_ADDRESS, 0))
    tcp_server_port = tcp_server_sock.getsockname()[1]
    secret_b = generate_secret()

    print('Secret B: ', secret_b)

    resp_b = add_header(struct.pack(B_RESP_FMT, tcp_server_port, secret_b), secret_a)
    sock_client.sendto(resp_b, client_address)
    timer.cancel()
    sock_client.close()
    stage_c(tcp_server_sock, secret_b)


while True:
    try:
        (data_a, address_a) = sock_a.recvfrom(struct.calcsize(A_RESP_FMT) + HEADER_SIZE)
    except:
        continue
    
    payload_a = check_and_strip_packet(data_a, 0, len(MESSAGE_A))
    if payload_a != MESSAGE_A:
        continue
    
    sock_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock_client.bind((SERVER_ADDRESS, 0))
    udp_port_a = sock_client.getsockname()[1]
    num_a = random.randint(1, 50)
    length_a = random.randint(1, 50)
    secret_a = generate_secret()

    print('Secret A: ', secret_a)

    resp_a = struct.pack(A_RESP_FMT, num_a, length_a, udp_port_a, secret_a)
    sock_a.sendto(add_header(resp_a, 0), address_a)
    
    t = threading.Thread(target=stage_b, args=[sock_client, num_a, length_a, secret_a])
    t.start()
