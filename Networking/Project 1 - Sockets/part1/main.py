import socket
import sys
import struct
import math
import threading

"""
------------------------- HEADER FORMAT -------------------------

 0               1               2               3
 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          payload_len                          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                            psecret                            |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|              step             |   last 3 digits of student #  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
"""

HEADER_SIZE = 12

STUDENT_NUM = 805
STEP_NUM = 1
SERVER_ADDRESS = 'attu2.cs.washington.edu'
# SERVER_ADDRESS = 'localhost'
SERVER_PORT = 12235

def four_byte_align(payload_length):
    return math.ceil(float(payload_length) / 4) * 4

def add_header(payload, psecret):
    payload_aligned = payload.ljust(four_byte_align(len(payload)), b'0')
    header = struct.pack('!iihh', len(payload), psecret, STEP_NUM, STUDENT_NUM)
    return header + payload_aligned

def strip_header(data):
    return data[HEADER_SIZE:]

### STAGE A ###

sock_a = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

MESSAGE_A = b'hello world\0'
A_RESP_FMT = '!iiii'

data_a = add_header(MESSAGE_A, 0)
sock_a.sendto(data_a, (SERVER_ADDRESS, SERVER_PORT))

resp_a = strip_header(sock_a.recv(struct.calcsize(A_RESP_FMT) + HEADER_SIZE))

unpacked_resp_a = struct.unpack(A_RESP_FMT, resp_a)

num_b = unpacked_resp_a[0]
len_b = unpacked_resp_a[1]
udp_port_b = unpacked_resp_a[2]
secret_a = unpacked_resp_a[3]

print("SECRET A: ", secret_a)

### STAGE B ###

B_TIMEOUT_SECS = 0.5
B_ACK_FMT = '!i'
B_RESP_FMT = '!ii'

sock_b = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

timer = None

def send_b(packet_id):
    global timer
    id_head = struct.pack('!i', packet_id)
    msg = id_head + bytes(len_b)
    full_msg = add_header(msg, secret_a)
    sock_b.sendto(full_msg, (SERVER_ADDRESS, udp_port_b))
    timer = threading.Timer(B_TIMEOUT_SECS, send_b, args=[packet_id])
    timer.start()


for packet_id in range(num_b):
    send_b(packet_id)
    ack = strip_header(sock_b.recv(struct.calcsize(B_ACK_FMT) + HEADER_SIZE))
    packet_id = struct.unpack(B_ACK_FMT, ack)
    timer.cancel()

resp_b = strip_header(sock_b.recv(struct.calcsize(B_RESP_FMT) + HEADER_SIZE))
(tcp_port_c, secret_b) = struct.unpack(B_RESP_FMT, resp_b)

print('SECRET B: ', secret_b)

### STAGE C ###

sock_c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sock_c.connect((SERVER_ADDRESS, tcp_port_c))

C_RESP_FMT = '!iiicxxx'
resp_c = strip_header(sock_c.recv(struct.calcsize(C_RESP_FMT) + HEADER_SIZE))

unpacked_resp_c = struct.unpack(C_RESP_FMT, resp_c)

num_c = unpacked_resp_c[0]
len_c = unpacked_resp_c[1]
secret_c = unpacked_resp_c[2]
c_c = unpacked_resp_c[3]

print('SECRET C: ', secret_c)

### STAGE D ###

payload_d = c_c * len_c

D_RESP_FMT = '!i'

for i in range(num_c):
    sock_c.sendall(add_header(payload_d, secret_c))
    
resp_d = strip_header(sock_c.recv(struct.calcsize(D_RESP_FMT) + HEADER_SIZE))

(secret_d,) = struct.unpack(D_RESP_FMT, resp_d)

sock_c.close()

print('SECRET D: ', secret_d)
