## Names:
- Adam Anderson (adamand2)
- Ramya Ravichandran Asha (ramya5)
- Frank Poon (fpoon)

## Instructions:
- Use python version 3.6.8
- Run the client by running `python3 main.py`
- Change the server address using the `SERVER_ADDRESS` constant on line 23
  - NOTE: If running on attu (and maybe also other machines), the address you put for
    the server on both ends must be an external name (i.e. DNS name or ip address, not localhost)
- Extracted secrets are printed to stdout

## Server secrets:
- A: 63
- B: 17
- C: 32
- D: 49
