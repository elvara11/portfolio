'''
    Template for polynomial regression
    AUTHOR Eric Eaton, Xiaoxiang Hu
'''

import numpy as np
import scipy as sp

#-----------------------------------------------------------------
#  Class PolynomialRegression
#-----------------------------------------------------------------

class PolynomialRegression:

    def __init__(self, degree=1, reg_lambda=1E-8):
        """
        Constructor
        """
        self.regLambda = reg_lambda
        self.theta = None
        self.degree = degree
        self.means = None
        self.stds = None

    def polyfeatures(self, X, degree):
        """
        Expands the given X into an n * d array of polynomial features of
            degree d.

        Returns:
            A n-by-d numpy array, with each row comprising of
            X, X * X, X ** 3, ... up to the dth power of X.
            Note that the returned matrix will not include the zero-th power.

        Arguments:
            X is an n-by-1 column numpy array
            degree is a positive integer
        """
        n = len(X)
        feat = np.ndarray((n, degree))
        for i in range(n):
            for j in range(degree):
                feat[i, j] = X[i, 0] ** (j + 1)
        return feat

    def standardize(self, X):
        scaledX = np.ndarray(X.shape)
        for i in range(X.shape[0]):
            for j in range(X.shape[1]):
                scaledX[i, j] = (X[i, j] - self.means[j]) / self.stds[j]
        return scaledX

    def fit(self, X, y):
        """
            Trains the model
            Arguments:
                X is a n-by-1 array
                y is an n-by-1 array
            Returns:
                No return value
            Note:
                You need to apply polynomial expansion and scaling
                at first
        """
        polyX = self.polyfeatures(X, self.degree)

        self.means = polyX.mean(0)
        self.stds = polyX.std(0)
        scaledX = self.standardize(polyX)

        n = len(X)

        # add 1s column
        scaledX_ = np.c_[np.ones([n, 1]), scaledX]

        n, d = scaledX_.shape
        d = d-1  # remove 1 for the extra column of ones we added to get the original num features

        # construct reg matrix
        reg_matrix = self.regLambda * np.eye(d + 1)
        reg_matrix[0, 0] = 0

        # analytical solution (X'X + regMatrix)^-1 X' y
        self.theta, _, _, _ = np.linalg.lstsq(scaledX_.T.dot(scaledX_) + reg_matrix, scaledX_.T.dot(y), rcond=None)

    def predict(self, X):
        """
        Use the trained model to predict values for each instance in X
        Arguments:
            X is a n-by-1 numpy array
        Returns:
            an n-by-1 numpy array of the predictions
        """
        n = len(X)

        polyX = self.polyfeatures(X, self.degree)
        scaledX = self.standardize(polyX)
        scaledX_ = np.c_[np.ones([n, 1]), scaledX]
        y = scaledX_.dot(self.theta)
        return y


#-----------------------------------------------------------------
#  End of Class PolynomialRegression
#-----------------------------------------------------------------



def learningCurve(Xtrain, Ytrain, Xtest, Ytest, reg_lambda, degree):
    """
    Compute learning curve

    Arguments:
        Xtrain -- Training X, n-by-1 matrix
        Ytrain -- Training y, n-by-1 matrix
        Xtest -- Testing X, m-by-1 matrix
        Ytest -- Testing Y, m-by-1 matrix
        regLambda -- regularization factor
        degree -- polynomial degree

    Returns:
        errorTrain -- errorTrain[i] is the training accuracy using
        model trained by Xtrain[0:(i+1)]
        errorTest -- errorTrain[i] is the testing accuracy using
        model trained by Xtrain[0:(i+1)]

    Note:
        errorTrain[0:1] and errorTest[0:1] won't actually matter, since we start displaying the learning curve at n = 2 (or higher)
    """

    n = len(Xtrain)

    errorTrain = np.zeros(n)
    errorTest = np.zeros(n)

    #TODO -- complete rest of method; errorTrain and errorTest are already the correct shape
    reg = PolynomialRegression(degree, reg_lambda)
    
    for i in range(1, n):
        reg.fit(Xtrain[:i + 1], Ytrain[:i + 1])

        trainPred = reg.predict(Xtrain[:i + 1])
        errorTrain[i] = ((trainPred - Ytrain[:i + 1]) ** 2).sum() / (i + 1)

        testPred = reg.predict(Xtest)
        errorTest[i] = ((testPred - Ytest) ** 2).sum() / len(Xtest)

    return errorTrain, errorTest
