from mnist import MNIST
import numpy as np

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

""" X : R^n x d
    Y : (0, 1)^n x k
    lambda > 0

    Solve (X^T X + lambda I)W = X^T Y for W

    return W
"""
def train(X, Y, reg_lambda):
    _, d = X.shape

    reg_matrix = reg_lambda * np.eye(d)
    W = np.linalg.solve(X.T.dot(X) + reg_matrix, X.T.dot(Y))
    return W

""" W : R^d x k
    X : R^m x d

    f(z) = argmax_j e_j^T W^T x_i
    Return m-length vector
"""
def predict(W, X):
    inter = W.T.dot(X.T)
    return inter.argmax(0).T

""" Y : R^n
    Convert to one hot in R^n x k
"""
def one_hot(Y):
    n = len(Y)
    oh = np.zeros((n, 10))
    for i in range(n):
        oh[i, Y[i]] = 1
    return oh

""" labels_pred : R^n
    labels_actual : R^n
    Return the number of labels where the prediction was wrong
"""
def error(labels_pred, labels_actual):
    n = len(labels_pred)
    sum = 0
    for i in range(n):
        if labels_pred[i] != labels_actual[i]:
            sum += 1
    return sum / n

X_train, labels_train, X_test, labels_test = load_dataset()

OH_train = one_hot(labels_train)

W = train(X_train, OH_train, 10E-4)

pred_train = predict(W, X_train)
pred_test = predict(W, X_test)

print("Training error: ", error(pred_train, labels_train))
print("Testing error: ", error(pred_test, labels_test))
