import torch
import torchvision
import torchvision.transforms as transforms
import math
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batch_size = 1000

trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size,
                                         shuffle=True)#, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

d1 = 3
d2 = 32
d3 = 32

c = 10

class Net(nn.Module):
    def __init__(self, M1, k, M2, N, h):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(d1, M1, k)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(M1, M2, k)
        pool_k = 10 - k
        self.flat = M2 * pool_k * pool_k
        # print(M2, k, pool_k)
        self.fc1 = nn.Linear(self.flat, N)
        self.fc2 = nn.Linear(N, h)
        self.fc3 = nn.Linear(h, c)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        n, M2, k, _ = x.shape
        #x = x.view(-1, self.flat)
        # print(x.shape, self.flat)
        x = x.reshape((n, self.flat))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def imshow(img1, img2, i):
    img = torch.cat((img1, img2), 1)
    img = img / 2 + 0.5     # unnormalize
    npimg = img.detach().cpu().numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    # plt.show()
    plt.savefig('out/part_e_{}.png'.format(i))
    plt.close()

M1 = 7
k = 5
M2 = 15
N = 117
h = 72

PATH = './cifar_net_d_main.pth'

net = Net(M1, k, M2, N, h)
net.load_state_dict(torch.load(PATH))
ins = []
outs = []

num_ins = 4

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

net.to(device)

for i, data in enumerate(trainloader, 0):
    inputs, labels = data
    inputs = inputs.to(device)
    labels = labels.to(device)
    outputs = net(inputs)
    correct_indices = outputs.argmax(1) == labels
    correct_ins = inputs[correct_indices]
    print(correct_ins[0].shape)
    correct_outs = labels[correct_indices]
    if len(correct_indices) >= num_ins:
        for j in range(num_ins):
            ins.append(correct_ins[j])
            outs.append(correct_outs[j])
        break
    else:
        for j in range(len(correct_indices)):
            ins.append(correct_ins[j])
            outs.append(correct_outs[j])

eps = 0.01

loss_fn = nn.CrossEntropyLoss()
def fgsm(input, label):
    # input = Variable(input, requires_grad=True)
    input.requires_grad = True
    input = input.reshape(1, 3, 32, 32).to(device)
    input.retain_grad()
    output = net(input)
    
    label_tensor = label.reshape((1,)).to(device)
    loss = loss_fn(output, label_tensor)
    loss.backward(retain_graph=True)
    # print(input.requires_grad, input.grad)
    new_in = input + eps * torch.sign(input.grad)
    new_out = net(new_in)
    return new_in.reshape(3, 32, 32), new_out.argmax(1)

# tensor_ins = torch.tensor(ins)
# tensor_outs = torch.tensor(outs)

for i in range(len(ins)):
    input_img = ins[i]
    label = outs[i]
    # print(i, label.item())
    # imshow(input_img)
    new_img, out_label = fgsm(input_img, label)
    imshow(input_img, new_img, i)
    print(i, label.item(), out_label.item())
