import torch
import torchvision
import torchvision.transforms as transforms
import math
import torch.optim as optim
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

batch_size = 500
validate_prop = 0.1

trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)

len_validate = int(len(trainset) * validate_prop)
len_train = int(len(trainset) - len_validate)

trainsubset, validateset = torch.utils.data.random_split(trainset, [len_train, len_validate])

trainloader = torch.utils.data.DataLoader(trainsubset, batch_size=batch_size,
                                          shuffle=True)#, num_workers=2)

validateloader = torch.utils.data.DataLoader(validateset, batch_size=batch_size,
                                          shuffle=True)#, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size,
                                         shuffle=False)#, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

def imshow(img):
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

part = 'd'
vectorize = part == 'a' or part == 'b'

def input_map(inputs):
    if vectorize:
        return inputs.flatten(1)
    else:
        return inputs

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

d1 = 3
d2 = 32
d3 = 32

c = 10

hyper_num_configs = 5

def hyper_default(part):
    if part == 'a':
        # []
        return []
    elif part == 'b':
        # [h]
        return [300]
    elif part == 'c':
        # [M, k, N]
        [100, 5, 14]
    else:
        # []
        [6, 5, 16, 120, 84]

def search_ranges(part):
    if part == 'a':
        # [lr, moment]
        return []
    elif part == 'b':
        # [lr, moment, h]
        return [[100, 300]]
    elif part == 'c':
        # [lr, moment, M, k, N]
        return [[50, 100], [4, 3], [10, 10]]
    else:
        # lr, moment, convolutional filters, filter sizes, dimensionality of the fully-connected layers, stepsize
        return [[6, 2], [5, 2], [14, 4], [100, 40], [70, 30]]

class Net(nn.Module):
    def __init__(self, M1, k, M2, N, h):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(d1, M1, k)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(M1, M2, k)
        pool_k = 10 - k
        self.flat = M2 * pool_k * pool_k
        # print(M2, k, pool_k)
        self.fc1 = nn.Linear(self.flat, N)
        self.fc2 = nn.Linear(N, h)
        self.fc3 = nn.Linear(h, c)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        n, M2, k, _ = x.shape
        #x = x.view(-1, self.flat)
        # print(x.shape, self.flat)
        x = x.reshape((n, self.flat))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

def make_net(part, hyperparams):
    #hyperparams = hypers[2:]
    if part == 'a':
        return nn.Sequential(
            nn.Linear(d1 * d2 * d3, c)
        )
    elif part == 'b':
        return nn.Sequential(
            nn.Linear(d1 * d2 * d3, int(hyperparams[0].item())),
            nn.ReLU(),
            nn.Linear(int(hyperparams[0].item()), c)
        )
    elif part == 'c':
        M = int(hyperparams[0].item())
        k = int(hyperparams[1].item())
        N = int(hyperparams[2].item())
        return nn.Sequential(
            nn.Conv2d(d1, M, k, stride=1),
            nn.ReLU(),
            nn.MaxPool2d((N, N)),
            nn.Flatten(1),
            nn.Linear(M * math.floor((33 - k) / N) ** 2, c)
        )
    else:
        # number of convolutional filters, filter sizes, dimensionality of the fully-connected layers, stepsize
        # 6, 5, 16, 120, 84
        # M1, k, M2, N, h
        M1 = int(hyperparams[0].item())
        k = int(hyperparams[1].item())
        M2 = int(hyperparams[2].item())
        N = int(hyperparams[3].item())
        h = int(hyperparams[4].item())
        return Net(M1, k, M2, N, h)

num_epochs = 25

def train_and_eval(part, hyperparams):
    net = make_net(part, hyperparams)
    net.to(device)

    #lr = hyperparams[0].item()
    #momentum = hyperparams[1].item()
    criterion = nn.CrossEntropyLoss()
    #optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
    optimizer = optim.Adam(net.parameters(), lr=0.001)

    t_accs = []#torch.zeros(num_epochs)
    v_accs = []#torch.zeros(num_epochs)
    epoch = 0
    #for epoch in range(num_epochs):
    training_acc = 0.0
    validate_acc = 0.0
    while (part != 'd' and epoch < num_epochs) or (part == 'd' and training_acc < 0.87):
        # print('Epoch', epoch)
        training_acc = 0.0
        validate_acc = 0.0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()
            mapped = input_map(inputs).to(device)
            outputs = net(mapped)
            outputs = outputs.to(device)
            training_acc += (outputs.argmax(1) == labels).sum().item() / len_train
            loss = criterion(outputs, labels)
            loss = loss.to(device)
            loss.backward()
            optimizer.step()
        for i, data in enumerate(validateloader, 0):
            inputs, labels = data
            inputs = inputs.to(device)
            labels = labels.to(device)
            mapped = input_map(inputs).to(device)
            outputs = net(mapped)
            outputs = outputs.to(device)
            validate_acc += (outputs.argmax(1) == labels).sum().item() / len_validate
        t_accs.append(training_acc)
        v_accs.append(validate_acc)
        print('Part {} epoch {} t_acc: {} v_acc: {}'.format(part, epoch, training_acc, validate_acc))
        epoch = epoch + 1
    v_accs_t = torch.tensor(v_accs)
    t_accs_t = torch.tensor(t_accs)
    return net, v_accs_t, t_accs_t

def plot_accs(v_accs, t_accs, path):
    n, = v_accs.shape
    plt.figure()
    plt.title('Training and Validation Accuracy by Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Classification accuracy')
    plt.plot(np.array(range(n)), v_accs, label='Validation Accuracy')
    plt.plot(np.array(range(n)), t_accs, label='Training Accuracy')
    plt.legend()
    plt.savefig(path)
    plt.close()

def hyper_search(part, search_ranges, num_picks):
    if len(search_ranges) == 0:
        hypers = torch.zeros(0)
        net, v_accs, t_accs = train_and_eval(part, hypers)
        n, = v_accs.shape
        plot_accs(v_accs, t_accs, './out/part_{}.png'.format(part))
        torch.save(net.state_dict(), './cifar_net_{}.pth'.format(part))
        return v_accs[n - 1], hypers, net
    best_v_acc = 0.0
    best_net = None
    best_hypers = None
    best_i = -1
    for i in range(num_picks):
        hypers = torch.zeros(len(search_ranges))
        for j in range(len(search_ranges)):
            hypers[j] = torch.rand(1) * search_ranges[j][1] + search_ranges[j][0]
        net, v_accs, t_accs = train_and_eval(part, hypers)
        print('Part {} i={} hypers={}'.format(part, i, hypers))
        plot_accs(v_accs, t_accs, './out/part_{}_{}.png'.format(part, i))
        n, = v_accs.shape
        if v_accs[n - 1] > best_v_acc:
            best_v_acc = v_accs[n - 1]
            best_net = net
            best_hypers = hypers
            best_i = i
        torch.save(net.state_dict(), './cifar_net_{}_{}.pth'.format(part, i))
    return best_v_acc, best_hypers, best_net
    
    # for i in range(subdivs ** len(search_ranges)):
    #     hypers = torch.zeros(len(search_ranges))
    #     for j in range(len(search_ranges)):
    #         hypers[j] = search_ranges[j][0] + (i // (subdivs ** j)) * (search_ranges[j][1])
    #     net, v_accs, t_accs = train_and_eval(part, hypers)
    #     plot_accs(v_accs, t_accs, './out/part_{}_{}.png'.format(part, i))
    #     if v_accs[num_epochs - 1] > best_v_acc:
    #         best_v_acc = v_accs[num_epochs - 1]
    #         best_net = net
    #         best_hypers = hypers
    #         best_i = i
    # torch.save(net.state_dict(), './cifar_net_{}_{}.pth'.format(part, best_i))
    # return best_v_acc, best_hypers, best_net

train = True
net = None
if train:
    ranges = search_ranges(part)
    best_v_acc, best_hypers, net = hyper_search(part, ranges, hyper_num_configs)
    print('Best validation accuracy:', best_v_acc)
    print('Best hyperparameters:', best_hypers)

# net = make_net(part)
# net.load_state_dict(torch.load(PATH))

correct = 0
total = 0
with torch.no_grad():
    for data in testloader:
        images, labels = data
        outputs = net(input_map(images).to(device))
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted.to(device) == labels.to(device)).sum().item()

print('Accuracy of the network on the 10000 test images: %d %%' % (
    100 * correct / total))

class_correct = list(0. for i in range(10))
class_total = list(0. for i in range(10))
with torch.no_grad():
    for data in testloader:
        images, labels = data
        outputs = net(input_map(images).to(device))
        _, predicted = torch.max(outputs, 1)
        c = (predicted.to(device) == labels.to(device)).squeeze()
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i].item()
            class_total[label] += 1

for i in range(10):
    print('Accuracy of %5s : %2d %%' % (
        classes[i], 100 * class_correct[i] / class_total[i]))
