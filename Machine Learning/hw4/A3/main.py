import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
import torchvision.models as models
import torchvision

part_a = True

model = torchvision.models.alexnet(pretrained=True)
if part_a:
    for param in model.parameters():
        param.requires_grad = False
else:
    for param in model.parameters():
        param.requires_grad = True
model.classifier[6] = nn.Linear(4096, 10)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

model.to(device)

transform = torchvision.transforms.Compose([
    torchvision.transforms.Resize((128, 128)),
    torchvision.transforms.ToTensor(),
    torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])

batch_size = 1000
validate_prop = 0.1

data = torchvision.datasets.CIFAR10('data/', train=True, download=True, transform=transform)

num_batches = len(data) // batch_size
len_train = int(len(data) * (1 - validate_prop))
len_validate = int(len(data) * validate_prop)
# num_batches_train = int(float(num_batches) * (1 - validate_prop))
# num_batches_validate = int(float(num_batches) * validate_prop)

validation_gen = torch.Generator().manual_seed(5)
[train_ds, validate_ds] = torch.utils.data.random_split(data, [len_train, len_validate])#, generator=validation_gen)

data_loader_train = torch.utils.data.DataLoader(train_ds, shuffle=True, batch_size=batch_size)
data_loader_validate = torch.utils.data.DataLoader(validate_ds, shuffle=True, batch_size=batch_size)

epochs = 5
learning_rate = 0.01
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
training_losses = []
validation_losses = []
for i in range(epochs):
    training_loss = 0
    for batch_idx, (inputs, targets) in enumerate(data_loader_train):
        inputs = inputs.to(device)
        targets = targets.to(device)
        out = model(inputs)
        out = out.to(device)
        loss = loss_fn(out, targets)
        loss = loss.to(device)
        print(i, batch_idx, loss.item())
        training_loss += loss.item() * (len(targets) / len_train)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    training_losses.append(training_loss)
    validation_loss = 0
    for batch_idx, (inputs, targets) in enumerate(data_loader_validate):
        inputs = inputs.to(device)
        targets = targets.to(device)
        out = model(inputs)
        out = out.to(device)
        loss = loss_fn(out, targets)
        validation_loss += loss.item() * (len(targets) / len_validate)
    validation_losses.append(validation_loss)

plt.figure()
plt.title('Training and validation loss by epoch')
plt.xlabel('Epoch number')
plt.ylabel('Cross Entropy Loss')
plt.plot(np.array(training_losses), label='Training loss')
plt.plot(np.array(validation_losses), label='Validation loss')
plt.legend()
plt.show()

print('Final validation loss:', validation_losses[len(validation_losses) - 1])

PATH = './cifar_net.pth'
torch.save(model.state_dict(), PATH)

model = torchvision.models.alexnet(pretrained=True)
if part_a:
    for param in model.parameters():
        param.requires_grad = False
model.classifier[6] = nn.Linear(4096, 10)
model.load_state_dict(torch.load(PATH))
model.to(device)

test_data = torchvision.datasets.CIFAR10('data/', train=False, download=True, transform=transform)
data_loader_test = torch.utils.data.DataLoader(test_data, shuffle=True, batch_size=batch_size)
len_test = len(test_data)
test_loss = 0
test_accuracy = 0
for batch_idx, (inputs, targets) in enumerate(data_loader_test):
    inputs = inputs.to(device)
    targets = targets.to(device)
    out = model(inputs)
    out = out.to(device)
    loss = loss_fn(out, targets)
    loss = loss.to(device)
    test_loss += loss.item() * (len(targets) / len_test)
    pred = out.argmax(1)
    test_accuracy += (pred == targets).sum().item() / len_test

print('Test loss:', test_loss)
print('Test accuracy:', test_accuracy)
