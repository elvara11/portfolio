import torch
import torch.nn as nn


def collate_fn(batch):
    """
    Create a batch of data given a list of N sequences and labels. Sequences are stacked into a single tensor
    of shape (N, max_sequence_length), where max_sequence_length is the maximum length of any sequence in the
    batch. Sequences shorter than this length should be filled up with 0's. Also returns a tensor of shape (N, 1)
    containing the label of each sequence.

    :param batch: A list of size N, where each element is a tuple containing a sequence tensor and a single item
    tensor containing the true label of the sequence.

    :return: A tuple containing two tensors. The first tensor has shape (N, max_sequence_length) and contains all
    sequences. Sequences shorter than max_sequence_length are padded with 0s at the end. The second tensor
    has shape (N, 1) and contains all labels.
    """
    sentences, labels = zip(*batch)

    max_len = 0
    for s in sentences:
        if len(s) > max_len:
            max_len = len(s)
    
    N = len(labels)
    seqs = torch.zeros((N, max_len), dtype=torch.long)
    labels_out = torch.zeros((N, 1))

    for i in range(len(sentences)):
        sentence_len, = sentences[i].shape
        seqs[i, :sentence_len] = sentences[i]
        seqs[i, sentence_len:] = torch.zeros(max_len - sentence_len, dtype=torch.long)
        labels_out[i] = labels[i].item()

    return seqs, labels_out

class RNNBinaryClassificationModel(nn.Module):
    def __init__(self, embedding_matrix):
        super().__init__()

        vocab_size = embedding_matrix.shape[0]
        embedding_dim = embedding_matrix.shape[1]

        # Construct embedding layer and initialize with given embedding matrix. Do not modify this code.
        self.embedding = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embedding_dim, padding_idx=0)
        self.embedding.weight.data = embedding_matrix

        # self.rnn = nn.RNN(embedding_dim, 64, batch_first=True)
        # self.rnn = nn.GRU(embedding_dim, 64, batch_first=True)
        self.rnn = nn.LSTM(embedding_dim, 64, batch_first=True)
        self.lin = nn.Linear(64, 1)

    def forward(self, inputs):
        """
        Takes in a batch of data of shape (N, max_sequence_length). Returns a tensor of shape (N, 1), where each
        element corresponds to the prediction for the corresponding sequence.
        :param inputs: Tensor of shape (N, max_sequence_length) containing N sequences to make predictions for.
        :return: Tensor of predictions for each sequence of shape (N, 1).
        """
        N, _ = inputs.shape
        mapped_ins = self.embedding(inputs)
        # output, h_n = self.rnn(mapped_ins)
        output, (h_n, c_n) = self.rnn(mapped_ins)
        pred = self.lin(h_n.reshape((N, 64))) #h_n.view((N, 64))
        return torch.sigmoid(pred)

    def loss(self, logits, targets):
        """
        Computes the binary cross-entropy loss.
        :param logits: Raw predictions from the model of shape (N, 1)
        :param targets: True labels of shape (N, 1)
        :return: Binary cross entropy loss between logits and targets as a scalar tensor.
        """
        return torch.nn.functional.binary_cross_entropy(logits, targets)
        # return 1 - self.accuracy(logits, targets)
        # raise NotImplementedError()

    def accuracy(self, logits, targets):
        """
        Computes the accuracy, i.e number of correct predictions / N.
        :param logits: Raw predictions from the model of shape (N, 1)
        :param targets: True labels of shape (N, 1)
        :return: Accuracy as a scalar tensor.
        """
        one_preds = logits > 0.5
        target_ones = targets == 1
        return (one_preds == target_ones).sum() / len(targets)


# Training parameters
TRAINING_BATCH_SIZE = 100
NUM_EPOCHS = 16
LEARNING_RATE = 0.0001

# Batch size for validation, this only affects performance.
VAL_BATCH_SIZE = 128
