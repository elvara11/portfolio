import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
from mnist import MNIST

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

def to_image_mat(x, stride):
    n, = x.shape
    A = np.zeros((stride, stride))
    for i in range(n):
        A[i // stride, i % stride] = x[i]
    return A

X_train_np, labels_train_np, X_test_np, labels_test_np = load_dataset()

X_train_tensor = torch.tensor(X_train_np, requires_grad = True).float()
y_train_tensor = torch.tensor(labels_train_np).long()

X_test_tensor = torch.tensor(X_test_np, requires_grad = True).float()
y_test_tensor = torch.tensor(labels_test_np).long()

n_train, d = X_train_np.shape

num_iters = 300

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

X_train_tensor = X_train_tensor.to(device)
y_train_tensor = y_train_tensor.to(device)
X_test_tensor = X_test_tensor.to(device)
y_test_tensor = y_test_tensor.to(device)

def part_a_b(h, model, part):
    model.to(device)

    loss_fn = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters())
    for i in range(num_iters):
        out = model(X_train_tensor)
        out.to(device)
        loss = loss_fn(X_train_tensor, out)
        loss.to(device)
        if i % 25 == 24:
            print(i, loss.item())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print('Part {} loss (h = {}): {}'.format(part, h, loss_fn(X_train_tensor, model(X_train_tensor))))
    image_mat = np.zeros((28 * 2, 28 * 10))
    for i in range(10):
        ex = (X_train_tensor[y_train_tensor == i])[0]
        approx = model(ex)
        image_mat[:28, 28 * i : 28 * (i + 1)] = to_image_mat(ex, 28)
        image_mat[28:, 28 * i : 28 * (i + 1)] = to_image_mat(approx, 28)

    plt.figure()
    plt.imshow(image_mat)
    plt.savefig('out/A2_part_{}_{}.png'.format(part, h))
    plt.close()
    return model

# Part a
part_a_model = None
hs = [32, 64, 128]
for h in hs:
    out_model = part_a_b(h,
        nn.Sequential(
            nn.Linear(d, h),
            nn.Linear(h, d)
        ), 'a')
    if h == 128:
        part_a_model = out_model

# Part b
hs = [32, 64, 128]
part_b_model = None
for h in hs:
    out_model = part_a_b(h,
        nn.Sequential(
            nn.Linear(d, h),
            nn.ReLU(),
            nn.Linear(h, d),
            nn.ReLU()
        ), 'b')
    if h == 128:
        part_b_model = out_model

loss_fn = nn.MSELoss()
print('Part a test loss: {}'.format(loss_fn(X_test_tensor, part_a_model(X_test_tensor))))
print('Part b test loss: {}'.format(loss_fn(X_test_tensor, part_b_model(X_test_tensor))))
