import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def train(X, Y, reg_lambda, w_initial):
    (n, d) = X.shape
    A = 2 * (X ** 2).sum(0)
    w = w_initial.copy()
    w_old = w_initial.copy()
    max_delta = np.inf

    while max_delta >= 10E-4:
        b = (Y - X.dot(w)).mean()
        for k in range(d):
            c_k = 2 * X.T[k].dot(Y - (b + X.dot(w) - w[k] * X.T[k]))
            if c_k < -reg_lambda:
                w[k] = (c_k + reg_lambda) / A[k]
            elif c_k > reg_lambda:
                w[k] = (c_k - reg_lambda) / A[k]
            else:
                w[k] = 0
        max_delta = abs(w - w_old).max()
        w_old = w.copy()
    return w

def max_lambda(X, Y):
    return (2 * abs((Y - Y.mean()).dot(X))).max()

def reg_path(X, Y):
    (n, d) = X.shape
    reg_lambda = max_lambda(X, Y)
    w = np.zeros(d)
    w_hist = []
    lambda_hist = []
    
    while reg_lambda * 2 >= 0.01:
        w = train(X, Y, reg_lambda, w)
        w_hist.append(w.copy())
        lambda_hist.append(reg_lambda)
        reg_lambda /= 2
    
    return (w_hist, lambda_hist)

# Data import
df_train = pd.read_table("crime-train.txt")
df_test = pd.read_table("crime-test.txt")

Y_train = df_train['ViolentCrimesPerPop'].values
X_train = df_train.iloc[:,1:].values

Y_test = df_test['ViolentCrimesPerPop'].values
X_test = df_test.iloc[:,1:].values

(_, d) = X_train.shape

# Part a
(w_hist, lambda_hist) = reg_path(X_train, Y_train)

non_zero_count = np.zeros(len(w_hist))
for i in range(len(w_hist)):
    non_zero_count[i] = (w_hist[i] != 0).sum()
lambda_hist_arr = np.array(lambda_hist)

plt.figure()
plt.title('LASSO weight sparsity by lambda')
plt.plot(lambda_hist_arr, non_zero_count, 'b-')
plt.xlabel('lambda')
plt.ylabel('# of nonzeros in w')
plt.xscale('log')
plt.show()

# Part b
plt.figure()
plt.title('LASSO coefficient for selected variables by lambda')

b_variables = ['agePct12t29', 'pctWSocSec', 'pctUrban', 'agePct65up', 'householdsize']
for v in b_variables:
    col_i = df_train.columns.get_loc(v)
    y_list = np.zeros(len(w_hist))
    for j in range(len(w_hist)):
        y_list[j] = w_hist[j][col_i]
    plt.plot(lambda_hist_arr, y_list, label=v)
plt.xlabel('lambda')
plt.ylabel('coefficient')
plt.xscale('log')
plt.legend()
plt.show()

# Part c
err = np.zeros(len(w_hist))
for i in range(len(w_hist)):
    Y_pred_test = X_test.dot(w_hist[i])
    sse = ((Y_pred_test - Y_test) ** 2).sum()
    err[i] = sse

plt.figure()
plt.title('LASSO squared error by lambda')
plt.plot(lambda_hist_arr, err, 'b-')
plt.xlabel('lambda')
plt.ylabel('Squared error')
plt.xscale('log')
plt.show()

# Part d
w_l30 = train(X_train, Y_train, 30, np.zeros(d))
print("At lambda=30, the largest weight was {} on variable {}".format(w_l30.max(), df_train.columns[w_l30.argmax()]))
print("At lambda=30, the smallest weight was {} on variable {}".format(w_l30.min(), df_train.columns[w_l30.argmin()]))
