import numpy as np
import matplotlib.pyplot as plt

def train(X, Y, reg_lambda, w_initial):
    (n, d) = X.shape
    A = 2 * (X ** 2).sum(0)
    w = w_initial.copy()
    w_old = w_initial.copy()
    max_delta = np.inf

    while max_delta >= 10E-4:
        b = (Y - X.dot(w)).mean()
        for k in range(d):
            c_k = 2 * X.T[k].dot(Y - (b + X.dot(w) - w[k] * X.T[k]))
            if c_k < -reg_lambda:
                w[k] = (c_k + reg_lambda) / A[k]
            elif c_k > reg_lambda:
                w[k] = (c_k - reg_lambda) / A[k]
            else:
                w[k] = 0
        max_delta = abs(w - w_old).max()
        w_old = w.copy()
    return w

# X is n x d chosen from N(0, 1)
# y_i = w^T x_i + epsilon_i where epsiolon_i is chosen from N(0, sigma^2)
def gen_data(n, d, k, sigma):
    X = np.random.normal(0, 1, (n, d))
    noise = np.random.normal(0, sigma, n)
    w = np.zeros(d)
    for j in range(k):
        w[j] = (j + 1) / k
    Y = w.dot(X.T) + noise
    return (X, Y)

def max_lambda(X, Y):
    return (2 * abs((Y - Y.mean()).dot(X))).max()

def reg_path(X, Y):
    (n, d) = X.shape
    reg_lambda = max_lambda(X, Y)
    w = np.zeros(d)
    w_hist = []
    lambda_hist = []
    
    while (w == 0).any():
        w = train(X, Y, reg_lambda, w)
        w_hist.append(w.copy())
        lambda_hist.append(reg_lambda)
        reg_lambda /= 1.5
    
    return (w_hist, lambda_hist)

# Constants
n = 500
d = 1000
k = 100
sigma = 1

# Part a
(X, Y) = gen_data(n, d, k, sigma)
(w_hist, lambda_hist) = reg_path(X, Y)

non_zero_count = np.zeros(len(w_hist))
for i in range(len(w_hist)):
    non_zero_count[i] = (w_hist[i] != 0).sum()
lambda_hist_arr = np.array(lambda_hist)

# Generate plot 1
plt.figure()
plt.title('LASSO weight sparsity by lambda')
plt.plot(lambda_hist_arr, non_zero_count, 'b-')
plt.xlabel('lambda')
plt.ylabel('# of nonzeros in w')
plt.xscale('log')
plt.show()

# Part b
incorrect_non_zero_count = np.zeros(len(w_hist))
for i in range(len(w_hist)):
    incorrect_non_zero_count[i] = ((w_hist[i] != 0) & (np.array(range(d)) >= k)).sum()
correct_non_zero_count = non_zero_count - incorrect_non_zero_count
fdr = incorrect_non_zero_count / non_zero_count
tpr = correct_non_zero_count / k

# Generate plot 2
plt.figure()
plt.title('LASSO path regularization TPR vs FDR')
plt.plot(fdr, tpr, 'b-')
plt.xlabel('False Discovery Rate (FDR)')
plt.ylabel('True Positive Rate (TPR)')
plt.show()
