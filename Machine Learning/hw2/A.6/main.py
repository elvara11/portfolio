from mnist import MNIST
import numpy as np
import matplotlib.pyplot as plt

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

def prune_data(X, labels):
    X_prune = np.concatenate((X[labels == 2], X[labels == 7]))
    two_labels = np.ones(len(labels[labels == 2])) * -1
    seven_labels = np.ones(len(labels[labels == 7])) * 1
    labels_prune = np.concatenate((two_labels, seven_labels))
    return (X_prune, labels_prune)

def grad_descent(X, Y, reg_lambda, w_initial, iters, step):
    w = w_initial.copy()
    b = 0
    n, d = X.shape

    param_hist = []

    for i in range(iters):
        exp_term = -Y * (b + X.dot(w))
        mu = 1 / (1 + np.exp(exp_term))

        grad_terms = Y * mu * (1 - (1 / mu)) # Terms common to the two gradients

        grad_w_terms = grad_terms.reshape((n, 1))
        grad_w = (grad_w_terms * X).mean(0) + 2 * reg_lambda * w

        grad_b = (grad_terms * b).mean()

        w = w - step * grad_w
        b = b - step * grad_b
        param_hist.append((w, b))
    return param_hist

def stoch_grad_descent(X, Y, reg_lambda, w_initial, iters, step, batch_size):
    w = w_initial.copy()
    b = 0
    n, d = X.shape

    param_hist = []

    for i in range(iters):
        indices = np.random.choice(range(n), batch_size) # TODO: w/ or w/o replacement?
        X_red = X[indices]
        Y_red = Y[indices]

        exp_term = -Y_red * (b + X_red.dot(w))
        mu = 1 / (1 + np.exp(exp_term))

        grad_terms = Y_red * mu * (1 - (1 / mu)) # Terms common to the two gradients

        grad_w_terms = grad_terms.reshape((batch_size, 1))
        grad_w = (grad_w_terms * X_red).mean(0) + 2 * reg_lambda * w

        grad_b = (grad_terms * b).mean()

        w = w - step * grad_w
        b = b - step * grad_b
        param_hist.append((w, b))
    return param_hist

# Data import
X_train_full, labels_train_full, X_test_full, labels_test_full = load_dataset()
X_train, labels_train = prune_data(X_train_full, labels_train_full)
X_test, labels_test = prune_data(X_test_full, labels_test_full)

def b_plots(param_hist, reg_lambda, iters, title1, title2):
    # Generate plot of J(w, b) by iter
    iters_arr = np.array(range(iters)) + 1
    plot1_y_train = np.zeros(iters)
    plot1_y_test = np.zeros(iters)
    for i in range(iters):
        w, b = param_hist[i]

        exp_term = -labels_train * (b + X_train.dot(w))
        J_terms = np.log(1 + np.exp(exp_term))
        J = J_terms.mean() + reg_lambda * np.linalg.norm(w)
        plot1_y_train[i] = J

        exp_term = -labels_test * (b + X_test.dot(w))
        J_terms = np.log(1 + np.exp(exp_term))
        J = J_terms.mean() + reg_lambda * np.linalg.norm(w)
        plot1_y_test[i] = J

    plt.figure()
    plt.title(title1)
    plt.plot(iters_arr, plot1_y_train, label='train')
    plt.plot(iters_arr, plot1_y_test, label='test')
    plt.xlabel('iteration')
    plt.ylabel('loss')
    plt.legend()
    plt.show()

    # Generate plot of misclassification error by iter
    plot2_y_train = np.zeros(iters)
    plot2_y_test = np.zeros(iters)

    for i in range(iters):
        w, b = param_hist[i]

        lin_pred_train = np.sign(X_train.dot(w))
        plot2_y_train[i] = (lin_pred_train != labels_train).mean()

        lin_pred_test = np.sign(X_test.dot(w))
        plot2_y_test[i] = (lin_pred_test != labels_test).mean()

    plt.figure()
    plt.title(title2)
    plt.plot(iters_arr, plot2_y_train, label='train')
    plt.plot(iters_arr, plot2_y_test, label='test')
    plt.xlabel('iteration')
    plt.ylabel('misclassification error')
    plt.legend()
    plt.show()

# Constants
_, d = X_train.shape
reg_lambda = 0.1
w_initial = np.zeros(d)

iters_gd = 30
step_gd = 0.5
title1_gd = 'Logistic regression loss by iter during gradient descent'
title2_gd = 'Logistic regression misclassification error by iter during gradient descent'

iters_sgd1 = 3000
step_sgd1 = 0.002
title1_sgd = 'Logistic regression loss by iter during SGD'
title2_sgd = 'Logistic regression misclassification error by iter during SGD'

iters_sgd100 = 500
step_sgd100 = 0.02

# Run GD
param_hist_gd = grad_descent(X_train, labels_train, reg_lambda, w_initial, iters_gd, step_gd)
b_plots(param_hist_gd, reg_lambda, iters_gd, title1_gd, title2_gd)

# Run SGD with batch_size = 1
param_hist_sgd1 = stoch_grad_descent(X_train, labels_train, reg_lambda, w_initial, iters_sgd1, step_sgd1, 1)
b_plots(param_hist_sgd1, reg_lambda, iters_sgd1, title1_sgd, title2_sgd)

# Run SGD with batch_size = 100
param_hist_sgd100 = stoch_grad_descent(X_train, labels_train, reg_lambda, w_initial, iters_sgd100, step_sgd100, 100)
b_plots(param_hist_sgd100, reg_lambda, iters_sgd100, title1_sgd, title2_sgd)
