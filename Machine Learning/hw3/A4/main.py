import numpy as np
import matplotlib.pyplot as plt
import math

# Kernel ridge regression

def gen_data(n):
    X = np.random.uniform(size=n)
    f = 4 * np.sin(np.pi * X) * np.cos(6 * np.pi * (X ** 2))
    y = f + np.random.normal(size=n)
    return (X, y)

def gen_kernel(X, k_param, kernel_func):
    n, = X.shape
    index_mat = np.resize(np.array(range(n)), (n, n)).T
    K = kernel_func(X[index_mat], X[index_mat.T], k_param)
    # K = np.zeros((n, n))
    # for i in range(n):
    #     for j in range(n):
    #         K[i, j] = kernel_func(X[i], X[j], k_param)
    return K

def learn(X, y, reg_lambda, K):
    # return np.linalg.pinv(K).dot(y)
    # reg_lambda = 0
    # A = 2 * K.T.dot(K) + reg_lambda * K + reg_lambda * K.T
    # alpha = np.linalg.solve(A, 2 * K.T.dot(y))
    # (alpha, _, r, _) = np.linalg.lstsq(A, 2 * K.T.dot(y), rcond=None)
    # print('r: {}, n: {}'.format(r, len(X)))
    # print(np.allclose(A.dot(alpha), 2 * K.T.dot(y)))
    # print(np.linalg.norm(A.dot(alpha) - 2 * K.T.dot(y)))
    # return 2 * np.linalg.inv(A).dot(K.T).dot(y)
    # (alpha, _, _, _) = np.linalg.lstsq(2 * K.T.dot(K) + reg_lambda * K + reg_lambda * K.T, 2 * K.T.dot(y))
    # alpha = np.linalg.solve(A, K.T.dot(y))
    # alpha = np.linalg.inv(K.T.dot(K) + reg_lambda * K).dot(K.T).dot(y)
    n, = X.shape
    (alpha, _, _, _) = np.linalg.lstsq(K + reg_lambda * np.eye(n), y, rcond=None)
    return alpha

def predict(X_orig, X_eval, alpha, k_param, k_fn):
    y_pred = np.zeros(len(X_eval))
    for j in range(len(X_eval)):
        for i in range(len(X_orig)):
            y_pred[j] += alpha[i] * k_fn(X_orig[i], X_eval[j], k_param)
    return y_pred

n = 30
(X, y) = gen_data(n)

k_poly = lambda x, z, d: (1 + x * z) ** d
k_rbf = lambda x, z, gamma: np.exp(-gamma * np.abs(x - z))

d = 20
gamma = 3 * 10E-2#3 * 10E-2
# reg_lambda = 1 * 10E-3
reg_lambda_poly = 1 * 10E-3
reg_lambda_rbf = 1 * 10E-3


x_orig_fn = np.array(range(1001)) / 1000
y_orig_fn = 4 * np.sin(np.pi * x_orig_fn) * np.cos(6 * np.pi * (x_orig_fn ** 2))

# E_train_loo_poly = 0
# E_train_loo_rbf = 0

# d_high = 101
# d_low = 1
# d_subdiv = 100

# d_poss = np.zeros(d_subdiv)
# index_mat_red = np.resize(np.array(range(n - 1)), (n - 1, n - 1)).T
# for i in range(d_subdiv):
#     d_eval = d_low + i * (d_high - d_low) / d_subdiv

#     E_train_loo_poly = 0
#     for j in range(n):
#         X_loo = np.delete(X, j)
#         y_loo = np.delete(y, j)

#         # K_poly = gen_kernel(X_loo, d_eval, k_poly)
#         K_poly = k_poly(X_loo[index_mat_red], X_loo[index_mat_red.T], d_eval)
#         alpha_poly = learn(X_loo, y_loo, reg_lambda, K_poly)
#         y_pred_poly = (alpha_poly * k_poly(X_loo, X[j] * np.ones(n - 1), d_eval)).sum()
#         E_train_loo_poly += (y_pred_poly - y[j]) ** 2
#     d_poss[i] = E_train_loo_poly
# d_pick = d_low + d_poss.argmin() * (d_high - d_low) / d_subdiv
# print("D_pick: {}".format(d_pick))
# print("Error: {}".format(d_poss.min()))

loo_batch_size = 1
loo_num_batches = n // loo_batch_size
E_train_loo_poly = 0
for batch_num in range(loo_num_batches):
    j = loo_batch_size * batch_num
    X_loo = np.delete(X, range(j, j + loo_batch_size))
    y_loo = np.delete(y, range(j, j + loo_batch_size))

    K_poly = gen_kernel(X_loo, d, k_poly)
    alpha_poly = learn(X_loo, y_loo, reg_lambda_poly, K_poly)
    y_pred_poly = predict(X_loo, X[j : j + loo_batch_size], alpha_poly, d, k_poly)
    E_train_loo_poly += ((y_pred_poly - y[j : j + loo_batch_size]) ** 2).mean()
print('CV poly error: {}'.format(E_train_loo_poly / loo_num_batches))

E_train_loo_rbf = 0
for batch_num in range(loo_num_batches):
    j = loo_batch_size * batch_num
    X_loo = np.delete(X, range(j, j + loo_batch_size))
    y_loo = np.delete(y, range(j, j + loo_batch_size))

    K_rbf = gen_kernel(X_loo, gamma, k_rbf)
    alpha_rbf = learn(X_loo, y_loo, reg_lambda_rbf, K_rbf)
    y_pred_rbf = predict(X_loo, X[j : j + loo_batch_size], alpha_rbf, gamma, k_rbf)
    E_train_loo_rbf += ((y_pred_rbf - y[j : j + loo_batch_size]) ** 2).mean()
print('CV rbf error: {}'.format(E_train_loo_rbf / loo_num_batches))

K_poly = gen_kernel(X, d, k_poly)
alpha_poly = learn(X, y, reg_lambda_poly, K_poly)
y_pred = K_poly.T.dot(alpha_poly)

plt.figure()
plt.title("poly")
plt.scatter(X, y, label='actual')
# plt.scatter(X, y_pred, label='pred')
plt.plot(x_orig_fn, y_orig_fn, 'r', label='fn')
plt.plot(x_orig_fn, predict(X, x_orig_fn, alpha_poly, d, k_poly), 'g', label='pred')
plt.xlim(0,1)
plt.ylim(-10,10)
plt.legend()
plt.savefig('out/poly_pred.png')
plt.show()

K_rbf = gen_kernel(X, gamma, k_rbf)
alpha_rbf = learn(X, y, reg_lambda_rbf, K_rbf)
y_pred = K_rbf.T.dot(alpha_rbf)

plt.figure()
plt.title('rbf')
plt.scatter(X, y, label='actual')
# plt.scatter(X, y_pred, label='pred')
plt.plot(x_orig_fn, y_orig_fn, 'r', label='fn')
plt.plot(x_orig_fn, predict(X, x_orig_fn, alpha_rbf, gamma, k_rbf), 'g', label='pred')
plt.xlim(0,1)
plt.ylim(-10,10)
plt.legend()
plt.savefig('out/rbf_pred.png')
plt.show()

# E_train_loo_poly = 0
# E_train_loo_rbf = 0
# y_pred_loo = np.zeros(n)
# for i in range(n):
#     X_loo = np.delete(X, i)
#     y_loo = np.delete(y, i)

#     print(X_loo)

#     K_poly = gen_kernel(X_loo, d, k_poly)
#     K_rbf = gen_kernel(X_loo, gamma, k_rbf)

#     alpha_poly = learn(X_loo, y_loo, reg_lambda, K_poly)
#     alpha_rbf = learn(X_loo, y_loo, reg_lambda, K_rbf)

#     y_pred_poly = (alpha_poly * k_poly(X_loo, X[i] * np.ones(n - 1), d)).sum()
#     y_pred_rbf = (alpha_rbf * k_rbf(X_loo, X[i] * np.ones(n - 1), gamma)).sum()

#     print(E_train_loo_poly)
#     E_train_loo_poly += (y_pred_poly - y[i]) ** 2
#     E_train_loo_rbf += (y_pred_rbf - y[i]) ** 2
#     y_pred_loo[i] = y_pred_poly

# print("Poly LOO error: {}".format(E_train_loo_poly / n))
# print("RBF LOO error: {}".format(E_train_loo_rbf / n))

# plt.figure()
# plt.title("poly")
# plt.scatter(X, y, label='actual')
# plt.scatter(X, y_pred, label='pred')
# plt.legend()
# plt.show()
def part_c(k_fn, k_param, reg_lambda, title, fname):
    B = 300
    b_eval = np.ndarray((B, len(x_orig_fn)))
    for i in range(B):
        # print(i)
        index_b = np.random.choice(np.array(range(n)), size=n)
        X_b = X[index_b]
        y_b = y[index_b]

        K = gen_kernel(X_b, k_param, k_fn)
        alpha = learn(X_b, y_b, reg_lambda, K)

        b_eval[i] = predict(X_b, x_orig_fn, alpha, k_param, k_fn)

    b_bottom_ci = np.zeros(len(x_orig_fn))
    b_top_ci = np.zeros(len(x_orig_fn))
    for i in range(len(x_orig_fn)):
        # print(i)
        eval_vec = b_eval.T[i].copy()
        eval_vec.sort()
        b_bottom_ci[i] = eval_vec[math.floor((1 / 20) * B)]
        b_top_ci[i] = eval_vec[math.floor((19 / 20) * B)]

    K = gen_kernel(X, k_param, k_fn)
    alpha = learn(X, y, reg_lambda, K)

    plt.figure()
    plt.title('{} Confidence Interval'.format(title))
    plt.scatter(X, y, label='actual')
    # plt.scatter(X, y_pred, label='pred')
    plt.plot(x_orig_fn, y_orig_fn, 'r', label='Function')
    plt.plot(x_orig_fn, predict(X, x_orig_fn, alpha, k_param, k_fn), 'g', label='Predicted')
    plt.plot(x_orig_fn, b_bottom_ci, label='5th percentile')
    plt.plot(x_orig_fn, b_top_ci, label='95th percentile')
    plt.legend()
    plt.xlim(0, 1)
    plt.ylim(-10, 10)
    plt.savefig(fname)
    plt.show()

part_c(k_poly, d, reg_lambda_poly, 'Poly', 'out/poly_ci.png')
part_c(k_rbf, gamma, reg_lambda_rbf, 'RBF', 'out/rbf_ci.png')

alpha_rbf = learn(X, y, reg_lambda_rbf, K_rbf)
alpha_poly = learn(X, y, reg_lambda_poly, K_poly)

B = 300
m = 1000
new_X, new_y = gen_data(m)
b_errors = np.zeros(B)
for i in range(B):
    index_b = np.random.choice(np.array(range(m)), size=m)
    X_b = new_X[index_b]
    y_b = new_y[index_b]
    y_pred_poly = predict(X, X_b, alpha_poly, d, k_poly)
    y_pred_rbf = predict(X, X_b, alpha_rbf, gamma, k_rbf)
    error = ((y_b - y_pred_poly) ** 2 - (y_b - y_pred_rbf) ** 2).mean()
    b_errors[i] = error
b_errors.sort()

print('5%: {}'.format(b_errors[math.floor((1 / 20) * B)]))
print('95%: {}'.format(b_errors[math.floor((19 / 20) * B)]))
