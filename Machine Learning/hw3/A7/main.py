import numpy as np
import matplotlib.pyplot as plt
from mnist import MNIST

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

def to_image_mat(x, stride):
    n, = x.shape
    A = np.zeros((stride, stride))
    for i in range(n):
        A[i // stride, i % stride] = x[i]
    return A

X_whole, labels_whole, _, _ = load_dataset()

X_train = X_whole[:50000]
X_test = X_whole[50000:]

n, d = X_train.shape

n_test, _ = X_test.shape
mu = X_train.mean(0)
delta_train = X_train - np.ones((n, 1)).dot(mu.reshape((d,1)).T)
delta_test = X_test - np.ones((n_test, 1)).dot(mu.reshape((d,1)).T)

sigma = delta_train.T.dot(delta_train)
# eigvals = np.linalg.eigvals(sigma)
# eigvals.sort()
# print(eigvals)
# print('lambda1: {}, lambda2: {}, lambda10: {}, lambda30: {}, lambda50: {}'.format(eigvals[0], eigvals[1], eigvals[9], eigvals[29], eigvals[49]))
# print('Eigval sum: {}'.format(eigvals.sum()))

s, v = np.linalg.eig(sigma)

print('lambda1: {}, lambda2: {}, lambda10: {}, lambda30: {}, lambda50: {}'.format(s[0], s[1], s[9], s[29], s[49]))
print('Eigval sum: {}'.format(s.sum()))

m = 100
train_err_arr = np.zeros(m)
test_err_arr = np.zeros(m)
eigval_prop = np.zeros(m)
for k in range(1, m + 1):
    print(k)
    eigvecs = v[:,:k]
    e_sq = eigvecs.dot(eigvecs.T)

    X_train_proj = delta_train.dot(e_sq)
    train_errors = delta_train - X_train_proj
    train_err = (np.linalg.norm(train_errors, axis=1) ** 2).mean()
    train_err_arr[k - 1] = train_err

    X_test_proj = delta_test.dot(e_sq)
    test_errors = delta_test - X_test_proj
    test_err = (np.linalg.norm(test_errors, axis=1) ** 2).mean()
    test_err_arr[k - 1] = test_err

    eigval_prop[k - 1] = 1 - (s[:k].sum()) / (s.sum())

plt.figure()
plt.plot(np.array(range(m)), train_err_arr, label='training error')
plt.plot(np.array(range(m)), test_err_arr, label='test error')
plt.title('Reconstruction error by dimensionality')
plt.xlabel('Number of eigenvectors')
plt.ylabel('Mean reconstruction error')
plt.legend()
plt.show()

plt.figure()
plt.plot(np.array(range(m)), eigval_prop)
plt.title('Eigenvalue proportion by dimensionality')
plt.xlabel('Number of eigenvectors')
plt.ylabel('Eigenvalue proportion')
plt.show()

im_mat = np.zeros((28, 28 * 10))
for i in range(10):
    vec = v[:,i]
    im_mat[:, 28 * i : 28 * (i + 1)] = to_image_mat(vec, 28)
plt.figure()
plt.imshow(im_mat)
plt.savefig('out/d.png')
plt.show()

two = X_whole[labels_whole == 2][0]
six = X_whole[labels_whole == 6][0]
seven = X_whole[labels_whole == 7][0]
nums = [2, 6, 7]
k_vals = [5, 15, 40, 100]

for num in nums:
    x = X_whole[labels_whole == num][0]
    img = np.zeros((28, (len(k_vals) + 1) * 28))
    img[:, len(k_vals) * 28 :] = to_image_mat(x, 28)
    for i in range(len(k_vals)):
        k_val = k_vals[i]
        eigvecs = v[:, : k_val]
        e_sq = eigvecs.dot(eigvecs.T)

        x_vec = (x - mu).copy().reshape((d, 1))
        x_proj = x_vec.T.dot(e_sq).flatten() + mu
        img[:, i * 28 : (i + 1) * 28] = to_image_mat(x_proj, 28)
    plt.figure()
    plt.imshow(img)
    plt.savefig('out/{}.png'.format(num))
    plt.close()
