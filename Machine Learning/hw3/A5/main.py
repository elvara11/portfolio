import numpy as np
import matplotlib.pyplot as plt
from mnist import MNIST

# K-means clustering

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

def lloyds(X, k):
    n, d = X.shape
    points = X[np.random.choice(np.array(range(n)), size=k, replace=False)]
    delta = np.array([np.inf]).repeat(k)
    points_by_iter = [points.copy()]
    while (delta.sum() / k) > 0.01:
        print('delta: {}'.format(delta.sum() / k))
        # dists = np.zeros((k, n))
        # for i in range(k):
        #     dists[i] = np.linalg.norm(X - np.dot(np.ones((n, 1)), points[i].reshape((1, d))), axis=1)
        # min_dists = dists.min(0)
        # closest_center = dists.argmin(0)
        # for i in range(k):
        #     newPt = X[closest_center == i].mean()
        #     delta[i] = np.linalg.norm(newPt - points[i])
        #     points[i] = newPt
        # points_by_iter.append(points.copy())
        # print('delta: {}, err: {}'.format(delta.mean(), min_dists.mean()))

        sums = np.zeros((k, d))
        counts = np.zeros(k)
        for i in range(n):
            min_dist = np.inf
            closest = -1
            for j in range(k):
                dist = np.linalg.norm(points[j] - X[i])
                if dist < min_dist:
                    min_dist = dist
                    closest = j
            sums[closest] += X[i]
            counts[closest] += 1
        for i in range(k):
            newPt = sums[i] / counts[i]
            delta[i] = np.linalg.norm(points[i] - newPt)
            points[i] = newPt
        points_by_iter.append(points.copy())
    return points_by_iter

def to_image_mat(x, stride):
    n, = x.shape
    A = np.zeros((stride, stride))
    for i in range(n):
        A[i // stride, i % stride] = x[i]
    return A

def get_error(X, clusters):
    n, d = X.shape
    k, _ = clusters.shape
    # dists = np.zeros((k, n))
    # for i in range(k):
    #     dists[i] = np.linalg.norm(X - np.dot(np.ones((n, 1)), clusters[i].reshape((1, d))), axis=1)
    # min_dists = dists.min(0)
    # return min_dists.mean()
    sum = 0
    for i in range(n):
        min_dist = np.inf
        closest = -1
        for j in range(k):
            dist = np.linalg.norm(clusters[j] - X[i])
            if dist < min_dist:
                min_dist = dist
                closest = j
        sum += np.linalg.norm(X[i] - clusters[closest])
    return sum / n

X_train, labels_train, X_test, labels_test = load_dataset()

n, d = X_train.shape
k = 10
stride = 28

clusters_by_iter = lloyds(X_train, 10)
num_iters = len(clusters_by_iter)

for j in range(num_iters):
    comb = np.zeros((stride, k * stride))
    for i in range(k):
        pt = clusters_by_iter[j][i]
        comb[:, 28 * i : 28 * (i + 1)] = to_image_mat(pt, stride)
    plt.figure()
    plt.imshow(comb)
    plt.savefig('out/{}.png'.format(j))
    plt.close()

# Plot objective function by iter
obj_fn = np.zeros(num_iters)
for i in range(num_iters):
    centers = clusters_by_iter[i]
    obj_fn[i] = get_error(X_train, centers) * n

plt.figure()
plt.title('Lloyd\'s algorithm objective function by iter')
plt.plot(np.array(range(num_iters)), obj_fn)
plt.xlabel('Iteration number')
plt.ylabel('Objective function')
plt.savefig('out/obj.png')
plt.show()

# Print final clusters
comb = np.zeros((stride, k * stride))
for i in range(k):
    pt = clusters_by_iter[num_iters - 1][i]
    comb[:, 28 * i : 28 * (i + 1)] = to_image_mat(pt, stride)
plt.figure()
plt.imshow(comb)
plt.savefig('out/final_clusters.png')
plt.close()

# Plot training and test error by k-value
k_arr = np.array([2, 4, 8, 16, 32, 64])
train_error_arr = np.zeros(len(k_arr))
test_error_arr = np.zeros(len(k_arr))
for i in range(len(k_arr)):
    k_val = k_arr[i]
    clusters_by_iter = lloyds(X_train, k_val)
    last_clusters = clusters_by_iter[len(clusters_by_iter) - 1]

    train_error_arr[i] = get_error(X_train, last_clusters)
    test_error_arr[i] = get_error(X_test, last_clusters)
    print('Train: {}'.format(train_error_arr[i]))
    print('Test: {}'.format(test_error_arr[i]))

plt.figure()
plt.title('Training and test error by k-value')
plt.plot(k_arr, train_error_arr, label='training error')
plt.plot(k_arr, test_error_arr, label='test error')
plt.legend()
plt.savefig('out/error.png')
plt.show()
