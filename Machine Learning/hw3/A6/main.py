import torch
import numpy as np
import matplotlib.pyplot as plt
from mnist import MNIST

def load_dataset():
    mndata = MNIST('./data/')
    X_train, labels_train = map(np.array, mndata.load_training())
    X_test, labels_test = map(np.array, mndata.load_testing())
    X_train = X_train/255.0
    X_test = X_test/255.0
    return X_train, labels_train, X_test, labels_test

X_train, labels_train, X_test, labels_test = load_dataset()

def eval_shallow(X, params):
    n, _ = X.shape
    [W_zero, b_zero, W_one, b_one] = params
    y_pred = \
        torch.mm( \
            W_one, \
            torch.nn.functional.relu( \
                torch.mm( \
                    W_zero, \
                    X.T \
                ) + \
                torch.mm( \
                    b_zero, \
                    torch.ones((1, n)) \
                ) \
            ) \
        ) + \
        torch.mm(b_one, torch.ones((1, n)))
    return y_pred

def eval_deep(X, params):
    n, _ = X.shape
    [W_zero, b_zero, W_one, b_one, W_two, b_two] = params
    y_pred = \
        torch.mm( \
            W_two, \
            torch.nn.functional.relu( \
                torch.mm( \
                    W_one, \
                    torch.nn.functional.relu( \
                        torch.mm( \
                            W_zero, \
                            X.T \
                        ) + \
                        torch.mm( \
                            b_zero, \
                            torch.ones((1, n)) \
                        ) \
                    ) \
                ) + \
                torch.mm(b_one, torch.ones((1, n))) \
            )
        ) + \
        torch.mm(b_two, torch.ones((1, n)))
    return y_pred

# Proportion of x's which were misclassified
def error_shallow(X, y, params):
    [W_zero, b_zero, W_one, b_one] = params
    y_pred = eval_shallow(X, params)
    label_pred = y_pred.argmax(0)
    return (label_pred != y).sum() / len(y)

def error_deep(X, y, params):
    [W_zero, b_zero, W_one, b_one, W_two, b_two] = params
    y_pred = eval_deep(X, params)
    label_pred = y_pred.argmax(0)
    return (label_pred != y).sum() / len(y)

def train_shallow(X, y, h, k):
    n, d = X.shape

    alpha = 1 / np.sqrt(d)
    W_zero = (torch.rand((h, d)) - 0.5) * 2 * alpha
    b_zero = (torch.rand((h, 1)) - 0.5) * 2 * alpha
    W_one = (torch.rand((k, h)) - 0.5) * 2 * alpha
    b_one = (torch.rand((k, 1)) - 0.5) * 2 * alpha

    W_zero.requires_grad_(True)
    b_zero.requires_grad_(True)
    W_one.requires_grad_(True)
    b_one.requires_grad_(True)

    loss_history = []

    params = [W_zero, b_zero, W_one, b_one]

    optimizer = torch.optim.Adam(params)

    i = 0
    while error_shallow(X, y, params) > 0.01:
        y_pred = eval_shallow(X, params)

        loss = torch.nn.functional.cross_entropy(y_pred.T, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        loss_history.append(loss.item())
        print('Iter: {}, error: {}, loss: {}'.format(i, error_shallow(X, y, params), loss_history[i]))
        i += 1
    return (params, loss_history)

def train_deep(X, y, h1, h2, k):
    n, d = X.shape

    alpha = 1 / np.sqrt(d)
    W_zero = (torch.rand((h1, d)) - 0.5) * 2 * alpha
    b_zero = (torch.rand((h1, 1)) - 0.5) * 2 * alpha
    W_one = (torch.rand((h2, h1)) - 0.5) * 2 * alpha
    b_one = (torch.rand((h2, 1)) - 0.5) * 2 * alpha
    W_two = (torch.rand((k, h2)) - 0.5) * 2 * alpha
    b_two = (torch.rand((k, 1)) - 0.5) * 2 * alpha

    W_zero.requires_grad_(True)
    b_zero.requires_grad_(True)
    W_one.requires_grad_(True)
    b_one.requires_grad_(True)
    W_two.requires_grad_(True)
    b_two.requires_grad_(True)

    loss_history = []

    params = [W_zero, b_zero, W_one, b_one, W_two, b_two]

    optimizer = torch.optim.Adam(params)

    i = 0
    while error_deep(X, y, params) > 0.01:
        y_pred = eval_deep(X, params)

        loss = torch.nn.functional.cross_entropy(y_pred.T, y)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        loss_history.append(loss.item())
        print('Iter: {}, error: {}, loss: {}'.format(i, error_deep(X, y, params), loss_history[i]))
        i += 1
    return (params, loss_history)

def part_a():
    h = 64
    k = 10

    X_train_tensor = torch.tensor(X_train, requires_grad = True).float()
    y_train_tensor = torch.tensor(labels_train).long()
    params, loss_history = train_shallow(X_train_tensor, y_train_tensor, h, k)

    print(params)
    print(loss_history)

    plt.figure()
    plt.title('Training loss by epoch')
    plt.plot(np.array(range(len(loss_history))), np.array(loss_history))
    plt.xlabel('Epoch')
    plt.ylabel('Cross entropy loss')
    plt.show()

    X_test_tensor = torch.tensor(X_test, requires_grad = True).float()
    y_test_tensor = torch.tensor(labels_test).long()

    test_accuracy = 1 - error_shallow(X_test_tensor, y_test_tensor, params)
    print('Accuracy on test data: {}'.format(test_accuracy))
    test_pred = eval_shallow(X_test_tensor, params)
    print('Loss on test data: {}'.format(torch.nn.functional.cross_entropy(test_pred.T, y_test_tensor)))

def part_b():
    h1 = 32
    h2 = 32
    k = 10

    X_train_tensor = torch.tensor(X_train, requires_grad = True).float()
    y_train_tensor = torch.tensor(labels_train).long()
    params, loss_history = train_deep(X_train_tensor, y_train_tensor, h1, h2, k)

    print(params)
    print(loss_history)

    plt.figure()
    plt.title('Training loss by epoch')
    plt.plot(np.array(range(len(loss_history))), np.array(loss_history))
    plt.xlabel('Epoch')
    plt.ylabel('Cross entropy loss')
    plt.show()

    X_test_tensor = torch.tensor(X_test, requires_grad = True).float()
    y_test_tensor = torch.tensor(labels_test).long()

    test_accuracy = 1 - error_deep(X_test_tensor, y_test_tensor, params)
    print('Accuracy on test data: {}'.format(test_accuracy))
    test_pred = eval_deep(X_test_tensor, params)
    print('Loss on test data: {}'.format(torch.nn.functional.cross_entropy(test_pred.T, y_test_tensor)))

part_a()
part_b()