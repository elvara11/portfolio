#%%
print("Hello world")
# %%
import numpy as np
a = np.array([[0, 2, 4], [2, 4, 2], [3, 3, 1]])
print("A^-1")
print(np.linalg.inv(a))

# %%
import numpy as np
a = np.array([[0, 2, 4], [2, 4, 2], [3, 3, 1]])
b = np.array([[-2], [-2], [-4]])
c = np.array([[1], [1], [1]])
print("A^-1 b")
print(np.matmul(np.linalg.inv(a), b))
print("A c")
print(np.matmul(a, c))
# %%
import numpy as np
import matplotlib.pyplot as plt
n = 40000
Z = np.random.randn(n)
plt.step(sorted(Z), np.arange(1, n + 1) / float(n))
# %%
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_theme()

n = 40000
Z = np.random.randn(n)
plt.step(sorted(Z), np.arange(1, n + 1) / float(n), label='Gaussian')

for k in [1, 8, 64, 512]:
    Y = np.sum(np.sign(np.random.randn(n, k))*np.sqrt(1./k), axis=1)
    plt.step(sorted(Y), np.arange(1, n + 1) / float(n), label=str(k))

plt.axis([-3, 3, 0, 1])
plt.xlabel('Observations')
plt.ylabel('Probability')
plt.legend()
plt.show()
# %%
