
import sys
from tqdm import tqdm

import torch
import torchvision

from lit_data import NUM_CLASSES
from birds import create_model
from data import get_bird_data

def collect_failure_stats(device, net, dataloader, idx_to_class):
    net.to(device)
    net.eval()

    bad_images = []
    label_failure_rate_hist = torch.zeros(NUM_CLASSES, dtype=torch.long, device=device)
    label_occurrence_rate_hist = torch.zeros(NUM_CLASSES, dtype=torch.long, device=device)

    with torch.no_grad():
        for batch in tqdm(dataloader):
            images, labels = batch[0].to(device), batch[1].to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)

            label_failure_rate_hist += torch.bincount(labels[labels != predicted], minlength=label_failure_rate_hist.shape[0])
            label_occurrence_rate_hist += torch.bincount(labels, minlength=label_failure_rate_hist.shape[0])

            # sample first image of each batch as example of failure
            if not labels[0] in predicted:
                bad_images.append(images[0, :, :, :].to(torch.device('cpu')))

    if len(bad_images) > 0:
        torchvision.utils.save_image(torchvision.utils.make_grid(bad_images, nrow=5, ), 'incorrect_predictions.png')

    frequent_failure_counts, frequent_failure_labels = torch.topk(label_failure_rate_hist, 100)
    frequent_failure_totals = label_occurrence_rate_hist[frequent_failure_labels]
    for count, total, model_label in zip(frequent_failure_counts.cpu(), frequent_failure_totals.cpu(), frequent_failure_labels.cpu()):
        class_num = idx_to_class[idx_to_class[model_label.item()]]
        print(f"\tclass {class_num}: {count}/{total} incorrect")

if __name__ == "__main__":
    _, checkpoint, data_root_dir = sys.argv

    checkpoint_data = torch.load(checkpoint)
    model = create_model()
    model.load_state_dict(checkpoint_data['net'])

    model.to('cuda')
    model.eval()

    data = get_bird_data(data_root_dir)

    collect_failure_stats('cuda', model, data.val, data.idx_to_class)
