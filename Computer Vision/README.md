# Bird species classification
## Video
Our video is [here](https://youtu.be/VP4jyk8iAK0).
## Problem and Dataset
The dataset given included 48,562 (38,562 train and 10,000 test) images of birds of many genders and species.
All in all, there were 555 classes of birds, with an average of 70 photos of each type of bird in the train
set. The challenge was to write a classifier, that given an unidentified picture of the bird never before
seen it could predict the class of that bird with high accuracy. The goal in this problem was to reach ~80%
accuracy or higher, with the challenge that nobody had ever reached above 90%. We achieved higher than 90% test accuracy.

The given code started with a ResNet-18 classifier pretrained on ImageNet fine-tuned to this dataset by
replacing the final fully-connected layer with one that mapped to the right number of output classes.

## Previous work

Our model and infrastructure, as described below, use both a pre-trained stock Faster R-CNN and a separate pre-trained
ResNet-50 which we fine-tune on the bird-classification problem.

The R-CNN is pre-trained on COCO, while the ResNet is pre-trained on ImageNet.

### Custom code

Some of the training infrastructure is adapted from provided sample notebooks, but heavily refactored. We have written
custom logic for dataset preprocessing, evaluation, prediction, and some augmentations.

## Approach

We began with a ResNet-18 and then progressively increased the model scale, improved training procedures, and tuned
hyperparameters. The final implementation is a hybrid composed of multiple models.

The core classifier is a ResNet-50. This classifier is initialized with pre-trained weights from PyTorch. We then train
on a "cropped" dataset, described below, for roughly 20 epochs, using an LR schedule which multiplies the LR by 0.7-0.85
every epoch. We use an 80/20 train/val split for iterating and then train on the full dataset for final submission.
Input samples are augmented with perspective transforms, a small amount of shifting/cropping, color jitter, and Gaussian
noise.

This classifier is the second stage of a larger pipeline. We observed that the actual bird target was often relatively
small in the input image, meaning the model was computing a lot of largely useless features. To compensate, what we have
built is essentially a "classification R-CNN". In a normal R-CNN, a region proposal network selects areas of interest,
then a classifier runs on each AOI to both decide whether it is a valid detection and compute the class. We are not
solving a detection problem in this challenge, but nonetheless use the detection capabilities to optimize for
computation on only the bird.

Conceptually, our model is a normal R-CNN where the highest-confidence detection is used as the overall class for the
image. In practice, however, we implemented the candidate detection as a separate preprocessing step, creating a new
"cropped" dataset from the region detected by the R-CNN. We did this for two reasons:

- The given bird dataset isn't labelled for the detection task; thus, we can't easily train the region proposal logic of
  an R-CNN (or the equivalent in other detectors).
- Detection is computationally intensive, and by pre-processing the dataset we can focus our cycles at train time on
  the specific bird species in this challenge.

As such, we used an R-CNN pre-trained on the COCO dataset. Conveniently, COCO has a "bird" label; we run the model on
each image, extract detections, filter for the "bird" class, select a high-confidence target, and crop to a minimal
square around that bounding box. If no bird is found, we fall back to the whole image. We then train our classifier on
the cropped results, and similarly for evaluation on the test set. This model had excellent performance in identifying
correct birds in the input data, however some _were_ over-cropped, mis-detected, or unidentified altogether.

Each column below shows the original image on the bottom and and cropped image on the top. In order, from left to right:\
a successful crop, a completely mis-detected crop, an mostly uncropped image due to failing to detect the bird, and a poorly-cropped image.

<img src="examples/crop_good.jpg" width="200" height="200" title="Successful Crop"/>
<img src="examples/crop_failed.jpg" width="200" height="200" title="Failed Crop"/>
<img src="examples/crop_none.jpg" width="200" height="200" title="No Crop"/>
<img src="examples/crop_toomuch.jpg" width="200" height="200" title="Over-cropped"/>
<p>
<img src="examples/crop_good_src.jpg" width="200" height="200" title="Original Image"/>
<img src="examples/crop_failed_src.jpg" width="200" height="200" title="Original Image"/>
<img src="examples/crop_none_src.jpg" width="200" height="200" title="Original Image"/>
<img src="examples/crop_toomuch_src.jpg" width="200" height="200" title="Original Image"/>


## Incremental improvements and experiments

The key insights that brought us to just shy of 90% test accuracy are as follows, roughly in order, with approximate
test accuracies achieved as reported by Kaggle:
- 50%: Base ResNet-18 model
- 80%: Better learning rate and schedule, increased model depth to 34 layers
- 82%: Further increased model depth to 50 layers
- 84%: Pre-cropping the dataset to focus on the bird targets
- 86%: Data augmentation and increasing from 224x224 to 320x320 images
- 88%: Training our final submissions with our own validation split included in the training data
- 90.7%: Poor man's ensemble -- making final predictions via a weighted average of softmax scores from multiple networks
  (same architecture, slightly different training parameters and image sizes)

The following _did not_ improve performance:
- Increasing the input image size beyond 320x320
- Using a model deeper than ResNet-50
- Seemingly "smarter" forms of letterboxing rather than straight black background, such as filling with grey, or
  edge-extending. Keeping the original background in the "cropped" dataset to make square images may have had a minor
  effect.
- Extreme data augmentation. Rotation in particular always hurt performance, even in small amounts.
- Increasing or varying augmentation once the training process had completed several epochs.
- A deeper fully-connected feature extraction network at the end of the ResNet model. Performance was significantly
  worse. Freezing the ResNet conv blocks did not improve model performance either (with or without multiple FC layers).
- Using a multi-view predictor. This was likely unhelpful due to our pre-cropped dataset.
- Weighting poorly-performing classes higher part way through training
- Adding weight decay
- Adding dropout before the final layer

Here are our training plots for a model that is representative of our approach. From left to right, top to bottom:
batch training loss, epoch training accuracy, epoch validation accuracy, epoch validation loss.

<img src="charts/Batch_Train_Loss.svg" width="400" height="400" title="Batch training loss"/>
<img src="charts/Epoch_Train_Acc.svg" width="400" height="400" title="Epoch training accuracy"/>
<img src="charts/Epoch_Val_Acc.svg" width="400" height="400" title="Epoch validation accuracy"/>
<img src="charts/Epoch_Val_Loss.svg" width="400" height="400" title="Epoch validation loss"/>

## Challenges, next steps and future work

### Improving the detection system

As mentioned above, the detection pre-processing step is not perfect. It is sometimes incorrect, which can introduce
systemic errors in the dataset seen by the classifier, and may bias against particular classes of birds. There are a
few potential improvements to consider:

- Combine the detector and classifier. This could be done either as one model, or with two models run in tandem.
  This would enable better data augmentation for the classifier (by applying variance to the detection box)
  and perhaps cut down on biasing side-effects by allowing for fine-tuning on our task.
- Replace the detector with a different mechanism that achieves a similar goal. The intent was to focus on specific
  parts of the image. It may be sensible to use a self-attention layer instead; attention on image tiles has been shown
  to work well on classification problems of this sort. We did not pursue this approach but imagine it would be
  effective.

### Further augmentation


Our current models are profoundly overfit. They achieve 99.9% accuracy on the training set while scoring ~88% on
validation. This isn't necessarily a problem, but likely means that the model could be doing a better job if we took
better advantage of the training data available or were able to introduce more overall. Increasing normal data
augmentation only helps so much. Since the images typically have significant background around each bird that our model
doesn't see, introducing variability in the detection boxes may be valuable.

Dropout and weight decay _did not_ help with this problem, and it is unclear to us what more should be done here.

Below are 8 sample images after augmentation.

<img src="examples/aug_result.png" width="800" height="100" title="Post Augmentation Images"/>