import os
import sys

# avoid tkinter
import matplotlib
from torch.nn.modules import loss
from torch.nn.modules.loss import CrossEntropyLoss
matplotlib.use('agg')

import numpy as np
import matplotlib.pyplot as plt

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim.lr_scheduler import MultiplicativeLR
from tqdm import tqdm
from PIL import Image
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader, random_split

import math

from data import get_bird_data

BATCH_SIZE = 10

def import_data(in_folder):
    totens = transforms.ToTensor()
    trainset = ImageFolder(
        root=in_folder,
        loader=lambda fn: {'filename':fn, 'image': totens(Image.open(fn))}
    )
    trainloader = DataLoader(
        trainset,
        batch_size=BATCH_SIZE,
        shuffle=False,
        num_workers=0,
        collate_fn=lambda x: x
    )

    return trainloader

if __name__ == "__main__":
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    _, in_path, out_path = sys.argv

    data = import_data(in_path)
    model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)
    # model.to(device)
    model.eval()

    for i, batch in enumerate(tqdm(data), 0):
        images_dicts = [i for i, _ in batch]
        images = [d['image'] for d in images_dicts]
        paths = [d['filename'] for d in images_dicts]
        labels = [j for _, j in batch]
        
        images = images#.to(device)
        paths = paths

        outputs = model(images)
        masked_images = []
        to_pil = transforms.ToPILImage()

        j = 0
        for fn, orig_lbl, input, output in zip(paths, labels, images, outputs):
            labels = output['labels']
            scores = output['scores']
            filter = labels == 16

            boxes = output['boxes'][filter]
            n,_ = boxes.shape
            orig_im = to_pil(input)

            im_path = os.path.join(out_path, f'{orig_lbl}/{os.path.basename(fn)}')
            os.makedirs(os.path.dirname(im_path), exist_ok=True)

            if n == 0:
                orig_im.save(im_path)
                print(f'No bird in {fn}')
                continue

            l, t, r, b = boxes[0, 0].item(), boxes[0, 1].item(), boxes[0, 2].item(), boxes[0, 3].item()

            crop = orig_im.crop((l, t, r, b))
            crop.save(im_path)
