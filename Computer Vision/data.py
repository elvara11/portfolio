from typing import Dict

import os

import torch
from torch import Tensor
from torch.utils.data import Dataset, DataLoader, random_split
import torchvision.transforms as transforms
import torchvision.transforms.functional as tF
from torchvision.datasets import ImageFolder
from torchvision.transforms.transforms import RandomRotation

TRAIN_BATCH_SIZE = 48
VAL_BATCH_SIZE = 128+32
TEST_BATCH_SIZE = 1

TRAIN_WORKERS = 10
VAL_WORKERS = 10
TEST_WORKERS = 1

IMAGE_SIZE = 320
NUM_CLASSES = 555

USE_VAL = True
N_VIEWS = 5
USE_MULTIVIEW_VAL = False

class DatasetFromSubset(Dataset):
    def __init__(self, subset, transform=None):
        self.subset = subset
        self.transform = transform

    def __getitem__(self, index):
        x, y = self.subset[index]
        if self.transform:
            x = self.transform(x)
        return x, y

    def __len__(self):
        return len(self.subset)

class BirdData:
    def __init__(
            self,
            train: DataLoader,
            val: DataLoader,
            test: DataLoader,
            class_to_idx: Dict[int, int],
            idx_to_class: Dict[int, int],
            idx_to_name: Dict[int, str],
        ):
        self.train = train
        self.val = val
        self.test = test

        self.class_to_idx = class_to_idx
        self.idx_to_class = idx_to_class
        self.idx_to_name = idx_to_name

def ceil_div(num, denom):
    return (num + denom - 1) // denom

class AddGaussianNoise:
    def __init__(self, mean=0., std=1., max_deviation=None):
        self.std = std
        self.mean = mean
        self.max_deviation = max_deviation

    def __call__(self, tensor):
        dev = torch.randn(tensor.size()) * self.std
        if self.max_deviation is not None:
            dev = dev.clamp(-self.max_deviation, self.max_deviation)
        return tensor + dev + self.mean

    def __repr__(self):
        return self.__class__.__name__ + '(mean={0}, std={1})'.format(self.mean, self.std)


class Letterbox:
    def __init__(self, fill=0.5, padding_mode='constant'):
        self.fill = fill
        self.padding_mode = padding_mode
        if self.padding_mode == 'edge':
            self.fill = 0

    def __call__(self, image: Tensor):
        _, height, width = image.shape
        new_dim = max(height, width)

        new_space_vertical = new_dim - height
        new_space_horizontal = new_dim - width

        left_pad = new_space_horizontal//2
        top_pad = new_space_vertical//2
        right_pad = ceil_div(new_space_horizontal, 2)
        bottom_pad = ceil_div(new_space_vertical, 2)
        padding = (left_pad, top_pad, right_pad, bottom_pad)
        
        return tF.pad(image, padding, fill=self.fill, padding_mode=self.padding_mode)

def get_bird_data(root_dir: str) -> BirdData:
    transform_train = transforms.Compose([
        # transforms.RandomRotation(15),
        # Take crops from padded images
        transforms.RandomPerspective(distortion_scale=0.1),
        transforms.ColorJitter(brightness=0.05, contrast=0.05, saturation=0.05, hue=0.02),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        Letterbox(padding_mode='edge'),
        transforms.Resize(IMAGE_SIZE+32),
        transforms.RandomCrop(IMAGE_SIZE, padding=1, padding_mode='edge'),
        AddGaussianNoise(mean=0, std=.01),
        # transforms.GaussianBlur(5,sigma=(0.01,2.5)),
        transforms.RandomHorizontalFlip(),    # 50% of time flip image along y-axis,
    ])

    transform_test = transforms.Compose([
        # transforms.RandomCrop(IMAGE_SIZE, padding=4, padding_mode='edge'),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        Letterbox(padding_mode='edge'),
        transforms.Resize(IMAGE_SIZE),
    ])

    transform_val_multiview = None

    dataset = ImageFolder(
        root=os.path.join(root_dir, 'train')
    )
    num_train = int(len(dataset) * 0.8) if USE_VAL else len(dataset)
    trainset, valset = random_split(dataset, [num_train, len(dataset) - num_train], generator=torch.Generator().manual_seed(42))

    trainset_augmented = DatasetFromSubset(trainset, transform_train)
    valset_augmented = DatasetFromSubset(valset, transform_val_multiview if USE_MULTIVIEW_VAL else transform_test)

    trainloader = DataLoader(
        trainset_augmented,
        batch_size=TRAIN_BATCH_SIZE,
        shuffle=True,
        num_workers=TRAIN_WORKERS,
        drop_last=True,
        pin_memory=True
    )

    valloader = None
    if USE_MULTIVIEW_VAL:
        valloader = DataLoader(
            valset_augmented,
            batch_size=VAL_BATCH_SIZE,
            shuffle=True,
            num_workers=VAL_WORKERS,
            pin_memory=True,
            collate_fn=lambda x : x
        )
    else:
        valloader = DataLoader(
            valset_augmented,
            batch_size=VAL_BATCH_SIZE,
            shuffle=True,
            num_workers=VAL_WORKERS,
            pin_memory=True
        )

    testloader = None

    classes = open(os.path.join(root_dir, "names.txt")).read().strip().split("\n")
    class_to_idx = trainset.dataset.class_to_idx
    class_to_idx_int = {int(k): int(v) for k, v in class_to_idx.items()}
    idx_to_class = {int(v): int(k) for k, v in class_to_idx.items()}
    idx_to_name = {k: classes[v] for k, v in idx_to_class.items()}

    return BirdData(trainloader, valloader, testloader, class_to_idx_int, idx_to_class, idx_to_name)
