import os
import sys

# avoid tkinter
import matplotlib
from torch.nn.modules import loss
from torch.nn.modules.loss import CrossEntropyLoss
from torch.nn.modules.module import Module
matplotlib.use('agg')

import numpy as np
import matplotlib.pyplot as plt

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim.lr_scheduler import MultiplicativeLR
from torch.utils.tensorboard import SummaryWriter

from tqdm import tqdm

import math

from data import BirdData, AddGaussianNoise, Letterbox, get_bird_data, NUM_CLASSES, IMAGE_SIZE, USE_VAL, N_VIEWS, USE_MULTIVIEW_VAL

# a map of classes (folder names in the crop2 dataset) which should be "boosted" in priority, mapping to their relative weight.
# weight of "1" is default if not in the map.
CLASS_WEIGHTS = {}

def get_lr(optim):
    for param_group in optim.param_groups:
        return param_group['lr']

def train(net, data: BirdData, epochs, start_epoch, lr, state, checkpoint_path, logger):
    print("training...")
    net.to(device)
    net.train()

    train_dataloader = data.train
    val_dataloader = data.val

    train_losses = []
    log_losses = []
    val_losses = []
    train_accs = []
    val_accs = []

    if len(CLASS_WEIGHTS) > 0:
        print (data.class_to_idx)
        boost_indices = { data.class_to_idx[class_num]: rate for class_num, rate in CLASS_WEIGHTS.items() }
        class_weights = torch.ones(NUM_CLASSES)
        boost_indices_keys, boost_indices_values = map(list, zip(*boost_indices.items()))
        class_weights[boost_indices_keys] = torch.tensor(boost_indices_values)
        class_weights = class_weights.to(device)

    criterion = nn.CrossEntropyLoss(weight=class_weights if len(CLASS_WEIGHTS) > 0 else None)
    optimizer = optim.Adam(net.parameters(), lr=lr)

    lmbda = lambda epoch: 0.85
    scheduler = MultiplicativeLR(optimizer, lr_lambda=lmbda)

    # Load previous training state
    if state:
        net.load_state_dict(state['net'])
        optimizer.load_state_dict(state['optimizer'])
        start_epoch = state['epoch'] + 1
        train_losses = state['train_losses']
        scheduler.load_state_dict(state['scheduler'])

    train_len = len(train_losses)
    print('start lr', lr)
    if USE_VAL:
        acc, val_loss = accuracy_loss(device, net, val_dataloader, logger, train_len, USE_MULTIVIEW_VAL)
        print(f"Pre-Epoch 0: final validation accuracy {acc*100:.2f}%, validation loss {val_loss}")

        acc, val_loss = accuracy_loss(device, net, train_dataloader, logger, train_len, USE_MULTIVIEW_VAL, is_train=True)
        print(f"Pre-Epoch 0: final train accuracy {acc*100:.2f}%, train loss {val_loss}")

    for epoch in range(start_epoch, epochs):
        
        net.train()

        train_total = 0
        train_correct = 0
        train_loss = 0

        for i, batch in enumerate(tqdm(train_dataloader), 0):
            inputs, labels = batch[0].to(device), batch[1].to(device)

            optimizer.zero_grad()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            _, predicted = torch.max(outputs.data, 1)
            total = labels.size(0)
            correct = (predicted == labels).sum().item()
            train_acc = correct / total

            train_total += total
            train_correct += correct
    

            # train_accs.append()

            train_len += 1
            train_losses.append(loss.item())
            log_losses.append(math.log(loss.item()))
            train_loss += loss.item()

            logger.add_scalar("Batch/Train/Loss", loss.item(), train_len)
            logger.add_scalar("Batch/Train/Acc", train_acc, train_len)
            logger.add_scalar('Epoch', epoch, train_len)
            logger.add_scalar('LR', get_lr(optimizer), train_len)

            plt.clf() 
            plt.plot(train_losses, linewidth=0.1, markersize=0)
            plt.savefig('train_loss.png')

            plt.clf() 
            plt.plot(log_losses, linewidth=0.25, markersize=0)
            plt.savefig('train_log_loss.png')

        logger.add_scalar("Epoch/Train/Loss", train_loss, train_len)
        logger.add_scalar("Epoch/Train/Acc", train_correct / train_total, train_len)
        
        inputs = None
        labels = None
        outputs = None
        if USE_VAL:
            acc, val_loss = accuracy_loss(device, net, val_dataloader, logger, train_len, USE_MULTIVIEW_VAL)
            print(f"Epoch {epoch}: final validation accuracy {acc*100:.2f}%, validation loss {val_loss}")
            val_accs.append(acc)
            val_losses.append(val_loss)

            plt.clf()
            plt.plot(val_accs)
            plt.savefig('acc.png')

            plt.clf()
            plt.plot(val_losses)
            plt.savefig('val_loss')

        scheduler.step()
        

        if checkpoint_path:
            state = {
                'epoch': epoch,
                'net': net.state_dict(),
                'optimizer': optimizer.state_dict(),
                'train_losses': train_losses,
                'scheduler': scheduler.state_dict(),
                'idx_to_class': data.idx_to_class
            }
            torch.save(state, os.path.join(checkpoint_path, 'checkpoint-%d.pkl'%(epoch)))

    return train_losses

def smooth(x, size):
    return np.convolve(x, np.ones(size)/size, mode='valid')

def accuracy_loss(device, net, dataloader, logger, train_len, multiview, is_train=False):
    net.to(device)
    net.eval()
    correct = 0
    total = 0
    totalloss = 0
    loss_fn = nn.CrossEntropyLoss()
    with torch.no_grad():
        for batch in tqdm(dataloader):
            outputs = None
            images, labels = batch[0], batch[1]
            predicted = None
            if multiview:
                outputs, predicted = multiview_pred(device, net, batch, N_VIEWS)
                labels = [j for _, j in batch]
                labels = torch.tensor(labels).to(device)
            else:
                outputs = net(images.to(device))
                labels = labels.to(device)
                _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            totalloss += (loss_fn(outputs, labels) * labels.size(0)).item()
        images = None
        labels = None
        predicted = None
    if not is_train:
        logger.add_scalar("Epoch/Val/Loss", totalloss / total, train_len)
        logger.add_scalar("Epoch/Val/Acc", correct / total, train_len)
    else:
        logger.add_scalar("Epoch/Train/Loss", totalloss / total, train_len)
        logger.add_scalar("Epoch/Train/Acc", correct / total, train_len)
    return correct / total, totalloss / total

def multiview_pred(device, net, batch, n_views):
    net.to(device)
    net.eval()
    images = [i for i, _ in batch]

    random_trans = transforms.Compose([
        transforms.RandomPerspective(distortion_scale=0.1),
        transforms.ColorJitter(brightness=0.05, contrast=0.05, saturation=0.05, hue=0.02),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        Letterbox(padding_mode='edge'),
        transforms.Resize(IMAGE_SIZE+32),
        transforms.RandomCrop(IMAGE_SIZE, padding=1, padding_mode='edge'),
        AddGaussianNoise(mean=0, std=.01),
        transforms.RandomHorizontalFlip(),
    ])

    noaug_trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        Letterbox(padding_mode='edge'),
        transforms.Resize(IMAGE_SIZE),
    ])

    noaug_images = torch.zeros((len(images), 3, IMAGE_SIZE, IMAGE_SIZE))
    for i, im in enumerate(images):
        noaug_images[i] = noaug_trans(im)

    avg_output = net(noaug_images.to(device))
    _, best_pred = torch.max(avg_output.data, 1)
    confidence = torch.zeros(len(images))
    for j in range(len(images)):
        confidence[j] = avg_output[j, j]
    for i in range(n_views):
        random_images = torch.zeros((len(images), 3, IMAGE_SIZE, IMAGE_SIZE))
        for i, im in enumerate(images):
            random_images[i] = random_trans(im)
        outputs = net(random_images.to(device))
        _, predicted = torch.max(outputs.data, 1)
        cur_conf = torch.zeros(len(images))
        for j in range(len(images)):
            cur_conf[j] = outputs[j, j]
        for j in range(len(images)):
            if cur_conf[j] > confidence[j]:
                best_pred[j] = predicted[j]
        avg_output += outputs
    avg_output = avg_output / (n_views + 1)
    return avg_output, best_pred

def freeze_model(net):
    for child in list(net.children())[:-1]:
        for param in child.parameters():
            param.requires_grad = False

def unfreeze_model(net):
    for child in list(net.children())[:-1]:
        for param in child.parameters():
            param.requires_grad = True

def create_model() -> Module:
    # resnet = torch.hub.load('pytorch/vision:v0.6.0', 'resnet18', pretrained=True)
    # resnet = torch.hub.load('pytorch/vision:v0.6.0', 'resnet34', pretrained=True)
    resnet = torch.hub.load('pytorch/vision:v0.6.0', 'resnet50', pretrained=True)
    mid_size = 1024
    #resnet.fc = nn.Sequential(nn.Linear(2048, mid_size), nn.ReLU(), nn.Linear(mid_size, NUM_CLASSES))
    resnet.fc = nn.Sequential(nn.Linear(2048, NUM_CLASSES))

    return resnet

def predict(net, dataloader, ofname, device):
    out = open(ofname, 'w')
    out.write("path,class\n")
    net.to(device)
    net.eval()
    correct = 0
    total = 0
    with torch.no_grad():
        for i, (images, labels) in enumerate(dataloader, 0):
            print(i, end='\r')
            images, labels = images.to(device), labels.to(device)
            outputs = net(images)
            _, predicted = torch.max(outputs.data, 1)
            fname, _ = dataloader.dataset.samples[i]
            out.write("test/{},{}\n".format(fname.split('/')[-1], data.idx_to_class[predicted.item()]))
    out.close()

def create_new_logdir(log_root_dir : str, prefix : str):
    log_idx = 0
    while True:
        log_dir = os.path.join(log_root_dir, prefix + str(log_idx))
        if (os.path.isdir(log_dir)):
            log_idx += 1
            continue
        os.mkdir(log_dir)
        return log_dir

if __name__ == "__main__":
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    _, data_root_dir, log_root_dir = sys.argv

    log_dir = create_new_logdir(log_root_dir, prefix="3xAUG__")

    logger = SummaryWriter(log_dir=log_dir, flush_secs=30)
    data = get_bird_data(data_root_dir)

    # dataiter = iter(data.train)
    # images, labels = dataiter.next()
    # images = images[:8]
    # print(images.size())

    # # show images
    # torchvision.utils.save_image(torchvision.utils.make_grid(images), 'samples_images_crop.png')
    # # print labels
    # print("Labels:" + ', '.join('%9s' % data.idx_to_name[labels[j].item()] for j in range(4)))

    resnet = create_model()

    # schedule = {0:0.005, 3: 0.0002, 6: 0.0001, 9: 0.00005}
    # schedule = {0:0.002, 3: 0.001, 6: 0.0005, 9: 0.00025}  # original resnet 18 schedule - discontinued

    resume_state = None
    # resume_state = torch.load("checkpoints_crop_unfrozen/checkpoint-7.pkl")
    # freeze_model(resnet)
    train_losses = train(resnet, data, epochs=5, start_epoch=0, lr=.00015, state=resume_state, checkpoint_path='/tmp/kaelinl_birds/checkpoints_crop/', logger=logger)
    # unfreeze_model(resnet)
    # train_losses = train(resnet, data.train, data.val, epochs=20, start_epoch=8, lr=0.0001, state=resume_state, checkpoint_path='checkpoints_crop_unfrozen/')
    # plt.clf()
    # plt.plot(smooth(train_losses,50))
    # plt.savefig('loss_plot_end.png')

    # resnet.load_state_dict(resume_state['net'])
    # predict(resnet, data.test, "pred_joe.csv", device)
