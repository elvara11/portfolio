import sys
import os
import posixpath
from torch import Tensor
from torch.utils.data.dataloader import DataLoader

from torchvision.datasets.folder import ImageFolder
import torchvision.transforms as transforms
import torch
from torchvision.transforms.transforms import CenterCrop

from tqdm import tqdm

from birds import create_model

from PIL import Image

from data import get_bird_data, Letterbox

if __name__ == "__main__":
    _, checkpoint, data_root_dir = sys.argv

    checkpoint_data = torch.load(checkpoint)
    model = create_model()
    model.load_state_dict(checkpoint_data['net'])

    idx_to_class = checkpoint_data['idx_to_class']

    model.to('cuda')

    model.eval()

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        # broke, yo
        Letterbox(),
        transforms.Resize(320),
    ])

    test_set = ImageFolder(
        root=os.path.join(data_root_dir, 'test'),
        transform=transform
    )

    test_loader = DataLoader(
        test_set,
        batch_size=8,
        shuffle=False,
        num_workers=10
    )

    csv_entries = []
    csv_entries.append("path,class")

    for batch_i, (images, _) in enumerate(tqdm(test_loader)):
        images_gpu = images.to('cuda')
        predictions = model(images_gpu)
        _, class_predictions = torch.max(predictions.data, 1)

        for sample_i in range(predictions.shape[0]):
            abs_i = batch_i * test_loader.batch_size + sample_i
            file_name, _ = test_loader.dataset.samples[abs_i]
            class_prediction = idx_to_class[class_predictions[sample_i].item()]
            class_prediction = idx_to_class[class_prediction]
            correct_file_name = file_name.replace('\\0\\', '/')

            image_path = os.path.relpath(correct_file_name, data_root_dir)
            image_path = posixpath.normpath(image_path.replace('\\', '/'))
            # Kaggle scoring uses old/incorrect directory structure paths
            output_path = image_path.replace("/0/", "/")
            csv_entries.append(f"{output_path},{class_prediction}")

    with open("predictions.csv", "w") as f:
        f.write('\n'.join(csv_entries))
