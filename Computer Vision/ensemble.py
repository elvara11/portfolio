import sys
import os
import posixpath
from torch import Tensor
from torch.utils.data.dataloader import DataLoader

from torchvision.datasets.folder import ImageFolder
import torchvision.transforms as transforms
import torch
from torchvision.transforms.transforms import CenterCrop

from tqdm import tqdm

from birds import create_model

from PIL import Image

from data import get_bird_data, Letterbox

if __name__ == "__main__":
    _, checkpoint, data_root_dir = sys.argv

    checkpoint_data = torch.load(checkpoint)

    idx_to_class = checkpoint_data['idx_to_class']

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        # broke, yo
        Letterbox(),
        transforms.Resize(512),
    ])

    test_set = ImageFolder(
        root=os.path.join(data_root_dir, 'test'),
        transform=transform
    )

    test_loader = DataLoader(
        test_set,
        batch_size=1,
        shuffle=False,
        num_workers=10
    )

    map_tensor = torch.LongTensor([idx_to_class[i] for i in range(555)])

    csv_entries = []
    csv_entries.append("path,class")

    t512_u = torch.load("tensor512_uncrop.pkl")
    print(t512_u.shape)

    t512 = torch.load("tensor512.pkl")
    print(t512.shape)

    t384 = torch.load("tensor384.pkl")
    print(t384.shape)
    
    t320 = torch.load("tensor320.pkl")
    print(t320.shape)
    
    softmax = torch.nn.Softmax(dim=0)

    for batch_i in range(10000):
        print('\r', batch_i, end='')
        p_512u = t512_u[batch_i]
        p_512 = t512[batch_i]
        p_384 = t384[batch_i]
        p_320 = t320[batch_i]

        p_512u = p_512u[map_tensor]
        p_512 = p_512
        p_384 = p_384
        p_320 = p_320
        
        s_512u = softmax(p_512u)
        s_512 = softmax(p_512) 
        s_384 = softmax(p_384) 
        s_320 = softmax(p_320)

        class_predictions = torch.argmax(s_512 + s_320 + s_384 + s_512u)

        for sample_i in range(1):
            abs_i = batch_i * test_loader.batch_size + sample_i
            file_name, _ = test_loader.dataset.samples[abs_i]
            class_prediction = idx_to_class[class_predictions.item()]
            class_prediction = idx_to_class[class_prediction]
            correct_file_name = file_name.replace('\\0\\', '/')

            image_path = os.path.relpath(correct_file_name, data_root_dir)
            image_path = posixpath.normpath(image_path.replace('\\', '/'))
            # Kaggle scoring uses old/incorrect directory structure paths
            output_path = image_path.replace("/0/", "/")
            csv_entries.append(f"{output_path},{class_prediction}")

    with open("predictions_no384.csv", "w") as f:
        f.write('\n'.join(csv_entries))
