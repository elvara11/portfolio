import sys
import os
import posixpath
from torch import Tensor
from torch.utils.data.dataloader import DataLoader

from torchvision.datasets.folder import ImageFolder
import torchvision.transforms as transforms
import torch
from torchvision.transforms.transforms import CenterCrop

from tqdm import tqdm

from birds import create_model

from PIL import Image

from data import get_bird_data, Letterbox

if __name__ == "__main__":
    _, checkpoint, data_root_dir = sys.argv

    checkpoint_data = torch.load(checkpoint, map_location='cpu')
    model = create_model()
    model.load_state_dict(checkpoint_data['net'])

    idx_to_class = checkpoint_data['idx_to_class']

    model.to('cuda')

    model.eval()

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        # broke, yo
        Letterbox(),
        transforms.Resize(320),
    ])

    test_set = ImageFolder(
        root=os.path.join(data_root_dir, 'test'),
        transform=transform
    )

    test_loader = DataLoader(
        test_set,
        batch_size=1,
        shuffle=False,
        num_workers=10
    )

    csv_entries = []
    csv_entries.append("path,class")

    big_preds = []

    for batch_i, (images, _) in enumerate(tqdm(test_loader)):
        images_gpu = images.to('cuda')
        predictions = model(images_gpu)
        big_preds.append(predictions.data[0].cpu())
       

    big_pred_T = torch.stack(big_preds)
    torch.save(big_pred_T, "tensor512_uncrop.pkl")


