# Portfolio

## Repo Contents

All of the contents are my work. Some are class projects, where the ones included were projects with minimal boilerplate and template code. Others are personal projects.

### Networking

Python projects for networking class.

Project 1 is a TCP/UDP socket networking puzzle, where the goal is to communicate with a remote server and extract information according to a protocol. The program implements the protocol at a low-level, creating the byte-level payloads and manually managing the socket connections. It includes a server built to handle the other side of the protocol, using socket servers and multithreading to handle multiple clients simultaneously.

Project 2 is a simple HTTP proxy server written on top of the python TCP socket library.

### Machine Learning

Python solutions for machine learning homeworks. Most involve doing some machine learning task and then generating charts to show output performance.

### Computer Vision

Computer vision final project in python using transfer learning and ensemble models to build a very accurate classifier for the NA Birds dataset, which consists of photos of birds classified into hundreds of different species.

### Digital Logic

System Verilog projects in digital logic, building up to a complete implementation of the game Snake on an FPGA, using a grid LED display.

### StyleGAN2 Replication

Replication of the StyleGAN2 architecture in PyTorch from [this paper](https://arxiv.org/abs/1912.04958). Most work was focused on the generator, with a partially built discrimator. The generator supports importing weights from a pretrained StyleGAN2 network using the official repo.

### Propositional Logic Simplifier

Propositional logic simplifier built in C# that, given an input expression, parses it as logic and uses logical equivalences as patterns to build a graph of equivalent expressions. By enumerating huge numbers of possible expression trees, we will see if there are any easily reachable simpler expression forms. This mechanism is an extremely inefficient but also simpler version of the EGraph data structure.

## Other Work

### Reincarnate: CAD Decompiler Research

Research project published at ICFP 2018 I participated in that used program syntesis techniques to find constructive solid geometry CAD programs that generate a given triangle mesh. [Paper](http://reincarnate.uwplse.org/papers/icfp18.pdf), [website](http://reincarnate.uwplse.org/index.html), [artifact](https://github.com/uwplse/reincarnate-aec). Project written in OCaml. I started working after the paper was submitted, but did a bunch of work on the project before the conference. Most of the work was in implementing additional algorithms to improve performance.

### Szalinski: Structured CAD Program Synthesis

Research project published at PLDI 2020 focused on using a simple lambda calculus language to express complex constructive solid geometery CAD representations as programs with functions and loops. This project was in Rust. [Paper](https://dl.acm.org/doi/10.1145/3385412.3386012), [artifact](https://github.com/uwplse/szalinski).

### Gayatri: Parallel Toolpath Generation

Research project published at SFF Symposium 2021 focused on taking reincarnate the other direction, and making better mesh-to-toolpath compilers for generating 3D printer plans from a given model. This project focused on parallel toolpath generation, i.e. how to leverage 3D printers with multiple nozzles to print models faster. I worked extensively on the toolpath compiler. [Paper](https://ztatlock.net/pubs/2021-sff-gayatri/paper.pdf).

### Ruler: Rewrite-Based Simplifier

Given an language evaluator and syntax description, ruler enumerates expressions in the language to find equivalences. It uses the equivalances it finds to reduce the size of the search space, allowing it to enumerate larger and larger expressions. Then in can generate a simple small set of rules that describes all of the rewrite rules it finds, and it can also use the found equivalences to simplify an expression in the target language. The paper was published in OOPSLA 2021 and won a Distinguished Paper Award. [Paper](https://dl.acm.org/doi/abs/10.1145/3485496), [artifact](https://zenodo.org/record/5459312).
