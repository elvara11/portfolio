module A
= struct

  type ineq =
  | LT
  | EQ
  | GT

  type t =
  | TA of string * t
  | TB of bool * t * t

  module ACacheMapKey
  = struct

    type t = t

    let compare a b =
      match cmp a b with
      | LT -> -1
      | EQ -> 0
      | GT -> 1

  end

  module CMap = Map.Make(ACacheMapKey)
  let cache_map = CMap.empty

  let cmp a b =
    match a with
    | TA -> (match b with
             | TA -> EQ
             | TB -> GT)
    | TB -> (match b with
             | TA -> LT
             | TB -> EQ)

end