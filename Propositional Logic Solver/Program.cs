﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;

namespace PropLogic
{
    class Program
    {
        static void Main(string[] args)
        {
            /*while (true)
            {
                Console.Write("Input proposition: ");
                GenerateTruthTable(ParseExpn(Console.ReadLine()));
                Console.WriteLine();
            }*/

            /*while (true)
            {
                Console.Write("Input proposition: ");
                Console.WriteLine(SATCheck(ParseExpn(Console.ReadLine())) ? "Proved!" : "Incorrect");
                Console.WriteLine();
            }*/


            List<Tuple<ExpnNode, ExpnNode>> rules = new List<Tuple<ExpnNode, ExpnNode>>();
            List<string> ruleStrs = new List<string>(System.IO.File.ReadAllLines("../../Rules.txt"));
            foreach (string s in ruleStrs)
            {
                if (s.Length == 0 || s.StartsWith("#"))
                    continue;
                List<string> ruleParts = new List<string>(s.Split(':'));
                if (ruleParts.Count != 2)
                    throw new Exception("Invalid rules file");

                ExpnNode ruleA = ParseExpn(ruleParts[0]);
                int freeA = ruleA.GetFreeVars().Count();
                ExpnNode ruleB = ParseExpn(ruleParts[1]);
                int freeB = ruleB.GetFreeVars().Count();

                if (freeA >= freeB)
                    rules.Add(new Tuple<ExpnNode, ExpnNode>(ruleA, ruleB));
                
                if (freeB >= freeA)
                    rules.Add(new Tuple<ExpnNode, ExpnNode>(ruleB, ruleA));
            }

            //rules.Add(new Tuple<ExpnNode, ExpnNode>(new ExpnNode(ExpnNode.BinopType.And, new ExpnNode("p"), new ExpnNode(ExpnNode.BinopType.Or, new ExpnNode("p"), new ExpnNode("q"))),
            //    new ExpnNode("p")));
            while (true)
            {
                Console.Write("Enter proposition: ");
                ExpnNode n = ParseExpn(Console.ReadLine());
                EGraph eg = new EGraph(n, rules);
                Console.Write("Node limit: ");
                int nodeLimit = int.Parse(Console.ReadLine());
                ExpnNode simp = eg.Simplify(nodeLimit);
                Console.WriteLine("Simplified: " + simp.ToString());

                Console.WriteLine();
                Console.WriteLine("----------------------");
                Console.WriteLine("Initial: " + n);
                Console.WriteLine("Initial cost: " + n.GetCost());
                Console.WriteLine();
                foreach (var t in eg.GetPath(simp))
                {
                    string ruleA = rules[t.Item1].Item1.ToString();
                    string ruleB = rules[t.Item1].Item2.ToString();
                    Console.WriteLine("Rule:= " + ruleA + "   :   " + ruleB);
                    Console.WriteLine("After: " + t.Item2);
                    Console.WriteLine("Cost: " + t.Item2.GetCost());
                    Console.WriteLine();
                }
                Console.WriteLine("Final cost: " + simp.GetCost());
                Console.WriteLine("----------------------");
            }
        }

        static bool SATCheck(ExpnNode expn)
        {
            var freeVars = expn.GetFreeVars();
            var freeVarsList = freeVars.ToList();

            for (ulong i = 0; i < (((ulong)1) << freeVarsList.Count); i++)
            {
                Dictionary<string, bool> bindings = new Dictionary<string, bool>();
                for (int j = 0; j < freeVarsList.Count; j++)
                {
                    bool val = ((i >> j) & ((ulong)1)) == (ulong)1;
                    bindings.Add(freeVarsList[j], val);
                }

                bool res = expn.Eval(bindings);
                int halfVars = freeVarsList.Count / 2;
                if (!res)
                    return false;
            }

            return true;
        }

        static void GenerateTruthTable(ExpnNode expn)
        {
            Console.WriteLine("Generating truth table for " + expn);
            Console.WriteLine();

            var freeVars = expn.GetFreeVars();
            var freeVarsList = freeVars.ToList();
            freeVarsList.ForEach(s => Console.Write(s + " | "));
            Console.WriteLine("Result");
            freeVarsList.ForEach(s => { for (int i = 0; i < s.Length + 3; i++) Console.Write("-"); });
            Console.WriteLine("------");

            for (ulong i = 0; i < (((ulong)1) << freeVarsList.Count); i++)
            {
                Dictionary<string, bool> bindings = new Dictionary<string, bool>();
                for (int j = 0; j < freeVarsList.Count; j++)
                {
                    bool val = ((i >> j) & ((ulong)1)) == (ulong)1;
                    bindings.Add(freeVarsList[j], val);
                    Console.Write((val ? "T" : "F") + " | ");
                }

                bool res = expn.Eval(bindings);
                Console.WriteLine(res ? "T" : "F");
            }
        }

        const string OPS = "^|>=&!";
        const string UNOPS = "!";
        const string BINOPS = "&|^>=";
        const string SKIP = "\t\n ";
        static ExpnNode ParseExpn(string expn)
        {
            var groupStack = new Stack<string>();
            var parenStack = new Stack<int>();
            string buildup = "";
            for (int i = 0; i < expn.Length; i++)
            {
                if (expn[i] == '(')
                    parenStack.Push(i);
                else if (expn[i] == ')')
                {
                    if (parenStack.Count <= 0)
                        throw new Exception("Unmatched parenthesis");

                    int start = parenStack.Pop();
                    if (parenStack.Count == 0)
                        groupStack.Push(expn.Substring(start + 1, i - start - 1));
                }
                else if (parenStack.Count == 0)
                {
                    if (OPS.Contains(expn[i]))
                    {
                        if (buildup.Length > 0)
                            groupStack.Push(buildup);
                        buildup = "";
                        groupStack.Push(expn[i].ToString());
                    }
                    else if (!SKIP.Contains(expn[i]))
                    {
                        buildup += expn[i];
                    }
                }
            }
            if (buildup.Length > 0)
                groupStack.Push(buildup);

            if (groupStack.Count == 0)
                return null;
            else if (groupStack.Count == 1)
            {
                string extr = groupStack.Pop();
                if (extr.Length == 1)
                {
                    if (extr == "T" || extr == "F")
                        return new ExpnNode(extr == "T" ? true : false);
                    else
                        return new ExpnNode(extr);
                }
                else
                    return ParseExpn(extr);
            }
            else if (groupStack.Count == 2)
            {
                string operand = groupStack.Pop();
                string op = groupStack.Pop();
                ExpnNode.UnopType ut;
                switch (op)
                {
                    case "!":
                        ut = ExpnNode.UnopType.Not;
                        break;
                    default:
                        throw new Exception("Invalid unop");
                }
                return new ExpnNode(ut, ParseExpn(operand));
            }
            else if (groupStack.Count == 3)
            {
                string operandB = groupStack.Pop();
                string op = groupStack.Pop();
                string operandA = groupStack.Pop();

                ExpnNode.BinopType bt;
                switch (op)
                {
                    case "&":
                        bt = ExpnNode.BinopType.And;
                        break;
                    case "|":
                        bt = ExpnNode.BinopType.Or;
                        break;
                    case "^":
                        bt = ExpnNode.BinopType.Xor;
                        break;
                    case ">":
                        bt = ExpnNode.BinopType.Imp;
                        break;
                    case "=":
                        bt = ExpnNode.BinopType.Bimp;
                        break;
                    default:
                        throw new Exception("Invalid binop type");
                }
                return new ExpnNode(bt, ParseExpn(operandA), ParseExpn(operandB));
            }
            else
                throw new Exception("Invalid parse string: too many groups");
        }
    }

    class EGraph
    {
        ENode Root;
        List<Tuple<ExpnNode, ExpnNode>> Rules;
        HashSet<ExpnNode> VisitedNodes;

        public EGraph(ExpnNode root, List<Tuple<ExpnNode, ExpnNode>> rules)
        {
            Root = new ENode(root);
            Rules = rules;
            VisitedNodes = new HashSet<ExpnNode>();
        }

        private ExpnNode ReplaceVars(ExpnNode rule, Dictionary<string, ExpnNode> bindings)
        {
            switch (rule.GetNodeType())
            {
                case ExpnNode.NodeType.Binop:
                    rule.SetChildA(ReplaceVars(rule.GetChildA(), bindings));
                    rule.SetChildB(ReplaceVars(rule.GetChildB(), bindings));
                    return rule;
                case ExpnNode.NodeType.Fixed:
                    return rule;
                case ExpnNode.NodeType.Unop:
                    rule.SetChildA(ReplaceVars(rule.GetChildA(), bindings));
                    return rule;
                case ExpnNode.NodeType.Variable:

                    return bindings[rule.GetVariable()];
                default:
                    throw new Exception("Invalid node");
            }
        }

        private List<ExpnNode> Rewrite(ExpnNode ruleA, ExpnNode ruleB, ExpnNode expn)
        {
            List<ExpnNode> rewrites = new List<ExpnNode>();
            Tuple<bool, Dictionary<string, ExpnNode>> equivTupA = expn.MatchEquiv(ruleA);
            if (equivTupA.Item1)
            {
                rewrites.Add(ReplaceVars(ruleB.Clone(), equivTupA.Item2));
            }
            /*Tuple<bool, Dictionary<string, ExpnNode>> equivTupB = expn.MatchEquiv(ruleB);
            if (equivTupB.Item1)
            {
                rewrites.Add(ReplaceVars(ruleA, equivTupB.Item2));
            }*/

            switch (expn.GetNodeType())
            {
                case ExpnNode.NodeType.Binop:
                    rewrites.AddRange(Rewrite(ruleA.Clone(), ruleB.Clone(), expn.GetChildA()).Select(n => { ExpnNode newNode = expn.Clone(); newNode.SetChildA(n); return newNode; }));
                    rewrites.AddRange(Rewrite(ruleA.Clone(), ruleB.Clone(), expn.GetChildB()).Select(n => { ExpnNode newNode = expn.Clone(); newNode.SetChildB(n); return newNode; }));
                    return rewrites;
                case ExpnNode.NodeType.Fixed:
                    return rewrites;
                case ExpnNode.NodeType.Unop:
                    rewrites.AddRange(Rewrite(ruleA.Clone(), ruleB.Clone(), expn.GetChildA()).Select(n => { ExpnNode newNode = expn.Clone(); newNode.SetChildA(n); return newNode; }));
                    return rewrites;
                case ExpnNode.NodeType.Variable:
                    return rewrites;
                default:
                    throw new Exception("Invalid node type");
            }
        }

        private List<ENode> Expand(List<ENode> active, int maxCost)
        {
            List<ENode> nextNodes = new List<ENode>();
            int counter = 0;
            int activeSize = active.Count;
            int lastPercent = 0;
            foreach (ENode e in active)
            {
                if (((counter * 100) / activeSize) != lastPercent)
                {
                    int newPercent = (counter * 100) / activeSize;
                    System.Diagnostics.Debug.WriteLine(newPercent + "%");
                    lastPercent = newPercent;
                }

                if (e.GetExpn().GetCost() > maxCost)
                    continue;
                for (int i = 0; i < Rules.Count; i++)
                {
                    Tuple<ExpnNode, ExpnNode> r = new Tuple<ExpnNode, ExpnNode>(Rules[i].Item1.Clone(), Rules[i].Item2.Clone());
                    foreach (ExpnNode node in Rewrite(r.Item1, r.Item2, e.GetExpn()))
                    {
                        if (!VisitedNodes.Contains(node))
                        {
                            ENode eNode = new ENode(node);
                            e.AddChild(eNode, i);
                            VisitedNodes.Add(node);
                            if (node.GetCost() < maxCost)
                                nextNodes.Add(eNode);
                        }
                    }
                }
                counter++;
            }
            return nextNodes;
        }

        private ExpnNode FindSimplest(List<ENode> nodes)
        {
            int minCost = Int32.MaxValue;
            ExpnNode min = null;
            foreach (ENode en in nodes)
            {
                ExpnNode n = en.GetExpn();
                if (n.GetCost() < minCost)
                {
                    minCost = n.GetCost();
                    min = n;
                }
            }

            return min;
        }

        private Tuple<int, double> CostStatistics(List<ENode> nodes)
        {
            ulong sum = 0;
            ulong sum2 = 0;
            foreach (ENode en in nodes)
            {
                ExpnNode n = en.GetExpn();
                ulong cost = (ulong)n.GetCost();
                sum += cost;
                sum2 += cost * cost;
            }

            int avg = (int)(sum / ((ulong)nodes.Count));
            int avg2 = (int)(sum2 / ((ulong)nodes.Count));
            double stddev = Math.Sqrt(avg2 - avg * avg);

            return new Tuple<int, double>(avg, stddev);
        }

        public ExpnNode Simplify(int nodeLimit)
        {
            Console.WriteLine("Simplifying...");
            Console.WriteLine();
            List<ENode> all = new List<ENode>();
            all.Add(Root);
            List<ENode> active = new List<ENode>();
            active.Add(Root);
            VisitedNodes.Add(Root.GetExpn());
            /*int counter = 0;
            while (active.Count > 0)
            {
                ExpnNode simplest = FindSimplest(active);
                int simplestCost = simplest.GetCost();
                var stats = CostStatistics(active);

                Console.WriteLine("Expansion step " + counter);
                Console.WriteLine("Next step nodes: " + active.Count);
                Console.WriteLine("Min cost: " + simplestCost);
                Console.WriteLine("Min expn: " + simplest);
                Console.WriteLine("Average cost: " + stats.Item1);
                Console.WriteLine("Stddev cost: " + stats.Item2);
                Console.Write("Max cost for next cycle: ");
                int cost = int.Parse(Console.ReadLine());
                Console.WriteLine();*/
                
            SimplePriorityQueue<ENode, int> nodeQueue = new SimplePriorityQueue<ENode, int>();
            nodeQueue.Enqueue(Root, Root.GetExpn().GetCost());
            int counter = 0;
            while (nodeQueue.Count > 0 && counter < nodeLimit)
            {
                ENode e = nodeQueue.Dequeue();
                for (int i = 0; i < Rules.Count; i++)
                {
                    Tuple<ExpnNode, ExpnNode> r = new Tuple<ExpnNode, ExpnNode>(Rules[i].Item1.Clone(), Rules[i].Item2.Clone());
                    foreach (ExpnNode node in Rewrite(r.Item1, r.Item2, e.GetExpn()))
                    {
                        if (!VisitedNodes.Contains(node))
                        {
                            ENode eNode = new ENode(node);
                            e.AddChild(eNode, i);
                            VisitedNodes.Add(node);
                            nodeQueue.Enqueue(eNode, node.GetCost());
                        }
                    }
                }
                counter++;
            }


                /*all.AddRange(active);
                counter++;
            }*/

            ExpnNode min = null;
            int minCost = Int32.MaxValue;
            foreach (ExpnNode node in VisitedNodes)
            {
                int cost = node.GetCost();
                if (cost < minCost)
                {
                    min = node;
                    minCost = cost;
                }
            }

            return min;
        }

        private List<Tuple<int, ExpnNode>> GetPathHelper(ENode cur, ExpnNode dest, int prevRule)
        {
            if (cur.GetExpn().Equiv(dest))
            {
                var newPath = new List<Tuple<int, ExpnNode>>();
                newPath.Add(new Tuple<int, ExpnNode>(prevRule, cur.GetExpn()));
                return newPath;
            }

            foreach (ENode child in cur.GetChildren())
            {
                int rule = cur.GetRule(child);
                List<Tuple<int, ExpnNode>> newPath = GetPathHelper(child, dest, rule);
                if (newPath.Count > 0)
                {
                    List<Tuple<int, ExpnNode>> path = new List<Tuple<int, ExpnNode>>();
                    path.Add(new Tuple<int, ExpnNode>(rule, child.GetExpn()));
                    path.AddRange(newPath);
                    return path;
                }
            }

            return new List<Tuple<int, ExpnNode>>();
        }

        public List<Tuple<int, ExpnNode>> GetPath(ExpnNode dest)
        {
            var path = GetPathHelper(Root, dest, -1);
            if (path.Count > 0)
                path.RemoveAt(path.Count - 1);

            return path;
        }
    }

    class ENode
    {
        List<ENode> Children;
        Dictionary<ENode, int> RuleUsed;
        ExpnNode Expn;

        public ENode(ExpnNode expn)
        {
            Expn = expn;
            Children = new List<ENode>();
            RuleUsed = new Dictionary<ENode, int>();
        }

        public List<ENode> GetChildren()
        {
            return Children;
        }

        public ExpnNode GetExpn()
        {
            return Expn;
        }

        public void AddChild(ENode child, int ruleUsed)
        {
            Children.Add(child);
            RuleUsed.Add(child, ruleUsed);
        }

        public int GetRule(ENode child)
        {
            return RuleUsed[child];
        }

        public override int GetHashCode()
        {
            return Expn.GetHashCode();
        }
    }

    class ExpnNode
    {
        public enum NodeType { Binop, Unop, Variable, Fixed };
        public enum BinopType { Or, And, Xor, Imp, Bimp };
        public enum UnopType { Not };

        private NodeType Type;
        private BinopType Binop;
        private UnopType Unop;
        string Variable;
        bool Fixed;

        ExpnNode ChildA;
        ExpnNode ChildB;

        public ExpnNode(BinopType bt, ExpnNode childA, ExpnNode childB)
        {
            Type = NodeType.Binop;
            Binop = bt;
            ChildA = childA;
            ChildB = childB;
        }

        public ExpnNode(UnopType ut, ExpnNode child)
        {
            Type = NodeType.Unop;
            Unop = ut;
            ChildA = child;
        }

        public ExpnNode(string var)
        {
            Type = NodeType.Variable;
            Variable = var;
        }

        public ExpnNode(bool fix)
        {
            Type = NodeType.Fixed;
            Fixed = fix;
        }

        private bool EvalBinop(Dictionary<string, bool> ExecContext)
        {
            switch (Binop)
            {
                case BinopType.And:
                    return ChildA.Eval(ExecContext) && ChildB.Eval(ExecContext);
                case BinopType.Bimp:
                    return ChildA.Eval(ExecContext) == ChildB.Eval(ExecContext);
                case BinopType.Imp:
                    return !ChildA.Eval(ExecContext) || ChildB.Eval(ExecContext);
                case BinopType.Or:
                    return ChildA.Eval(ExecContext) || ChildB.Eval(ExecContext);
                case BinopType.Xor:
                    return ChildA.Eval(ExecContext) ^ ChildB.Eval(ExecContext);
                default:
                    throw new Exception("Invalid binop");
            }
        }

        private bool EvalUnop(Dictionary<string, bool> ExecContext)
        {
            switch (Unop)
            {
                case UnopType.Not:
                    return !ChildA.Eval(ExecContext);
                default:
                    throw new Exception("Invalid unop");
            }
        }

        private bool EvalVariable(Dictionary<string, bool> ExecContext)
        {
            if (ExecContext.ContainsKey(Variable))
                return ExecContext[Variable];
            else
                throw new Exception("Unbound variable " + Variable);
        }

        public bool Eval(Dictionary<string, bool> ExecContext)
        {
            switch (Type)
            {
                case NodeType.Binop:
                    return EvalBinop(ExecContext);
                case NodeType.Fixed:
                    return Fixed;
                case NodeType.Unop:
                    return EvalUnop(ExecContext);
                case NodeType.Variable:
                    return EvalVariable(ExecContext);
                default:
                    throw new Exception("Invalid node type");
            }
        }

        public bool Equiv(ExpnNode node)
        {
            if (node.Type == Type)
            {
                switch (Type)
                {
                    case NodeType.Binop:
                        return (Binop == node.Binop) && ChildA.Equiv(node.ChildA) && ChildB.Equiv(node.ChildB);
                    case NodeType.Fixed:
                        return Fixed == node.Fixed;
                    case NodeType.Unop:
                        return (Unop == node.Unop) && ChildA.Equiv(node.ChildA);
                    case NodeType.Variable:
                        return Variable == node.Variable;
                    default:
                        throw new Exception("Invalid node type");
                }
            }
            else
                return false;
        }

        private bool MatchEquivHelper(ExpnNode pattern, Dictionary<string, ExpnNode> context)
        {
            if (pattern.Type == NodeType.Variable)
            {
                if (context.ContainsKey(pattern.Variable) && Equiv(context[pattern.Variable]))
                    return true;
                else if (context.ContainsKey(pattern.Variable))
                    return false;
                else
                {
                    context.Add(pattern.Variable, this);
                    return true;
                }
            }
            if (pattern.Type == Type)
            {
                switch (Type)
                {
                    case NodeType.Binop:
                        return (Binop == pattern.Binop)
                            && ChildA.MatchEquivHelper(pattern.ChildA, context)
                            && ChildB.MatchEquivHelper(pattern.ChildB, context);
                    case NodeType.Fixed:
                        return Fixed == pattern.Fixed;
                    case NodeType.Unop:
                        return (Unop == pattern.Unop) && ChildA.MatchEquivHelper(pattern.ChildA, context);
                    default:
                        throw new Exception("Invalid node type");
                }
            }
            else
                return false;
        }

        public Tuple<bool, Dictionary<string, ExpnNode>> MatchEquiv(ExpnNode pattern)
        {
            Dictionary<string, ExpnNode> bindings = new Dictionary<string, ExpnNode>();
            return new Tuple<bool, Dictionary<string, ExpnNode>>(MatchEquivHelper(pattern, bindings), bindings);
        }

        public NodeType GetNodeType()
        {
            return Type;
        }

        public BinopType GetBinopType()
        {
            if (Type != NodeType.Binop)
                throw new Exception("Node is not binop");

            return Binop;
        }

        public UnopType GetUnopType()
        {
            if (Type != NodeType.Unop)
                throw new Exception("Node is not binop");

            return Unop;
        }

        public void SetChildA(ExpnNode child)
        {
            if (Type != NodeType.Binop && Type != NodeType.Unop)
                throw new Exception("Node not binop or unop");

            ChildA = child;
        }

        public ExpnNode GetChildA()
        {
            if (Type != NodeType.Binop && Type != NodeType.Unop)
                throw new Exception("Node not binop or unop");

            return ChildA;
        }

        public void SetChildB(ExpnNode child)
        {
            if (Type != NodeType.Binop)
                throw new Exception("Node not binop");

            ChildB = child;
        }

        public ExpnNode GetChildB()
        {
            if (Type != NodeType.Binop)
                throw new Exception("Node not binop");

            return ChildB;
        }

        public string GetVariable()
        {
            if (Type != NodeType.Variable)
                throw new Exception("Node not variable");

            return Variable;
        }

        public HashSet<string> GetFreeVars()
        {
            switch (Type)
            {
                case NodeType.Binop:
                    var hsA = ChildA.GetFreeVars();
                    hsA.UnionWith(ChildB.GetFreeVars());
                    return hsA;
                case NodeType.Fixed:
                    return new HashSet<string>();
                case NodeType.Unop:
                    return ChildA.GetFreeVars();
                case NodeType.Variable:
                    var hs = new HashSet<string>();
                    hs.Add(Variable);
                    return hs;
                default:
                    throw new Exception("Invalid node type");
            }
        }

        private int GetBinopCost(BinopType bt)
        {
            switch (bt)
            {
                case BinopType.And:
                    return 1;
                case BinopType.Bimp:
                    return 15;
                case BinopType.Imp:
                    return 10;
                case BinopType.Or:
                    return 1;
                case BinopType.Xor:
                    return 1;
                default:
                    throw new Exception("Invalid binop type");
            }
        }

        private int GetUnopCost(UnopType ut)
        {
            switch (ut)
            {
                case UnopType.Not:
                    return 1;
                default:
                    throw new Exception("Invalid unop type");
            }
        }

        private bool CanCollapseBinop(BinopType bt)
        {
            switch (bt)
            {
                case BinopType.And:
                    return true;
                case BinopType.Bimp:
                    return false;
                case BinopType.Imp:
                    return false;
                case BinopType.Or:
                    return true;
                case BinopType.Xor:
                    return true;
                default:
                    throw new Exception("Invalid binop type");
            }
        }

        private int GetCostHelper(HashSet<ExpnNode> cached, BinopType? last)
        {
            cached.Add(this);
            switch (Type)
            {
                case NodeType.Binop:
                    int childACost = cached.Contains(ChildA) ? 0 : ChildA.GetCostHelper(cached, Binop);
                    int childBCost = cached.Contains(ChildB) ? 0 : ChildB.GetCostHelper(cached, Binop);
                    int binopCost = last == Binop && CanCollapseBinop(Binop) ? 0 : GetBinopCost(Binop);
                    return binopCost + childACost + childBCost;
                case NodeType.Fixed:
                    return 0;
                case NodeType.Unop:
                    int childCost = cached.Contains(ChildA) ? 0 : ChildA.GetCostHelper(cached, null);
                    return GetUnopCost(Unop) + childCost;
                case NodeType.Variable:
                    return 0;
                default:
                    throw new Exception("Invalid node type");
            }
        }

        public int GetCost()
        {
            return GetCostHelper(new HashSet<ExpnNode>(), null);
        }

        private string BinopStr()
        {
            switch (Binop)
            {
                case BinopType.And:
                    return "&";
                case BinopType.Or:
                    return "|";
                case BinopType.Bimp:
                    return "=";
                case BinopType.Imp:
                    return ">";
                case BinopType.Xor:
                    return "^";
                default:
                    throw new Exception("Invalid binop");
            }
        }

        private string UnopStr()
        {
            switch (Unop)
            {
                case UnopType.Not:
                    return "!";
                default:
                    throw new Exception("Invalid unop");
            }
        }

        public override string ToString()
        {
            switch (Type)
            {
                case NodeType.Binop:
                    return "(" + ChildA.ToString() + " " + BinopStr() + " " + ChildB.ToString() + ")";
                case NodeType.Fixed:
                    return Fixed ? "T" : "F";
                case NodeType.Unop:
                    return UnopStr() + ChildA.ToString();
                case NodeType.Variable:
                    return Variable;
                default:
                    throw new Exception("Invalid node type");
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is ExpnNode)
                return Equiv((ExpnNode)obj);
            else
                return false;
        }

        public ExpnNode Clone()
        {
            switch (Type)
            {
                case NodeType.Binop:
                    return new ExpnNode(Binop, ChildA.Clone(), ChildB.Clone());
                case NodeType.Fixed:
                    return new ExpnNode(Fixed);
                case NodeType.Unop:
                    return new ExpnNode(Unop, ChildA.Clone());
                case NodeType.Variable:
                    return new ExpnNode(Variable);
                default:
                    throw new Exception("Invalid node type");
            }
        }
    }
}
