

module Logic
= struct

  type binop =
  | Or
  | And

  type unop =
  | Not

  type t =
  | Fixed of bool
  | Variable of string
  | Binop of binop * t * t
  | Unop of unop * t

  let var_lookup s =
    match s with
    | "p" -> true
    | "q" -> false
    | _   -> false

  let app_binop s la lb =
    match s with
    | Or -> la || lb
    | And -> la && lb
  
  let app_unop s l =
    match s with
    | Not -> not l

  let rec compile expn =
    match expn with
    | Fixed b -> b
    | Variable s -> var_lookup s
    | Binop (b, la, lb) -> app_binop b (compile la) (compile lb)
    | Unop (u, l) -> app_unop u (compile l)
end