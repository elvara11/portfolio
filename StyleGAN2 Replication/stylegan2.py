from typing import List, Tuple, Union
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.conv import Conv2d
from torch.nn.parameter import Parameter
import torch.nn.utils
from torch.utils.data.dataloader import DataLoader
import torch.autograd
import torch.optim
import math
import numpy as np
from torchvision import transforms
import torchvision
import argparse

from torchvision.transforms.functional import pad

class EqualizedWeight(nn.Module):
    def __init__(self, shape: List[int]):
        super().__init__()

        self.c = 1 / math.sqrt(np.prod(shape[1:]))
        self.weight = Parameter(torch.randn(shape))
    
    def forward(self):
        return self.c * self.weight

class EqualizedLinear(nn.Module):
    def __init__(self, in_features : int, out_features : int, bias : float = 1., weight_gain = 1., bias_gain=1.):
        super().__init__()

        self.weight = EqualizedWeight([out_features, in_features])
        self.bias = Parameter(torch.ones(out_features) * bias, requires_grad=True)
        self.weight_gain = weight_gain
        self.bias_gain = bias_gain

    def forward(self, x):
        return F.linear(x, self.weight() * self.weight_gain, bias = self.bias * self.bias_gain)

class EqualizedConv2d(nn.Module):
    def __init__(self, in_features : int, out_features : int, kernel_size : int, padding : int = 0):
        super().__init__()

        self.weight = EqualizedWeight([out_features, in_features, kernel_size, kernel_size])
        self.bias = Parameter(torch.ones(out_features), requires_grad=True)
        self.padding = padding
    
    def forward(self, x):
        return F.conv2d(x, self.weight(), bias=self.bias, padding=self.padding)

class LeakyReLUGain(nn.Module):
    def __init__(self, negative_slope=0.2, in_place=True, gain=np.sqrt(2)):
        super().__init__()
        self.lrelu = nn.LeakyReLU(negative_slope=negative_slope, inplace=in_place)
        self.gain = gain
    
    def forward(self, x):
        return self.lrelu(x) * self.gain

class Smooth(nn.Module):
    def __init__(self):
        super().__init__()

        kernel = [[1, 3, 3, 1],
                  [3, 9, 9, 3],
                  [3, 9, 9, 3],
                  [1, 3, 3, 1]]
        kernel = torch.tensor(kernel, dtype=torch.float)
        kernel /= kernel.sum()
        self.kernel = Parameter(kernel, requires_grad=False)
    
    def forward(self, x : torch.Tensor):
        # x : N x C x H x W
        # kernel: 3 x 3
        batch_size, num_channels, height, width = x.shape
        x = x.view(-1, 1, height, width)
        x = F.pad(x, [2, 1, 2, 1])
        x = F.conv2d(x, self.kernel[None, None, :, :])
        return x.view(batch_size, num_channels, height, width)

# class Upsample(nn.Module):
#     def __init__(self):
#         super().__init__()
#         self.upscale = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=False)
#         self.smooth = Smooth()
    
#     def forward(self, x : torch.Tensor):
#         return self.smooth(self.upscale(x))

class Upsample(nn.Module):
    def __init__(self):
        super().__init__()
        self.smooth = Smooth()

    def forward(self, x : torch.Tensor, out_pad : int = 0):
        batch_size, num_channels, in_height, in_width = x.shape

        # Double input size with 0s
        x = x.reshape(batch_size, num_channels, in_height, 1, in_width, 1)
        x = F.pad(x, (0, 1, 0, 0, 0, 1))
        x = x.reshape(batch_size, num_channels, in_height * 2, in_width * 2)

        # Apply padding
        x = F.pad(x, [out_pad, out_pad, out_pad, out_pad])

        # Smooth out result
        x = self.smooth(x)
        x = x * 4 # Gain compensates for extra 0s
        return x

class Downsample(nn.Module):
    def __init__(self):
        super().__init__()
        self.smooth = Smooth()
    
    def forward(self, x : torch.Tensor):
        x = self.smooth(x)
        x = F.interpolate(x, scale_factor=0.5, mode='bilinear', align_corners=False)
        return x

class LatentMapper(nn.Module):
    def __init__(self, d_latent : int, n_layers : int = 8, lr_multiplier=0.01):
        super().__init__()

        self.d_latent = d_latent
        self.n_layers = n_layers
        blocks = []
        activations = []
        for i in range(n_layers):
            blocks.append(EqualizedLinear(d_latent, d_latent, weight_gain=lr_multiplier, bias_gain=lr_multiplier))
            activations.append(LeakyReLUGain(0.2, True))
        self.blocks = nn.ModuleList(blocks)
        self.activations = nn.ModuleList(activations)
    
    def normalize_2nd_moment(self, x, dim=1, eps=1e-8):
        return x * (x.square().mean(dim=dim, keepdim=True) + eps).rsqrt()

    def forward(self, z : torch.Tensor):
        # x = F.normalize(z, p=2, dim=1) # N x d_latent
        x = self.normalize_2nd_moment(z)
        for i in range(self.n_layers):
            x = self.blocks[i](x)
            x = self.activations[i](x)
        return x

class ModulatedConv2d(nn.Module):
    def __init__(self, in_features: int, out_features: int, kernel_size: int,
                 demodulate: bool = True, eps: float = 1e-8, padding=1):
        super().__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.kernel_size = kernel_size
        self.demodulate = demodulate
        self.eps = eps
        self.padding = padding

        # self.weights = Parameter(torch.randn(out_features, in_features, kernel_size, kernel_size), requires_grad=True)
        self.weights = EqualizedWeight([out_features, in_features, kernel_size, kernel_size])

    def forward(self, x : torch.Tensor, style : torch.Tensor):
        # X:                                         batch_size            x in_feat              x height x width
        # style:                                     batch_size            x in_feat
        batch_size, _, height, width = x.shape
        style = style[:, None, :, None, None]      # batch_size            x 1        x in_feat   x 1      x 1
        weights = self.weights()[None, :, :, :, :] # 1                     x out_feat x in_feat   x k_size x k_size
        weights = weights * style                  # batch_size            x out_feat x in_feat   x k_size x k_size

        if self.demodulate:
                                                   # batch_size            x out_feat x 1         x 1      x 1
            rsigma = torch.rsqrt((weights ** 2).sum(dim=(2, 3, 4), keepdim=True) + self.eps) 
            weights = weights * rsigma             # batch_size            x out_feat x in_feat   x k_size x k_size

        x = x.reshape(1, -1, height, width)        # 1                     x batch_size * in_feat x height x width
                                                   # batch_size * out_feat x in_feat              x k_size x k_size
        weights = weights.reshape(batch_size * self.out_features, self.in_features, self.kernel_size, self.kernel_size)
        y = F.conv2d(x, weights, padding=self.padding, groups=batch_size)
        size_reduction = 2 * ((self.kernel_size // 2) - self.padding)
        return y.reshape(batch_size, self.out_features, height - size_reduction, width - size_reduction)

class StyleBlock(nn.Module):
    def __init__(self, size : int, d_latent : int, in_features : int, out_features : int, conv_clamp = None, up : bool = False):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.size = size


        self.up = up
        if self.up:
            self.us = Upsample()
        self.affine = EqualizedLinear(d_latent, in_features)
        self.mod_conv = ModulatedConv2d(in_features, out_features, 3, padding=(1 if not self.up else 0))
        self.bias = Parameter(torch.zeros(out_features), requires_grad=True)
        self.noise_strength = Parameter(torch.zeros([]))
        self.activation = LeakyReLUGain(0.2, True)
        self.noise_const = Parameter(torch.randn(self.size, self.size), requires_grad=False)
        self.conv_clamp = conv_clamp
    
    def forward(self, x : torch.Tensor, w : torch.Tensor, noise_mode='random'):
        batch_size = x.shape[0]
        style = self.affine(w)
        if self.up:
            x = self.us(x, out_pad=1) # Upscale with the padding for the conv in mind
        x = self.mod_conv(x, style)
        if noise_mode == 'random':
            noise = torch.randn(batch_size, 1, self.size, self.size, device=x.device).repeat((1, self.out_features, 1, 1)) * self.noise_strength
        if noise_mode == 'const':
            noise = (self.noise_const * self.noise_strength).unsqueeze(0).unsqueeze(0).repeat((batch_size, self.out_features, 1, 1))
        if noise_mode == 'none':
            noise = torch.zeros(batch_size, self.out_features, self.size, self.size)
        x += noise
        x += self.bias[None, :, None, None]
        x = self.activation(x)
        if not self.conv_clamp is None:
            clamp = self.conv_clamp * self.activation.gain
            x = x.clamp(-clamp, clamp)
        return x

class ToRGB(nn.Module):
    def __init__(self, size : int, d_latent : int, in_features : int, out_features : int, conv_clamp = None):
        super().__init__()
        self.affine = EqualizedLinear(d_latent, in_features)
        self.mod_conv = ModulatedConv2d(in_features, out_features, 1, demodulate=False, padding=0)
        self.bias = Parameter(torch.zeros(out_features), requires_grad=True)
        self.conv_clamp = conv_clamp
    
    def forward(self, x : torch.Tensor, w : torch.Tensor):
        style = self.affine(w)
        x = self.mod_conv(x, style)
        x += self.bias[None, :, None, None]
        if not self.conv_clamp is None:
            clamp = self.conv_clamp
            x = x.clamp(-clamp, clamp)
        return x

class GeneratorBlock(nn.Module):
    def __init__(self, d_latent : int, size : int, in_featues : int, out_features : int, rgb_features : int, n_blocks : int = 2, first_block : bool = False):
        super().__init__()

        blocks = [StyleBlock(size, d_latent, in_featues if i == 0 else out_features, out_features, up=(i==0 and not first_block)) for i in range(n_blocks)]
        self.blocks = nn.ModuleList(blocks)
        self.to_rgb = ToRGB(size, d_latent, out_features, rgb_features)
    
    def forward(self, x, w, **sb_args):
        for block in self.blocks:
            x = block(x, w, **sb_args)
        rgb = self.to_rgb(x, w)
        return x, rgb

class Generator(nn.Module):
    def __init__(self, log_resolution: int, d_latent: int, n_features: int = 32, max_features: int = 512, rgb_features : int = 3):
        super().__init__()

        features = [min(max_features, n_features * (2 ** i)) for i in range(log_resolution - 2, -1, -1)]
        self.n_blocks = len(features)
        self.init_constant = Parameter(torch.randn(features[0], 4, 4), requires_grad=True)

        blocks = [GeneratorBlock(d_latent, 4 * (2 ** i), features[0] if i == 0 else features[i - 1], features[i], rgb_features, 1 if i == 0 else 2, i == 0) for i in range(self.n_blocks)]
        self.blocks = nn.ModuleList(blocks)
        self.upsample = Upsample()

    def forward(self, w, eval_resolution=-1, **gb_args):
        batch_size = w.shape[1]
        expanded_constant = self.init_constant.unsqueeze(0).repeat((batch_size, 1, 1, 1))
        rgb = None
        x = expanded_constant
        n_layers = self.n_blocks if eval_resolution < 0 else min(int(math.log2(eval_resolution)) - 1, self.n_blocks)
        for i, block in enumerate(self.blocks):
            if i == n_layers:
                break
            new_x, new_rgb = block(x, w[i], **gb_args)
            rgb = self.upsample(rgb) + new_rgb if not rgb is None else new_rgb
            # if i < self.n_blocks - 1:
            #     x = self.upsample(new_x)
            x = new_x
        return rgb

class MinibatchStdDev(nn.Module):
    def __init__(self, group_size : int = 4, eps : float = 1e-8):
        super().__init__()
        self.group_size = 4
        self.eps = eps
    
    def forward(self, x):
        assert x.shape[0] % self.group_size == 0
        batch_size, _, height, width = x.shape
        grouped_x = x.view(self.group_size, -1)
        std = torch.sqrt(torch.var(grouped_x, dim=0) + self.eps)
        std = std.mean()
        std = std.view(1, 1, 1, 1).repeat((batch_size, 1, height, width))
        return torch.cat((x, std), dim=1)

class DiscriminatorBlock(nn.Module):
    def __init__(self, in_features, out_features):
        super().__init__()

        self.downsample = Downsample()
        self.block = nn.Sequential(
            EqualizedConv2d(in_features, out_features, 3, padding=1),
            LeakyReLUGain(0.2, True),
            EqualizedConv2d(out_features, out_features, 3, padding=1),
            LeakyReLUGain(0.2, True),
            self.downsample,
        )
        self.residual = nn.Sequential(
            self.downsample,
            EqualizedConv2d(in_features, out_features, 1)
        )
        self.scale = 1 / math.sqrt(2)
    
    def forward(self, x):
        residual = self.residual(x)
        x = self.block(x)
        return (x + residual) * self.scale

class Discriminator(nn.Module):
    def __init__(self, log_resolution: int, n_features: int = 64, max_features: int = 512, rgb_features : int = 3):
        super().__init__()

        self.from_rgb = nn.Sequential(
            EqualizedConv2d(rgb_features, n_features, 1),
            LeakyReLUGain(0.2, True),
        )
        features = [min(max_features, n_features * (2 ** i)) for i in range(log_resolution - 1)]
        n_blocks = len(features) - 1
        blocks = [DiscriminatorBlock(features[i], features[i + 1]) for i in range(n_blocks)]
        self.blocks = nn.Sequential(*blocks)
        self.std_dev = MinibatchStdDev()
        final_num_features = features[-1] + 1
        self.final_conv = EqualizedConv2d(final_num_features, final_num_features, 3, padding=0)
        self.final_lin = EqualizedLinear(final_num_features * 2 * 2, 1)
    
    def forward(self, x):
        batch_size = x.shape[0]
        # x -= 0.5 ???
        x = self.from_rgb(x)
        x = self.blocks(x)
        x = self.std_dev(x)
        x = self.final_conv(x)
        x = x.view(batch_size, -1)
        x = self.final_lin(x)
        return x

class GradientPenalty(nn.Module):
    def forward(self, x : torch.Tensor, d : torch.Tensor): # x are reals, d is D(x)
        batch_size = x.shape[0]
        grads, *_ = torch.autograd.grad(outputs=d, inputs=x, grad_outputs=torch.ones(d.shape, device=x.device), create_graph=True)
        grads = grads.reshape(batch_size, -1)
        norm = grads.norm(p=2, dim=-1)
        return (norm ** 2).mean()

class PathLengthPenalty(nn.Module):
    def __init__(self, beta : float):
        super().__init__()
        self.beta = beta
        self.steps = nn.Parameter(torch.tensor(0.), requires_grad=False)
        self.exp_sum_a = nn.Parameter(torch.tensor(0.), requires_grad=False)
    
    def forward(self, x : torch.Tensor, w : torch.Tensor):
        device = x.device
        image_size = x.shape[2] * x.shape[3]
        y = torch.randn(x.shape, device=device)
        output = (x * y).sum() / math.sqrt(image_size)
        grads, *_ = torch.autograd.grad(outputs=output, inputs=w, grad_outputs=torch.ones(output.shape, device=x.device), create_graph=True)
        norm = (grads ** 2).sum(dim=2).mean(dim=1).sqrt()

        if self.steps > 0:
            a = self.exp_sum_a / (1 - self.beta ** self.steps)
            loss = torch.mean((norm - a) ** 2)
        else:
            loss = norm.new_tensor(0)
        
        mean = norm.mean().detach()
        self.exp_sum_a.mul_(self.beta).add_(mean, alpha=1 - self.beta)
        self.steps.add_(1.)
        return loss

class StyleGAN2(LightningModule):
    def __init__(self, image_size: int, d_latent : int = 512, n_features_disc: int = 64, n_features_gen : int = 32, max_features: int = 512,
                 rgb_features : int = 3, map_n_layers = 8, lr : float=1e-3, map_lr  : float = 1e-5, adam_betas : Tuple[float, float] = (0., 0.99),
                 grad_penalty_interval : int = 4, grad_penalty_coefficient : float = 10., path_length_interval : int = 32, path_length_beta = 0.99,
                 start_path_penalty : int = 5_000, style_mixing_prob : float = 0.9):
        
        super().__init__()
        self.image_size = image_size
        self.rgb_features = rgb_features
        log_resolution = int(math.log2(image_size))

        self.d_latent = d_latent
        self.mapping_net = LatentMapper(d_latent, n_layers=map_n_layers)
        self.generator = Generator(log_resolution, d_latent, n_features_gen, max_features, rgb_features)
        self.discriminator = Discriminator(log_resolution, n_features_disc, max_features, rgb_features)

        self.grad_penalty_interval = grad_penalty_interval
        self.grad_penalty = GradientPenalty()
        self.grad_penalty_coefficient = grad_penalty_coefficient
        self.path_length_interval = path_length_interval
        self.path_length_penalty = PathLengthPenalty(path_length_beta).to(self.device)
        self.start_path_penalty = start_path_penalty

        self.style_mixing_prob = style_mixing_prob
        self.learning_rate = lr
        self.map_learning_rate = map_lr
        self.adam_betas = adam_betas
        self.n_images = 0

    def configure_optimizers(self):
        self.g_opt = torch.optim.Adam(
            [ {'params' : self.generator.parameters()},
              {'params' : self.mapping_net.parameters(), 'lr' : self.map_learning_rate},
            ],
            lr=self.learning_rate,
            betas=self.adam_betas,
        )
        self.d_opt = torch.optim.Adam(self.discriminator.parameters(), lr=self.learning_rate, betas=self.adam_betas)
        return [self.g_opt, self.d_opt]

    def get_w(self, batch_size: int):
        if torch.rand(()).item() < self.style_mixing_prob:
            cross_over_point = int(torch.rand(()).item() * self.generator.n_blocks)
            z2 = torch.randn(batch_size, self.d_latent, device=self.device)
            z1 = torch.randn(batch_size, self.d_latent, device=self.device)
            w1 = self.mapping_net(z1)
            w2 = self.mapping_net(z2)
            w1 = w1[None, :, :].expand(cross_over_point, -1, -1)
            w2 = w2[None, :, :].expand(self.generator.n_blocks - cross_over_point, -1, -1)
            return torch.cat((w1, w2), dim=0)
        else:
            z = torch.randn(batch_size, self.d_latent).to(self.device)
            w = self.mapping_net(z)
            return w[None, :, :].expand(self.generator.n_blocks, -1, -1)

    def forward(self, batch_size):
        w = self.get_w(batch_size)
        return self.generator(w)

    def training_step(self, batch : Union[torch.Tensor, List[torch.Tensor]], batch_idx : int, optimizer_idx : int):
        if isinstance(batch, list):
            batch, _ = batch

        batch_size = batch.shape[0]
        w = self.get_w(batch_size) # num_gen_blocks x batch_size x d_latent
        fakes = self.generator(w)
        d_fakes = self.discriminator(fakes)

        if optimizer_idx == 0:
            # Train generator
            g_loss = -d_fakes.mean()
            self.log('g_loss', g_loss, prog_bar=True)
            if batch_idx > self.start_path_penalty and batch_idx % self.path_length_interval == 0:
                plp = self.path_length_penalty(fakes, w)
                if not torch.isnan(plp):
                    g_loss += plp
                    self.log('gen_path_length_penalty', plp)
            torch.nn.utils.clip_grad_norm_(self.generator.parameters(), max_norm=1.0)
            torch.nn.utils.clip_grad_norm_(self.mapping_net.parameters(), max_norm=1.0)
            loss = g_loss
        else:
            # Train discriminator
            if batch_idx % self.grad_penalty_interval == 0:
                batch.requires_grad_()
            
            d_reals = self.discriminator(batch)
            d_loss_real = F.relu(1 - d_reals).mean() 
            d_loss_fake = F.relu(1 + d_fakes).mean()
            d_loss = d_loss_real + d_loss_fake
            self.log('d_loss', d_loss, prog_bar=True)

            if batch_idx % self.grad_penalty_interval == 0:
                gp = 0.5 * self.grad_penalty_coefficient * self.grad_penalty(batch, d_reals) * self.grad_penalty_interval
                d_loss += gp
                self.log('d_grad_penalty', gp)
            
            torch.nn.utils.clip_grad_norm_(self.discriminator.parameters(), max_norm=1.0)
            loss = d_loss
            self.log('d_loss_real', d_loss_real)
            self.log('d_loss_fake', d_loss_fake)
        
        if optimizer_idx == 0:
            self.n_images += batch_size
            self.log('n_images', self.n_images)
        
        return loss

    def training_epoch_end(self, _):
        # Save grid of example generated images
        side_len = 10
        fakes = self(side_len ** 2)
        grid = torch.zeros(self.rgb_features, side_len * self.image_size, side_len * self.image_size)
        for i in range(side_len):
            for j in range(side_len):
                grid[:, j * self.image_size : (j + 1) * self.image_size, i * self.image_size : (i + 1) * self.image_size] = fakes[i * side_len + j]
        grid = (grid + 1) / 2
        grid[grid > 1.] = 1.
        grid[grid < 0.] = 0.
        to_pil = transforms.ToPILImage()
        im = to_pil(grid)
        im.save(f'samples/epoch_{self.current_epoch}_images_{self.n_images}.png')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--resume', '-r')
    parser.add_argument('--image-size', '-is', default=32, type=int)
    parser.add_argument('--batch-size', '-b', default=32, type=int)
    parser.add_argument('--make-grid', '-g', default=False, type=bool)

    args = vars(parser.parse_args())

    torch.autograd.set_detect_anomaly(True)

    image_size = args['image_size']
    assert math.log2(image_size) == int(math.log2(image_size)) and image_size >= 4
    if args['resume'] is None:
        model = StyleGAN2(image_size=image_size, d_latent=512, n_features_gen=32, n_features_disc=64, max_features=512, rgb_features=1)
    else:
        model = StyleGAN2.load_from_checkpoint(args['resume'],
            image_size=image_size, d_latent=512, n_features_gen=32, n_features_disc=64, max_features=512, rgb_features=1
        )
    
    if args['make_grid']:
        model.training_epoch_end(None)
        exit()

    dataset = torchvision.datasets.MNIST(
        'data',
        train=True,
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(0.5, 0.5),
            transforms.Resize((image_size, image_size)),
        ]),
        download=True
    )

    batch_size = args['batch_size']
    assert batch_size % 4 == 0

    trainloader = DataLoader(dataset, batch_size, shuffle=True, num_workers=1, pin_memory=True)
    logger = TensorBoardLogger('runs', name=f'mnist_stylegan2')
    callback = ModelCheckpoint(save_top_k=-1)
    trainer = Trainer(gpus=0, logger=logger, callbacks=[callback])
    trainer.fit(model, train_dataloader=trainloader)
