import argparse
import os
from posixpath import dirname
from pytorch_lightning import Trainer, LightningModule
from pytorch_lightning.callbacks import ModelCheckpoint, LambdaCallback
from pytorch_lightning.loggers import TensorBoardLogger
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
from torch.utils.data.dataset import TensorDataset, random_split
import torchvision
import torch.optim
from torch.utils.data.dataloader import DataLoader
import torchvision.transforms as transforms
from PIL import Image
import math
import torch.autograd as autograd
import numpy as np
from stylegan2 import LatentMapper, Generator, Discriminator, GradientPenalty, PathLengthPenalty, EqualizedLinear
from typing import List, Tuple, Union
import facenet_pytorch

class FaceGAN(LightningModule):
    def __init__(self, image_size: int = 1024, d_latent : int = 512, n_features_gen : int = 32, max_features: int = 512,
                 rgb_features : int = 3, map_n_layers = 8, lr : float=1e-3, adam_betas : Tuple[float, float] = (0.9, 0.999),# (0., 0.99),
                 save_interval : int = 100):
        
        super().__init__()
        self.image_size = image_size
        self.rgb_features = rgb_features
        log_resolution = int(math.log2(image_size))

        self.d_latent = d_latent
        self.mapping_net = LatentMapper(d_latent, n_layers=map_n_layers)
        self.generator = Generator(log_resolution, d_latent, n_features_gen, max_features, rgb_features)

        self.facenet = facenet_pytorch.InceptionResnetV1(pretrained='vggface2', device=self.device).eval()
        # self.facenet_to_latent = LatentMapper(512, lr_multiplier=1)
        self.face_latent = Parameter(torch.randn(512, device=self.device, requires_grad=True))
        self.mtcnn = facenet_pytorch.MTCNN(device=self.device).eval()

        self.learning_rate = lr
        self.adam_betas = adam_betas
        self.n_images = Parameter(torch.zeros([], device=self.device, dtype=torch.int), requires_grad=False)
        self.last_save = self.n_images
        self.save_interval = save_interval
        self.should_save_checkpoint = False

    def configure_optimizers(self):
        # self.opt = torch.optim.Adam(self.facenet_to_latent.parameters(),
        #     lr=self.learning_rate,
        #     betas=self.adam_betas,
        # )
        self.opt = torch.optim.Adam(
            [self.face_latent],
            lr=self.learning_rate,
            # betas=self.adam_betas,
        )
        return self.opt

    def forward(self, batch_size, embeds=None):
        # if embeds is None:
        #     embeds = F.normalize(torch.randn(batch_size, 512, device=self.device), p=2, dim=1)
        
        # z = self.facenet_to_latent(embeds)
        # z = torch.randn(batch_size, 512, device=self.device)
        z = self.face_latent.unsqueeze(0).repeat((batch_size, 1))
        w = self.mapping_net(z)
        w = w.unsqueeze(0).repeat((self.generator.n_blocks, 1, 1))
        fakes = self.generator(w)
        return fakes

    def get_face_latents(self, batch):
        self.mtcnn.device = self.device
        self.facenet.device = self.device
        self.facenet.eval()
        batch_size = batch.shape[0]
        batch_stretched = (256 * (batch + 1) / 2).permute(0, 2, 3, 1).type(torch.uint8)
        faces = self.mtcnn(batch_stretched.cpu())
        non_none_faces = list(filter(lambda x : not x is None, faces))
        if len(non_none_faces) > 0:
            faces_tens = torch.stack(non_none_faces).to(self.device)
            face_embed = self.facenet(faces_tens)
        has_face = torch.tensor(list(map(lambda x : not x is None, faces)), dtype=torch.bool, device=self.device)
        all_embed = torch.zeros(batch_size, 512, device=self.device)
        if len(non_none_faces) > 0:
            all_embed[has_face] = face_embed
        all_embed[torch.logical_not(has_face)] = F.normalize(torch.randn(torch.logical_not(has_face).sum(), 512, device=self.device), p=2, dim=1) # fake embeddings
        return all_embed, has_face

    def get_fake_grid(self, side_len=10):
        fakes = self(side_len ** 2)
        grid = torch.zeros(self.rgb_features, side_len * self.image_size, side_len * self.image_size)
        for i in range(side_len):
            for j in range(side_len):
                grid[:, j * self.image_size : (j + 1) * self.image_size, i * self.image_size : (i + 1) * self.image_size] = fakes[i * side_len + j]
        grid = (grid + 1) / 2
        grid = grid.clamp(0., 1.)
        to_pil = transforms.ToPILImage()
        im = to_pil(grid)
        return im

    def save_fake_grid(self, side_len=1):
        fake_grid = self.get_fake_grid(side_len=side_len)
        fname = f'samples/face_gan/fakes_{self.n_images.item()}.png'
        os.makedirs(os.path.dirname(fname), exist_ok=True)
        fake_grid.save(fname)

    def set_target_face(self, f_w):
        self.target_face = f_w.clone().detach()

    def training_step(self, batch : Union[torch.Tensor, List[torch.Tensor]], batch_idx : int):
        if isinstance(batch, list):
            # batch, _ = batch
            batch = batch[0]

        z = self.face_latent.unsqueeze(0)
        w = self.mapping_net(z)
        w = w.unsqueeze(0).repeat((self.generator.n_blocks, 1, 1))
        fake = self.generator(w)
        fake_face_embed, fake_has_face = self.get_face_latents(fake)
        face_dist = 1 - torch.cosine_similarity(fake_face_embed, self.target_face.unsqueeze(0), dim=1)[0]
        self.log('face_loss', face_dist, prog_bar=True, on_step=True)
        return face_dist

        batch_size = batch.shape[0]
        real_face_embed, real_has_face = self.get_face_latents(batch)
        z = self.facenet_to_latent(real_face_embed)
        w : torch.Tensor = self.mapping_net(z)
        w = w.unsqueeze(0).repeat((self.generator.n_blocks, 1, 1))
        fakes = self.generator(w)

        # Use a form of face embedding-based perceptual loss
        fake_face_embed, fake_has_face = self.get_face_latents(fakes)
        face_dist = (fake_face_embed - real_face_embed).norm(p=2, dim=1)
        # OK if real doesn't have a found face, because we still generated according to the given embedding
        face_dist = torch.max(face_dist, torch.logical_not(fake_has_face) * 2) # max distance using L2 norm on 512 dimensional hypersphere
        face_loss = face_dist.mean()
        self.log('face_loss', face_loss, prog_bar=True)
        
        torch.nn.utils.clip_grad_norm_(self.mapping_net.parameters(), max_norm=1.0)
        
        self.n_images += batch_size
        self.log('n_images', self.n_images, prog_bar=True)

        if self.n_images > self.last_save + self.save_interval:
            self.last_save = self.n_images
            self.save_fake_grid()
            self.should_save_checkpoint = True
        else:
            self.should_save_checkpoint = False

        return face_loss

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data', '-d', default='D:/ffhq_thumbnails128x128')
    parser.add_argument('--resume', '-r')
    parser.add_argument('--batch-size', '-b', default=32, type=int)
    parser.add_argument('--make-grid', '-g', default=False, type=bool)
    parser.add_argument('--base-model', '-bm', default='ffhq_facegan_base.pt')
    parser.add_argument('--learning-rate', '-lr', type=float, default=1e-3)
    parser.add_argument('--save-interval', '-si', type=int, default=10)
    parser.add_argument('--target-face', '-tf', default='D:/ffhq_thumbnails128x128/thumbnails128x128/00986.png')

    args = vars(parser.parse_args())

    # torch.autograd.set_detect_anomaly(True)

    model : FaceGAN = None
    if args['resume'] is None:
        # model = FaceGAN(image_size=image_size, d_latent=args['latent_dim'], n_features_gen=32, max_features=512, rgb_features=3)
        model = torch.load(args['base_model'])
        model.learning_rate = args['learning_rate']
        model.save_interval = args['save_interval']
    else:
        model = FaceGAN.load_from_checkpoint(args['resume'])
    
    if args['make_grid']:
        # model.save_fake_grid()
        print(model.model_size)
        exit()
    
    im = Image.open(args['target_face'])
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(0.5, 0.5),
        transforms.Resize((model.image_size, model.image_size)),
    ])
    target_tens = transform(im)
    target_embed, target_has_face = model.get_face_latents(target_tens.unsqueeze(0))
    assert target_has_face.item()
    model.set_target_face(target_embed[0])

    # dataset = torchvision.datasets.ImageFolder(
    #     root=args['data'],
    #     transform=transforms.Compose([
    #         transforms.ToTensor(),
    #         transforms.Normalize(0.5, 0.5),
    #         transforms.Resize((model.image_size, model.image_size)),
    #     ])
    # )
    dataset = TensorDataset(torch.zeros(300))

    batch_size = 1#args['batch_size']

    trainloader = DataLoader(dataset, batch_size, shuffle=True, num_workers=1, pin_memory=True)
    logger = TensorBoardLogger('runs', name=f'face_gan')
    # callback1 = ModelCheckpoint(save_top_k=-1)
    def train_batch_end(trainer, pl_mod, *_):
        if pl_mod.should_save_checkpoint:
            fpath = os.path.join(logger.save_dir, 'checkpoints', f'{pl_mod.n_images.item()}.ckpt')
            os.makedirs(os.path.dirname(fpath), exist_ok=True)
            trainer.save_checkpoint(fpath)
    callback = LambdaCallback(on_train_batch_end=train_batch_end)
    trainer = Trainer(gpus=0, logger=logger, callbacks=[callback])
    trainer.fit(model, train_dataloader=trainloader)
