import os
from stylegan2 import StyleBlock, ModulatedConv2d, Upsample, Smooth
import torch
import torch.nn as nn
import pickle
from face_gan import FaceGAN
import sys
import matplotlib.pyplot as plt
from PIL import Image
import torchvision.transforms as transforms
import torch.nn.functional as F

def copy_model():
    if not 'D:/stylegan2-ada-pytorch' in sys.path:
        sys.path.append('D:/stylegan2-ada-pytorch')

    print('Loading pickle...')
    obj = pickle.load(open('ffhq.pkl', 'rb'))
    print('Done')
    generator : nn.Module = obj['G'].synthesis
    discriminator : nn.Module = obj['D']
    mapper : nn.Module = obj['G'].mapping

    # Find network architecture hyperparams
    image_size = max(map(lambda x: int(x[0].replace('b','')), generator.named_children()))
    latent_dim = mapper.fc0.weight.shape[0]
    mapper_num_layers = len(list(mapper.children()))
    max_features = generator.b4.conv1.weight.shape[1] # input dimension to first layer
    n_feat_g = generator.__getattr__(f'b{image_size}').torgb.weight.shape[1] # output dimension of last layer
    n_feat_d = discriminator.__getattr__(f'b{image_size}').fromrgb.weight.shape[0] # input dimension of first layer

    # Instantiate model
    model = FaceGAN(
        image_size=image_size, d_latent=latent_dim,
        n_features_gen=n_feat_g, max_features=max_features, map_n_layers=mapper_num_layers
    )

    # Fill in weights
    g_modules = list(generator.named_children())
    g_modules.sort(key=lambda x: int(x[0].replace('b','')))
    for i, (block_name, block) in enumerate(g_modules):
        to_block = model.generator.blocks[i]

        # ToRGB: affine, ModConv, bias
        to_block.to_rgb.affine.weight.weight.data = block.torgb.affine.weight.clone()
        to_block.to_rgb.affine.bias.data = block.torgb.affine.bias.clone()
        to_block.to_rgb.mod_conv.weights.weight.data = block.torgb.weight.clone()
        to_block.to_rgb.bias.data = block.torgb.bias.clone()

        # Style block: affine, mod_conv, bias, noise const, noise strength
        conv_children = list(filter(lambda x : 'conv' in x[0], block.named_children()))
        conv_children.sort(key=lambda x: int(x[0].replace('conv', '')))
        for j, (conv_name, conv) in enumerate(conv_children):
            to_block.blocks[j].affine.weight.weight.data = conv.affine.weight.clone()
            to_block.blocks[j].affine.bias.data = conv.affine.bias.clone()
            to_block.blocks[j].noise_const.data = conv.noise_const.clone()
            to_block.blocks[j].noise_strength.data = conv.noise_strength.clone()
            to_block.blocks[j].bias.data = conv.bias.clone()
            if conv.up > 1:
                to_block.blocks[j].mod_conv.weights.weight.data = conv.weight.clone().flip([2, 3])
            else:
                to_block.blocks[j].mod_conv.weights.weight.data = conv.weight.clone()
            to_block.blocks[j].conv_clamp = conv.conv_clamp
        
        # Learned max_features x 4 x 4 initial image input
        if i == 0:
            model.generator.init_constant.data = block.const.clone()
    
    for i in range(mapper_num_layers):
        from_layer = mapper.__getattr__(f'fc{i}')
        to_layer = model.mapping_net.blocks[i]
        to_layer.weight.weight.data = from_layer.weight.clone()
        to_layer.bias.data = from_layer.bias.clone()
    
    return model

if __name__ == '__main__':
    force_copy = True
    if os.path.exists('my_ffhq_facegan.pt') and not force_copy:
        model = torch.load('my_ffhq_facegan.pt')
    else:
        model = copy_model()
        torch.save(model, 'my_ffhq_facegan.pt')
    model.save_fake_grid(side_len=1)
    # sys.path.append('D:/stylegan2-ada-pytorch')
    # from torch_utils.ops import upfirdn2d
    # from torch_utils.ops import conv2d_resample

    # print('Loading pickle...')
    # obj = pickle.load(open('ffhq.pkl', 'rb'))
    # print('Done')
    # generator : nn.Module = obj['G'].synthesis
    # discriminator : nn.Module = obj['D']
    # mapper : nn.Module = obj['G'].mapping

    # # Find network architecture hyperparams
    # image_size = max(map(lambda x: int(x[0].replace('b','')), generator.named_children()))
    # latent_dim = mapper.fc0.weight.shape[0]
    # mapper_num_layers = len(list(mapper.children()))
    # max_features = generator.b4.conv1.weight.shape[1] # input dimension to first layer
    # n_feat_g = generator.__getattr__(f'b{image_size}').torgb.weight.shape[1] # output dimension of last layer
    # n_feat_d = discriminator.__getattr__(f'b{image_size}').fromrgb.weight.shape[0] # input dimension of first layer

    # # Instantiate model
    # model = FaceGAN(
    #     image_size=image_size, d_latent=latent_dim, n_features_disc=n_feat_d,
    #     n_features_gen=n_feat_g, max_features=max_features, map_n_layers=mapper_num_layers
    # )

    # # Fill in weights
    # g_modules = list(generator.named_children())
    # g_modules.sort(key=lambda x: int(x[0].replace('b','')))
    # for i, (block_name, block) in enumerate(g_modules):
    #     to_block = model.generator.blocks[i]

    #     # ToRGB: affine, ModConv, bias
    #     to_block.to_rgb.affine.weight.weight.data = block.torgb.affine.weight.clone()
    #     to_block.to_rgb.affine.bias.data = block.torgb.affine.bias.clone()
    #     to_block.to_rgb.mod_conv.weights.weight.data = block.torgb.weight.clone()
    #     to_block.to_rgb.bias.data = block.torgb.bias.clone()

    #     # Style block: affine, mod_conv, bias, noise const, noise strength
    #     conv_children = list(filter(lambda x : 'conv' in x[0], block.named_children()))
    #     conv_children.sort(key=lambda x: int(x[0].replace('conv', '')))
    #     for j, (conv_name, conv) in enumerate(conv_children):
    #         to_block.blocks[j].affine.weight.weight.data = conv.affine.weight.clone()
    #         to_block.blocks[j].affine.bias.data = conv.affine.bias.clone()
    #         to_block.blocks[j].noise_const.data = conv.noise_const.clone()
    #         to_block.blocks[j].noise_strength.data = conv.noise_strength.clone()
    #         to_block.blocks[j].bias.data = conv.bias.clone()
    #         if conv.up > 1:
    #             to_block.blocks[j].mod_conv.weights.weight.data = conv.weight.clone().flip([2, 3])
    #         else:
    #             to_block.blocks[j].mod_conv.weights.weight.data = conv.weight.clone()
    #         to_block.blocks[j].conv_clamp = conv.conv_clamp
        
    #     # Learned max_features x 4 x 4 initial image input
    #     if i == 0:
    #         model.generator.init_constant.data = block.const.clone()
    
    # for i in range(mapper_num_layers):
    #     from_layer = mapper.__getattr__(f'fc{i}')
    #     to_layer = model.mapping_net.blocks[i]
    #     to_layer.weight.weight.data = from_layer.weight.clone()
    #     to_layer.bias.data = from_layer.bias.clone()

    # torch.save(model, 'my_ffhq_facegan.pt')

    # for block_name, block in g_modules:
    #     block.use_fp16 = False

    # # mapping net
    # z = torch.randn(1, 512)
    # w = model.mapping_net(z)

    # init_const = model.generator.init_constant.unsqueeze(0)
    # noise_mode = 'const'

    # y_4_from = generator.b4(None, None, w.unsqueeze(1).repeat(1, 2, 1), noise_mode=noise_mode)
    # y_8_from = generator.b8(y_4_from[0], y_4_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_16_from = generator.b16(y_8_from[0], y_8_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_32_from = generator.b32(y_16_from[0], y_16_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_64_from = generator.b64(y_32_from[0], y_32_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_128_from = generator.b128(y_64_from[0], y_64_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_256_from = generator.b256(y_128_from[0], y_128_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_512_from = generator.b512(y_256_from[0], y_256_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)
    # y_1024_from = generator.b1024(y_512_from[0], y_512_from[1], w.unsqueeze(1).repeat(1, 3, 1), noise_mode=noise_mode)

    # ws = w.unsqueeze(0).repeat((18, 1, 1))
    # y_4_to = model.generator(ws, eval_resolution=4, noise_mode=noise_mode)
    # y_8_to = model.generator(ws, eval_resolution=8, noise_mode=noise_mode)
    # y_16_to = model.generator(ws, eval_resolution=16, noise_mode=noise_mode)
    # y_32_to = model.generator(ws, eval_resolution=32, noise_mode=noise_mode)
    # y_64_to = model.generator(ws, eval_resolution=64, noise_mode=noise_mode)
    # y_128_to = model.generator(ws, eval_resolution=128, noise_mode=noise_mode)
    # y_256_to = model.generator(ws, eval_resolution=256, noise_mode=noise_mode)
    # y_512_to = model.generator(ws, eval_resolution=512, noise_mode=noise_mode)
    # y_1024_to = model.generator(ws, eval_resolution=1024, noise_mode=noise_mode)

    # to_rgbs = [y_4_to, y_8_to, y_16_to, y_32_to, y_64_to, y_128_to, y_256_to, y_512_to, y_1024_to]
    # from_rgbs = [y_4_from[1], y_8_from[1], y_16_from[1], y_32_from[1], y_64_from[1], y_128_from[1], y_256_from[1], y_512_from[1], y_1024_from[1]]
    # dists = []
    # for a, b in zip(to_rgbs, from_rgbs):
    #     dists.append((a - b).norm(p=2).item())

    # to_pil = transforms.ToPILImage()
    # final_to_rgb = ((to_rgbs[-1][0] + 1) / 2).clamp(0., 1.)
    # final_from_rgb = ((from_rgbs[-1][0] + 1) / 2).clamp(0., 1.)
    # plt.imshow(to_pil(final_to_rgb))
    # plt.show()
    # plt.imshow(to_pil(final_from_rgb))
    # plt.show()

    # # sb_to : StyleBlock = model.generator.blocks[1].blocks[0]
    # # s = sb_to.affine(w)
    # # conv_to = sb_to.mod_conv
    # # conv_weights = conv_to.weights.weight
    # # sb_from = generator.b8.conv0

    # # y = sb_to(init_const, w, noise_mode=noise_mode)
    # # rgb_from = generator.b4.torgb
    # # rgb_to = model.generator.blocks[0].to_rgb

    # f = [1, 3, 3, 1]
    # resample_filter = upfirdn2d.setup_filter(f)
    # us = Upsample()

    # print('Making fake grid...')
    # # model.save_fake_grid(side_len=1)
    # # their_fake = (generator.cuda()(w.unsqueeze(1).repeat(1, 18, 1).cuda(), noise_mode=None) + 1) / 2
    # # to_pil = transforms.ToPILImage()
    # # their_pil = to_pil(their_fake)
    # # their_pil.save('samples/face_gan/from_fakes.png')
    # print('Done')
