from pytorch_lightning import Trainer, LightningModule
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data.dataset import random_split
import torchvision
import torch.optim
from torch.utils.data.dataloader import DataLoader
import torchvision.transforms as transforms
from PIL import Image
import math
import torch.autograd as autograd
import numpy as np

class PixelNorm(nn.Module):
    EPS = 1e-8

    def __init__(self):
        super().__init__()

    def forward(self, x : torch.Tensor):
        # b_(x, y) = a_(x, y) / sqrt(mean_j( a_(x, y, j)^2 ) + eps)
        batch_size, num_channels, _, _ = x.shape
        x_sq = x ** 2
        x_sq_sum = x_sq.sum(dim=1)
        denom = torch.sqrt(self.EPS + x_sq_sum / num_channels)
        denom = denom.unsqueeze(1).repeat(1, num_channels, 1, 1)
        denom = denom.detach()
        y = x / denom
        yd = y.clone().detach()
        assert torch.isnan(yd).sum() == 0
        assert torch.isneginf(yd).sum() == 0
        assert torch.isposinf(yd).sum() == 0
        return y
        # n = x.sum(dim=1) + self.EPS # N x H x W
        # n = n.reciprocal()
        # n = n.unsqueeze(dim=1) # N x 1 x H x W
        # n = n.repeat(1, x.shape[1], 1, 1)
        # x = x / n
        # xd = x.clone().detach()
        # return x / n

class MinibatchStd(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x : torch.Tensor):
        batch_size, _, w, h = x.shape
        std = torch.std(x, dim=0)
        overall = std.mean()
        overall = overall.view(1, 1, 1, 1).repeat(batch_size, 1, w, h)
        overall = overall.detach()
        y = torch.cat((x, overall), dim=1)
        return y

class Generator(nn.Module):
    def __init__(self, latent_dim, image_channels=3, init_hidden_maps=128, map_dropoff_i=0, n_classes=0):
        super().__init__()
        # Start with 4x4 network
        self.image_channels = image_channels
        self.latent_dim = latent_dim
        self.side_len = 4
        self.alpha = 1
        self.map_dropoff_i = map_dropoff_i
        self.init_hidden_maps = init_hidden_maps
        self.n_classes = n_classes

        self.n_layers = 0
        self.blocks = []
        self.output_blocks = []
        self.output_upscale = nn.Upsample(scale_factor=2, mode='bilinear')
        self.add_layer('cpu')

    def add_layer(self, device):
        new_block = self.make_block(self.n_layers, device)
        new_out_block = self.make_out_block(self.n_layers, device)
        self.blocks.append(new_block)
        self.output_blocks.append(new_out_block)
        self.add_module(f'block{self.n_layers}', new_block)
        self.add_module(f'out_block{self.n_layers}', new_out_block)
        # param_dict = {
        #     f'block{self.n_layers}' : new_block.parameters(),
        #     f'out_block{self.n_layers}' : new_out_block.parameters(),
        # }
        self.n_layers += 1
        return [new_block.parameters(), new_out_block.parameters()]

    def set_alpha(self, alpha):
        self.alpha = alpha

    def get_channels(self, blocki):
        num_maps = self.init_hidden_maps
        last_num_maps = None
        for i in range(blocki):
            last_num_maps = num_maps
            if i >= self.map_dropoff_i:
                num_maps //= 2
        # in_channels = int(16 * (2 ** (self.n_layers - blocki)))
        # out_channels = int(16 * (2 ** (self.n_layers - 1 - blocki)))
        # return (in_channels, out_channels)
        return (last_num_maps, num_maps)

    def make_out_block(self, blocki, device):
        _, in_channels = self.get_channels(blocki)
        block = nn.Sequential(
            nn.Conv2d(in_channels, self.image_channels, 1),
            nn.Tanh(),
            nn.Upsample(size=(4 * (2 ** blocki), 4 * (2 ** blocki))),
        )
        block = block.to(device)
        return block

    def make_block(self, blocki, device):
        # -> upscale -> conv 3x3 -> pixel norm -> conv 3x3 -> pixel norm ->
        # Size    : 1 -> 4   -> 8  -> 16 -> 32  
        # Channels: l -> 128 -> 64 -> 32 -> 16 -> 3
        # i       : 0    1      2     3     4
        # n_layers: 4
        in_channels, out_channels = self.get_channels(blocki)
        if blocki == 0:
            in_channels = self.latent_dim + self.n_classes
            block = nn.Sequential(
                nn.ConvTranspose2d(in_channels, out_channels, kernel_size=4),
                nn.LeakyReLU(),
                # PixelNorm(),
                nn.BatchNorm2d(out_channels),
                nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                # PixelNorm(),
                nn.BatchNorm2d(out_channels),
            )
        else:
            block = nn.Sequential(
                nn.Upsample(scale_factor=2),
                nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                # PixelNorm(),
                nn.BatchNorm2d(out_channels),
                nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                # PixelNorm(),
                nn.BatchNorm2d(out_channels),
            )
        block = block.to(device)
        return block

    def forward(self, batch):
        z, classes = batch
        if self.n_classes > 0:
            classes = F.one_hot(classes, num_classes=self.n_classes)
        else:
            classes = torch.zeros(z.shape[0], 0, device=z.device)
        z = torch.cat((z, classes), dim=1)
        z = F.normalize(z, p=2)
        z = z.view(*z.shape, 1, 1)

        if self.alpha == 1:
            x = z
            for i in range(self.n_layers):
                x = self.blocks[i](x)
            y = self.output_blocks[self.n_layers - 1](x)
        else:
            x1 = z
            for i in range(self.n_layers - 1):
                x1 = self.blocks[i](x1)
            x2 = (self.blocks[self.n_layers - 1])(x1)
            y1 = self.output_blocks[self.n_layers - 2](x1)
            y1 = self.output_upscale(y1)
            y2 = self.output_blocks[self.n_layers - 1](x2)
            y = y2 * self.alpha + y1 * (1 - self.alpha)
        
        return y

class Discriminator(nn.Module):
    def __init__(self, image_channels=3, init_hidden_maps=128, map_dropoff_i=0, n_classes=0):
        super().__init__()
        self.image_channels = image_channels
        self.side_len = 4
        self.alpha = 1
        self.map_dropoff_i = map_dropoff_i
        self.init_hidden_maps = init_hidden_maps
        self.n_classes = n_classes

        self.n_layers = 0
        self.blocks = []
        self.in_blocks = []
        self.add_layer('cpu')

        self.out_block = nn.Sequential(
            # nn.Flatten(),
            nn.Linear(init_hidden_maps + n_classes, 1),
            # nn.Sigmoid(),
        )
        self.in_downscale = nn.AvgPool2d(kernel_size=2)

    def add_layer(self, device):
        new_block = self.make_block(self.n_layers, device)
        new_in_block = self.make_in_block(self.n_layers, device)
        self.blocks.append(new_block)
        self.in_blocks.append(new_in_block)
        self.add_module(f'block{self.n_layers}', new_block)
        self.add_module(f'in_block{self.n_layers}', new_in_block)
        # param_dict = {
        #     f'block{self.n_layers}' : new_block.parameters(),
        #     f'in_block{self.n_layers}' : new_in_block.parameters(),
        # }
        self.n_layers += 1
        return [new_block.parameters(), new_in_block.parameters()]

    def set_alpha(self, alpha):
        self.alpha = alpha

    def forward(self, batch):
        x, classes = batch
        if self.alpha == 1:
            x = self.in_blocks[self.n_layers - 1](x)
            for i in range(self.n_layers):
                i = self.n_layers - 1 - i
                x = self.blocks[i](x)
        else:
            x1 = self.in_blocks[self.n_layers - 1](x)
            x1 = self.blocks[self.n_layers - 1](x1)
            x2 = self.in_blocks[self.n_layers - 2](self.in_downscale(x))
            x = x1 * self.alpha + x2 * (1 - self.alpha)
            for i in range(self.n_layers - 1):
                i = self.n_layers - 2 - i
                x = self.blocks[i](x)
        y = torch.flatten(x, start_dim=1)
        if self.n_classes > 0:
            classes = F.one_hot(classes, num_classes=self.n_classes)
        else:
            classes = torch.zeros(x.shape[0], 0, device=x.device)
        y = torch.cat((y, classes), dim=1)
        y = self.out_block(y)
        return y
    
    def get_channels(self, blocki):
        num_maps = self.init_hidden_maps
        last_num_maps = self.init_hidden_maps
        for i in range(blocki - 1):
            last_num_maps = num_maps
            if i >= self.map_dropoff_i:
                num_maps //= 2
        return (last_num_maps, num_maps)

    def make_in_block(self, blocki, device):
        _, in_channels = self.get_channels(blocki)
        block = nn.Sequential(
            nn.Conv2d(self.image_channels, in_channels, kernel_size=1),
            nn.LeakyReLU(),
        )
        block = block.to(device)
        return block

    def make_block(self, blocki, device):
        # -> downscale -> conv 3x3 -> pixel norm -> conv 3x3 -> pixel norm ->
        # Size    :      32 -> 16 -> 8  -> 4   -> 1
        # Channels: C -> 16 -> 32 -> 64 -> 128 -> 256
        # i       :      0     1     2     3      4
        # n_layers: 4
        out_channels, in_channels = self.get_channels(blocki)
        if blocki == 0:
            block = nn.Sequential(
                MinibatchStd(),
                nn.Conv2d(in_channels + 1, in_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.Conv2d(in_channels, out_channels, kernel_size=4),
                nn.LeakyReLU(),
            )
        else:
            block = nn.Sequential(
                nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.AvgPool2d(kernel_size=2),
            )
        block = block.to(device)
        return block

class GAN(LightningModule):
    def __init__(self, latent_dim=10, image_channels=3, init_hidden_maps=128, map_dropoff_i=0, n_classes=0):
        super().__init__()
        self.image_channels = image_channels
        self.init_hidden_maps = init_hidden_maps
        self.map_dropoff_i = map_dropoff_i
        self.n_classes = n_classes
        self.n_images = 0
        self.n_layers = 1

        self.G = Generator(latent_dim=latent_dim, image_channels=image_channels, init_hidden_maps=init_hidden_maps, map_dropoff_i=map_dropoff_i, n_classes=n_classes)
        self.D = Discriminator(image_channels=image_channels, init_hidden_maps=init_hidden_maps, map_dropoff_i=map_dropoff_i, n_classes=n_classes)
        self.hparams = {
            'd_lr' : 1e-3,
            'd_betas' : (0, 0.99),
            'g_lr' : 1e-3,
            'g_betas' : (0, 0.99),
            'eps_drift' : 0.0001,
            'sched_lambda' : 0.8,
            'latent_dim' : latent_dim,
            'image_channels' : image_channels,
            'lambda' : 10,
            'n_critic' : 1,
            # 'images_per_stage' : 800000,
            'images_per_stage' : 600000,
            # 'images_per_stage' : 60000,
        }
    
    def forward(self, z):
        return self.G(z)

    def configure_optimizers(self):
        self.g_opt = torch.optim.Adam(self.G.parameters(), lr=self.hparams['g_lr'], betas=self.hparams['g_betas'])
        self.d_opt = torch.optim.Adam(self.D.parameters(), lr=self.hparams['d_lr'], betas=self.hparams['d_betas'])
        # self.sched = torch.optim.lr_scheduler.MultiplicativeLR(self.optimizer, self.hparams['sched_lambda'])
        return [self.g_opt, self.d_opt], []
    
    def training_epoch_end(self, _):
        cols = 10
        rows = self.n_classes if self.n_classes > 0 else cols
        latents = torch.randn(cols * rows, self.hparams['latent_dim'], device=self.device)
        classes = torch.randint(high=self.n_classes, size=(cols * rows,), device=self.device) if self.n_classes > 0 else torch.zeros(cols * rows, device=self.device, dtype=torch.long)
        # w, h = self.hparams['im_size'][1], self.hparams['im_size'][2]
        self.G.set_alpha(self.get_alpha())
        generated = self.G((latents, classes))
        _, _, im_side_len, _ = generated.shape
        grid = torch.zeros((self.image_channels, im_side_len * rows, im_side_len * cols))
        for i in range(rows):
            for j in range(cols):
                grid[:, i * im_side_len : (i + 1) * im_side_len, j * im_side_len : (j + 1) * im_side_len] = generated[i * rows + j]
        grid = (grid + 1) / 2
        to_pil = transforms.ToPILImage()
        grid_im = to_pil(grid)
        grid_im.save(f'samples/epoch-end-grid-{self.n_layers}-{self.current_epoch}-{self.n_images}.png')

    # WGAN GP loss functions
    def get_grad_penalty(self, reals, fakes, classes):
        batch_size = reals.shape[0]
        _, _, im_side_len, _ = reals.shape
        eps = torch.rand(batch_size, device=self.device).view(batch_size, 1, 1, 1).repeat(1, 1, im_side_len, im_side_len)
        mixed :torch.Tensor = fakes * eps + reals * (1 - eps)
        mixed = mixed.requires_grad_()
        dmixed = self.D((mixed, classes) if not classes is None else mixed)
        gradients = autograd.grad(outputs=dmixed, inputs=mixed,
                              grad_outputs=torch.ones(dmixed.size(), device=self.device),
                              create_graph=True, retain_graph=True, only_inputs=True)[0]
        gradient_penalty = ((gradients.norm(p=2, dim=1) - 1) ** 2).mean() * self.hparams['lambda']
        return gradient_penalty

    def get_d_loss(self, reals, classes):
        batch_size = reals.shape[0]
        z = torch.randn(batch_size, self.hparams.latent_dim, device=self.device)
        fakes = self.G((z, classes))
        dreal = self.D((reals, classes))
        dfake = self.D((fakes, classes))
        grad_penalty  = self.get_grad_penalty(reals, fakes, classes)
        loss = dfake - dreal + grad_penalty
        drift_val = (dreal ** 2).mean() * self.hparams['eps_drift']
        loss += drift_val
        return loss.mean()

    def get_g_loss(self, batch_size, classes):
        z = torch.randn(batch_size, self.hparams.latent_dim, device=self.device)
        fakes = self.G((z, classes))
        dfake = self.D((fakes, classes))
        loss = -dfake.mean()
        return loss

    # def handle_progression(self):
    #     stage = (self.n_images / self.hparams['images_per_stage']) + 1
    #     layer = self.n_layers
    #     layer_stage = int(stage) % 2
    #     stage_progress = stage % 1
    #     # assert self.G.n_layers == self.D.n_layers
    #     if self.n_layers - 1 < layer:
    #         g_new_layer_params = self.G.add_layer(self.device)
    #         d_new_layer_params = self.D.add_layer(self.device)
    #         for params in g_new_layer_params:
    #             self.g_opt.add_param_group({'params' : params})
    #         for params in d_new_layer_params:
    #             self.d_opt.add_param_group({'params' : params})
    #         self.n_layers += 1
    #     # assert self.G.n_layers - 1 == layer
    #     if layer_stage == 0:
    #         alpha = stage_progress
    #     else:
    #         alpha = 1
    #     self.G.set_alpha(alpha)
    #     self.D.set_alpha(alpha)
    #     self.log('layer', layer, prog_bar=True, logger=True, on_step=True)
    #     self.log('alpha', alpha, prog_bar=True, logger=True, on_step=True)
    #     return (layer, alpha)

    def get_alpha(self):
        stage = (self.n_images / self.hparams['images_per_stage']) + 1
        layer = self.n_layers - 1
        layer_stage = min(1, max(0, int(stage - layer * 2)))
        stage_progress = stage - layer * 2 - layer_stage
        if layer_stage == 0:
            alpha = min(1.0, max(0.0, stage_progress))
        else:
            alpha = 1.0
        return alpha

    def add_layer(self):
        self.G.add_layer(self.device)
        self.D.add_layer(self.device)
        self.n_layers += 1
        return self.n_layers

    def get_num_images_per_layer(self):
        if self.n_layers == 1:
            return self.hparams['images_per_stage']
        else:
            return self.hparams['images_per_stage'] * 2

    # def resize_input(self, batch):
    #     side_len = 4 * (2 ** (self.n_layers - 1))
    #     resize = transforms.Resize((side_len, side_len))
    #     return resize(batch)

    def get_input_resize(self):
        side_len = 4 * (2 ** (self.n_layers - 1))
        resize = transforms.Resize((side_len, side_len))
        return resize

    def training_step(self, batch, batch_idx, optimizer_idx):
        if isinstance(batch, list) or isinstance(batch, tuple):
            batch, classes = batch
            if self.n_classes == 0:
                classes = torch.zeros(batch.shape[0], 0, device=self.device)
        else:
            classes = torch.zeros(batch.shape[0], 0, device=self.device)
        batch_size = batch.shape[0]
        # layer, _ = self.handle_progression()
        # batch = self.resize_input(batch, layer)
        alpha = self.get_alpha()
        self.G.set_alpha(alpha)
        self.D.set_alpha(alpha)
        self.log('alpha', alpha, prog_bar=True, logger=True, on_step=True)

        loss = None
        if optimizer_idx == 0 and batch_idx % self.hparams['n_critic'] == 0:
            # generator
            # g_loss = self.generator_step(batch, batch_idx)
            g_loss = self.get_g_loss(batch_size, classes)
            self.log('train_g_loss', g_loss, prog_bar=True, logger=True, on_step=True, on_epoch=True)
            loss = g_loss
        if optimizer_idx == 1:
            # discriminator/critic
            # d_loss = self.discriminator_step(batch, batch_idx)
            d_loss = self.get_d_loss(batch, classes)
            self.log('train_d_loss', d_loss, prog_bar=True, logger=True, on_step=True, on_epoch=True)
            loss = d_loss
        if optimizer_idx == 0:
            self.n_images += batch.shape[0]
        return loss

    def on_validation_epoch_start(self) -> None:
        self.fid_net = torchvision.models.inception_v3(pretrained=True)
        self.fid_net.fc = nn.Identity()
        self.fid_net.to(self.device)
        self.fid_net.eval()

    def validation_step(self, batch, batch_idx):
        # Calculate FID
        if isinstance(batch, list) or isinstance(batch, tuple):
            batch, classes = batch
        else:
            classes = torch.zeros(batch.shape[0], 0, device=self.device)

        batch = batch.repeat((1, 3 // self.image_channels, 1, 1))
        batch_size = batch.shape[0]
        resize = transforms.Resize((299, 299))
        act_real = self.fid_net(resize(batch))

        z = torch.randn(batch_size, self.hparams.latent_dim, device=self.device)
        fakes = self.G((z, classes))
        fakes = fakes.repeat((1, 3 // self.image_channels, 1, 1))
        act_fake = self.fid_net(resize(fakes))

        return {'act_real' : act_real, 'act_fake' : act_fake}

    def validation_epoch_end(self, outputs):
        self.fid_net = None

        acts_real = list(map(lambda x : x['act_real'], outputs))
        act_real = torch.cat(acts_real, dim=0)

        acts_fake = list(map(lambda x : x['act_fake'], outputs))
        act_fake = torch.cat(acts_fake, dim=0)

        act_real_arr : np.ndarray = np.array(act_real.detach().cpu())
        act_fake_arr : np.ndarray = np.array(act_fake.detach().cpu())

        act_mean_dist_sq = np.abs(act_fake_arr.mean() - act_real_arr.mean()) ** 2
        act_fake_cov = np.cov(act_fake_arr, rowvar=False)
        act_real_cov = np.cov(act_real_arr, rowvar=False)
        fid = act_mean_dist_sq + np.trace(act_real_cov + act_fake_cov - 2 * (act_real_cov * act_fake_cov) ** (0.5)) # TODO: handle negatives/complex

        self.log('fid', fid, prog_bar=True, logger=True, on_epoch=True)

if __name__ == '__main__':
    # torch.autograd.set_detect_anomaly(True)
    model = GAN(latent_dim=512, image_channels=1, init_hidden_maps=512, map_dropoff_i=0, n_classes=0)

    # batch_size = 500 # 250 at 16 -> 500 at 8 -> 1000 at 4
    size = 4 * (2 ** (model.n_layers - 1))
    while size <= 32:
        dataset = torchvision.datasets.MNIST('data',
            train=True,
            transform=transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize(0.5, 0.5), # [-1, 1]
                model.get_input_resize(),
            ]),
            download=True
        )
        
        batch_size = 500 // (4 ** (model.n_layers - 1))
        trainloader = DataLoader(dataset, batch_size, shuffle=True, num_workers=1, pin_memory=True)

        val_len = int(len(dataset) * 0.1)
        valset, _ = random_split(dataset, [val_len, len(dataset) - val_len])
        val_batch_size = 10
        valloader = DataLoader(valset, val_batch_size, shuffle=False, num_workers=1, pin_memory=True)

        num_epochs = math.ceil(model.get_num_images_per_layer() / len(dataset)) # 600000 / 60000
        print(f'Starting training for {size}x{size} for {num_epochs} epochs with batch size {batch_size}')

        logger = TensorBoardLogger('runs', name=f'mnist_progan_{size}', version=4)
        callback = ModelCheckpoint(save_top_k=-1)
        trainer = Trainer(gpus=-1, max_epochs=num_epochs, logger=logger, callbacks=[callback])
        print(model)
        trainer.fit(model, train_dataloader=trainloader, val_dataloaders=None)

        # valloader = DataLoader(dataset, batch_size, shuffle=False, num_workers=4, pin_memory=True)
        # trainer.validate(model, val_dataloaders=valloader)

        model.add_layer()
        size = 4 * (2 ** (model.n_layers - 1))
