module lightFSM #(parameter logic init=1) (Clock, Reset, L, R, NL, NR, lightOn);
	input logic Clock, Reset;
	// L - True when left key (KEY[3]) is pressed
	// R - True when right key (KEY[0]) is pressed
	// NL - True when the light to the left of this one is ON
	// NR - True when the light on the right of this one is ON
	input logic L, R, NL, NR;
	// lightOn – True when this normal light should be ON/lit
	output logic lightOn;
	
	// YOUR CODE GOES HERE
	logic nextOn;
	always_comb
		if(NL & R & ~L & ~lightOn) // Move right
			nextOn = 1;
		else if (NR & L & ~R & ~lightOn) // Move left
			nextOn = 1;
		else if (lightOn & ~(L ^ R)) // Stalemate
			nextOn = 1;
		else
			nextOn = 0;
	
	always_ff @(posedge Clock)
		if (Reset)
			lightOn <= init;
		else
			lightOn <= nextOn;
endmodule

module normalLightFSM_testbench();
	logic clk, reset, L, R, NL, NR;
	logic out;

	lightFSM #(0) dut (clk, reset, L, R, NL, NR, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 0; R <= 1; NL <= 1; NR <= 0; // Move right
		@(posedge clk); L <= 0; R <= 0; NL <= 0; // Stalemate 1
		@(posedge clk); L <= 1; R <= 1; // Stalemate 2
		@(posedge clk); L <= 0; // Move off to the right
		@(posedge clk); L <= 1; R <= 0; NR <= 1; // Move back left
		@(posedge clk); L <= 0; R <= 1; NR <= 0; // Move off to the right
		@(posedge clk); NR <= 1; // Move further off to the right
		@(posedge clk); NR <= 0;
		@(posedge clk); L <= 1; // Away stalemate
		@(posedge clk); R <= 0; // Move back left a bit
		@(posedge clk); NR <= 1; // Back on this light
		@(posedge clk); NR <= 0; // Leave to the left
		@(posedge clk); NL <= 1;
		@(posedge clk); NL <= 0;
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule

module centerLightFSM_testbench();
	logic clk, reset, L, R, NL, NR;
	logic out;

	lightFSM #(1) dut (clk, reset, L, R, NL, NR, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 1; R <= 0; NR <= 0; NL <= 0; // Move left
		@(posedge clk); L <= 0; R <= 1; NL <= 1; // Move right
		@(posedge clk); L <= 0; R <= 0; NL <= 0; // Stalemate 1
		@(posedge clk); L <= 1; R <= 1; // Stalemate 2
		@(posedge clk); L <= 0; // Move off to the right
		@(posedge clk); L <= 1; R <= 0; NR <= 1; // Move back left
		@(posedge clk); L <= 0; R <= 1; NR <= 0; // Move off to the right
		@(posedge clk); NR <= 1; // Move further off to the right
		@(posedge clk); NR <= 0;
		@(posedge clk); L <= 1; // Away stalemate
		@(posedge clk); R <= 0; // Move back left a bit
		@(posedge clk); NR <= 1; // Back on this light
		@(posedge clk); NR <= 0; // Leave to the left
		@(posedge clk); NL <= 1;
		@(posedge clk); NL <= 0;
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
