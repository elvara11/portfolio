module victory (Clock, Reset, L, R, LL, RL, VictorLEDs, SomeVictory);
	input logic Clock, Reset;
	// L - True when left key (KEY[3]) is pressed
	// R - True when right key (KEY[0]) is pressed
	// LL - True when leftmost light is on
	// RL - True when rightmost light is on
	input logic L, R, LL, RL;
	
	// VictorLEDs - 7Seg display output for the last victor, default is blank
	output logic [6:0] VictorLEDs;
	
	// SomeVictory - True iff either player just won
	output logic SomeVictory;
	
	parameter [1:0] NoWinner = 2'b00, LeftWon = 2'b01, RightWon = 2'b10;
	
	logic [2:0] ps, ns;
	
	// ps -> ns
	always_comb
		if (L & ~R & LL) begin // Left wins
			ns = LeftWon;
			SomeVictory = 1;
		end else if (~L & R & RL) begin // Right wins
			ns = RightWon;
			SomeVictory = 1;
		end else begin // Nobody wins
			ns = ps;
			SomeVictory = 0;
		end
	
	// Output VictorLEDs
	always_comb
		case (ps)
			NoWinner: VictorLEDs = 7'b0000000;
			LeftWon: VictorLEDs = 7'b0000110;
			RightWon: VictorLEDs = 7'b1011011;
			default: VictorLEDs = 7'bxxxxxxx;
		endcase
	
	// Register
	always_ff @(posedge Clock)
		if (Reset)
			ps <= NoWinner;
		else
			ps <= ns;
endmodule

module victory_testbench();
	logic clk, reset, L, R, LL, RL;
	logic [6:0] VictorLEDs;
	logic SomeVictory;

	victory dut (clk, reset, L, R, LL, RL, VictorLEDs, SomeVictory);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 0; R <= 0; LL <= 0; RL <= 0; // Four cases of no victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0;
		@(posedge clk); RL <= 1; // Right victory
		@(posedge clk); R <= 0; // Three cases of almost-but-not right victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0; // Second right victory
		@(posedge clk); R <= 0; L <= 1; LL <= 1; RL <= 0; // Left victory
		@(posedge clk); L <= 0; // Three cases of almost-but-not left victory
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 0; // Second left victory
		@(posedge clk); L <= 0; LL <= 0; R <= 1; RL <= 1; // Right victory after left victory
		@(posedge clk); reset <= 1; R <= 0; RL <= 0; // Test no winner -> left victory
		@(posedge clk); reset <= 0; L <= 1; LL <= 1;
		@(posedge clk); L <= 0; LL <= 0; // Wait for victory propagation
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
