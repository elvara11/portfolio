module DE1_SoC (CLOCK_50, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, KEY, LEDR, SW);
	input  logic		CLOCK_50; // 50MHz clock.
	output logic [6:0]	HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	output logic [9:0]	LEDR;
	input  logic [3:0]	KEY;		 // True when not pressed, False when pressed
	input  logic [9:0]	SW;
	
	// Defaults
	assign LEDR[0] = 0;
	assign HEX1 = 7'b1111111;
	assign HEX2 = 7'b1111111;
	assign HEX3 = 7'b1111111;
	assign HEX4 = 7'b1111111;
	assign HEX5 = 7'b1111111;
	
	// Input handling
	logic reset, L_Raw, R_Raw;
	inputDelay delay(CLOCK_50, SW[9], KEY[3], KEY[0], reset, L_Raw, R_Raw);
	logic L, R;
	pressed l_pressed(CLOCK_50, reset, L_Raw, L);
	pressed r_pressed(CLOCK_50, reset, R_Raw, R);
	
	// Light FSMs
	logic light_reset;
	lightFSM #(0) light1 (CLOCK_50, light_reset, L, R, LEDR[2], 1'b0   , LEDR[1]);
	lightFSM #(0) light2 (CLOCK_50, light_reset, L, R, LEDR[3], LEDR[1], LEDR[2]);
	lightFSM #(0) light3 (CLOCK_50, light_reset, L, R, LEDR[4], LEDR[2], LEDR[3]);
	lightFSM #(0) light4 (CLOCK_50, light_reset, L, R, LEDR[5], LEDR[3], LEDR[4]);
	lightFSM #(1) light5 (CLOCK_50, light_reset, L, R, LEDR[6], LEDR[4], LEDR[5]);
	lightFSM #(0) light6 (CLOCK_50, light_reset, L, R, LEDR[7], LEDR[5], LEDR[6]);
	lightFSM #(0) light7 (CLOCK_50, light_reset, L, R, LEDR[8], LEDR[6], LEDR[7]);
	lightFSM #(0) light8 (CLOCK_50, light_reset, L, R, LEDR[9], LEDR[7], LEDR[8]);
	lightFSM #(0) light9 (CLOCK_50, light_reset, L, R, 1'b0   , LEDR[8], LEDR[9]);
	
	// Victory (boring version)
	logic [6:0] vicLEDs;
	logic some_victory;
	victory vic (CLOCK_50, reset, L, R, LEDR[9], LEDR[1], vicLEDs, some_victory);
	assign light_reset = some_victory | reset;
	assign HEX0 = ~vicLEDs;
	
	// Victory (fancy counters)
	/*logic some_victory;
	logic [3:0] LW, RW;
	victoryCounter vic (CLOCK_50, reset, L, R, LEDR[9], LEDR[1], LW, RW, some_victory);
	assign light_reset = some_victory | reset;
	logic [6:0] L_leds, R_leds;
	seg7 l_seg (LW % 4'd10, L_leds);
	seg7 r_seg (RW % 4'd10, R_leds);
	assign HEX0 = ~R_leds;
	assign HEX5 = ~L_leds;*/
endmodule

module DE1_SoC_testbench();
	logic clk;
	logic [3:0] KEY;
	logic [9:0] LEDR, SW;
	logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;

	DE1_SoC dut (clk, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, KEY, LEDR, SW);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); SW[9] <= 1;
		@(posedge clk); SW[9] <= 0; KEY[3] <= 1; KEY[0] <= 1;
		@(posedge clk); // Wait for reset to propagate
		@(posedge clk); KEY[3] <= 0; // Left one
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk);
		@(posedge clk); KEY[0] <= 0; // Right one
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk);
		@(posedge clk); KEY[0] <= 0; // Right one
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk); KEY[0] <= 0; KEY[3] <= 0; // Stalemate
		@(posedge clk); KEY[0] <= 1; KEY[3] <= 1;
		@(posedge clk); KEY[0] <= 0;
		@(posedge clk); KEY[0] <= 1; KEY[3] <= 0; // Off-by-one stalemate
		@(posedge clk);              KEY[3] <= 1;
		@(posedge clk); KEY[0] <= 0; // Right wins
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk); KEY[0] <= 0;
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk); KEY[0] <= 0;
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk); KEY[0] <= 0;
		@(posedge clk); KEY[0] <= 1;
		@(posedge clk); // Right victory propagation delay
		@(posedge clk); KEY[3] <= 0; // Left wins
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); // Left victory propagation delay
		@(posedge clk);
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
