#include <stdio.h>

int main(char **argv, int argc) {
  int state = 0;
  for (int i = 0; i < 512; i++) {
    state = ((state << 1) & 0x1fe) | (~((state >> 8) ^ (state >> 4)) & 0x1);
    printf("%x\n", state);
  }
}