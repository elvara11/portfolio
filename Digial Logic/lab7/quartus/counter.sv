module counter(clk, reset, in, out);
	input logic clk, reset, in;
	output logic [2:0] out;
	
	logic [2:0] next_state;
	
	always_comb
		if (in) begin
			next_state[0] = ~out[0];
			next_state[1] = out[0] ^ out[1];
			next_state[2] = out[2] ^ (out[0] & out[1]);
		end else
			next_state = out;
			
	always_ff @(posedge clk)
		if (reset)
			out <= 3'b000;
		else
			out <= next_state;
endmodule


module counter_testbench();
	logic clk, reset, in;
	logic [2:0] out;

	counter dut (clk, reset, in, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; in <= 1'b1; // 0
		@(posedge clk); // 1
		@(posedge clk); // 2
		@(posedge clk); // 3
		@(posedge clk); in <= 0; // 4
		@(posedge clk); // 4
		@(posedge clk); // 4
		@(posedge clk); in <= 1; // 4
		@(posedge clk); // 5
		@(posedge clk); // 6
		@(posedge clk); // 7
		@(posedge clk); // 0
		@(posedge clk); // 1
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
