module adder (A, B, out);
	input logic [9:0] A, B;
	output logic [9:0] out;
	logic c1, c2, c3, c4, c5, c6, c7, c8, c9, cout;
	
	fulladd b9 (cout, out[9], c9, A[9], B[9]);
	fulladd b8 (c9  , out[8], c8, A[8], B[8]);
	fulladd b7 (c8  , out[7], c7, A[7], B[7]);
	fulladd b6 (c7  , out[6], c6, A[6], B[6]);
	fulladd b5 (c6  , out[5], c5, A[5], B[5]);
	fulladd b4 (c5  , out[4], c4, A[4], B[4]);
	fulladd b3 (c4  , out[3], c3, A[3], B[3]);
	fulladd b2 (c3  , out[2], c2, A[2], B[2]);
	fulladd b1 (c2  , out[1], c1, A[1], B[1]);
	fulladd b0 (c1  , out[0], 1'b0, A[0], B[0]);
endmodule

module fulladd (cout, s, cin, a, b);
	output logic cout, s;
	input logic cin, a, b;
	always_comb begin
		{cout, s} = cin + a + b;
	end
endmodule

module adder_testbench();
	logic [9:0] A, B, out;
	adder dut (.A, .B, .out);
	// Try all combinations of inputs
	integer i;
	initial begin
		A = 10'b0000000000; B = 10'b0101110001; #10; // 0 + 369 = 369
		A = 10'b0010001110;                     #10; // 142 + 369 = 511
		A = 10'b1010001111;                     #10; // -369 + 369 = 0
		A = 10'b1100000000;                     #10; // 768 + 369 = 113
		A = 10'b0111111111;                     #10; // 511 + 369 = -144
		A = 10'b1010001111; B = 10'b1010001111; #10; // -369 + -369 = 286
	end
endmodule
