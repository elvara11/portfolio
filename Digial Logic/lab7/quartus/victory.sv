module victory (Clock, Reset, L, R, LL, RL, LV, RV);
	input logic Clock, Reset;
	// L - True when left key (KEY[3]) is pressed
	// R - True when right key (KEY[0]) is pressed
	// LL - True when leftmost light is on
	// RL - True when rightmost light is on
	input logic L, R, LL, RL;
	
	// LV/RV - 1 iff the left/right player just won
	output logic LV, RV;
	
	parameter [1:0] NoWinner = 2'b00, LeftWon = 2'b01, RightWon = 2'b10;
	
	logic [2:0] ps, ns;
	
	// ps -> ns
	always_comb
		if (L & ~R & LL) begin // Left wins
			ns = LeftWon;
			LV = 1;
			RV = 0;
		end else if (~L & R & RL) begin // Right wins
			ns = RightWon;
			RV = 1;
			LV = 0;
		end else begin // Nobody wins
			ns = ps;
			RV = 0;
			LV = 0;
		end
	
	// Register
	always_ff @(posedge Clock)
		if (Reset)
			ps <= NoWinner;
		else
			ps <= ns;
endmodule

module victory_testbench();
	logic clk, reset, L, R, LL, RL;
	logic LV, RV;

	victory dut (clk, reset, L, R, LL, RL, LV, RV);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 0; R <= 0; LL <= 0; RL <= 0; // Four cases of no victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0;
		@(posedge clk); RL <= 1; // Right victory
		@(posedge clk); R <= 0; // Three cases of almost-but-not right victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0; // Second right victory
		@(posedge clk); R <= 0; L <= 1; LL <= 1; RL <= 0; // Left victory
		@(posedge clk); L <= 0; // Three cases of almost-but-not left victory
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 0; // Second left victory
		@(posedge clk); L <= 0; LL <= 0; R <= 1; RL <= 1; // Right victory after left victory
		@(posedge clk); reset <= 1; R <= 0; RL <= 0; // Test no winner -> left victory
		@(posedge clk); reset <= 0; L <= 1; LL <= 1;
		@(posedge clk); L <= 0; LL <= 0; // Wait for victory propagation
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
