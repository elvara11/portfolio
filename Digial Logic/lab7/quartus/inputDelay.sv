module inputDelay(Clock, Reset, L, R, Reset_Out, L_Out, R_Out);
	// Delay Reset, L, and R by one cycle to reduce chance of metastability
	input logic Clock, Reset, L, R;
	output logic Reset_Out, L_Out, R_Out;
	
	always_ff @(posedge Clock) begin
		Reset_Out <= Reset;
		L_Out <= L;
		R_Out <= R;
	end
endmodule

module inputDelay_testbench();
	logic clk, reset, L, R;
	logic reset_out, l_out, r_out;

	inputDelay dut (clk, reset, L, R, reset_out, l_out, r_out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 0; L <= 0; R <= 0;
		@(posedge clk);                     R <= 1;
		@(posedge clk);             L <= 1; R <= 0;
		@(posedge clk);                     R <= 1;
		@(posedge clk); reset <= 1; L <= 0; R <= 0;
		@(posedge clk);                     R <= 1;
		@(posedge clk);             L <= 1; R <= 0;
		@(posedge clk);                     R <= 1;
		@(posedge clk);
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
