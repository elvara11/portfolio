module DE1_SoC (CLOCK_50, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, KEY, LEDR, SW);
	input  logic		CLOCK_50; // 50MHz clock.
	output logic [6:0]	HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	output logic [9:0]	LEDR;
	input  logic [3:0]	KEY;		 // True when not pressed, False when pressed
	input  logic [9:0]	SW;
	
	// Defaults
	assign LEDR[0] = 0;
	assign HEX1 = 7'b1111111;
	assign HEX2 = 7'b1111111;
	assign HEX3 = 7'b1111111;
	assign HEX4 = 7'b1111111;
	
	// Clock setup
	//parameter whichClock = 15;
	//logic [31:0] clks;
	//clock_divider cdiv (.clock(CLOCK_50), .divided_clocks(clks));
	logic clk;
	//assign clk = clks[whichClock];
	assign clk = CLOCK_50;
	
	// Input handling
	logic reset, L_Raw, R_Ignored;
	inputDelay delay(clk, SW[9], KEY[3], KEY[0], reset, L_Raw, R_Ignored);
	logic L, R;
	pressed l_pressed(clk, reset, L_Raw, L);
	
	logic [8:0] lfsr_out;
	lfsr lfsr_r (clk, reset, lfsr_out);
	logic [9:0] r_state;
	adder add_r ({1'b0, lfsr_out}, {1'b0, SW[8:0]}, r_state);
	logic R_Raw;
	assign R_Raw = r_state[9];	
	pressed r_pressed (clk, reset, R_Raw, R);
	
	// Light FSMs
	logic light_reset;
	lightFSM #(0) light1 (clk, light_reset, L, R, LEDR[2], 1'b0   , LEDR[1]);
	lightFSM #(0) light2 (clk, light_reset, L, R, LEDR[3], LEDR[1], LEDR[2]);
	lightFSM #(0) light3 (clk, light_reset, L, R, LEDR[4], LEDR[2], LEDR[3]);
	lightFSM #(0) light4 (clk, light_reset, L, R, LEDR[5], LEDR[3], LEDR[4]);
	lightFSM #(1) light5 (clk, light_reset, L, R, LEDR[6], LEDR[4], LEDR[5]);
	lightFSM #(0) light6 (clk, light_reset, L, R, LEDR[7], LEDR[5], LEDR[6]);
	lightFSM #(0) light7 (clk, light_reset, L, R, LEDR[8], LEDR[6], LEDR[7]);
	lightFSM #(0) light8 (clk, light_reset, L, R, LEDR[9], LEDR[7], LEDR[8]);
	lightFSM #(0) light9 (clk, light_reset, L, R, 1'b0   , LEDR[8], LEDR[9]);
	
	// Victory (boring version)
	logic [6:0] vicLEDs;
	logic LV, RV;
	victory vic (clk, reset, L, R, LEDR[9], LEDR[1], LV, RV);
	assign light_reset = LV | RV | reset;
	
	logic [2:0] LCount, RCount;
	counter lcounter (clk, reset, LV, LCount);
	counter rcounter (clk, reset, RV, RCount);
	
	logic [6:0] LLEDs, RLEDs;
	seg7 lseg7 ({1'b0, LCount}, LLEDs);
	seg7 rseg7 ({1'b0, RCount}, RLEDs);
	
	assign HEX5 = ~LLEDs;
	assign HEX0 = ~RLEDs;
endmodule

// divided_clocks[0] = 25MHz, [1] = 12.5Mhz, ... [23] = 3Hz, [24] = 1.5Hz, [25] = 0.75Hz, ...
// HARDWARE ONLY - not to be used in simulation
/*module clock_divider (clock, divided_clocks);
	input  logic        clock;
	output logic [31:0] divided_clocks;

	initial
		divided_clocks = 0;

	always_ff @(posedge clock)
		divided_clocks <= divided_clocks + 1;
        
endmodule*/

module DE1_SoC_testbench();
	logic clk;
	logic [3:0] KEY;
	logic [9:0] LEDR, SW;
	logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;

	DE1_SoC dut (clk, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, KEY, LEDR, SW);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); SW[9] <= 1; SW[8:0] <= 9'b100000000;
		@(posedge clk); SW[9] <= 0; KEY[3] <= 1;
		@(posedge clk); // Wait for reset to propagate
		@(posedge clk); KEY[3] <= 0; // Left wins
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); // Right wins
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);
		@(posedge clk); KEY[3] <= 0; // Left wins again, but big struggle
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk); KEY[3] <= 0;
		@(posedge clk); KEY[3] <= 1;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
