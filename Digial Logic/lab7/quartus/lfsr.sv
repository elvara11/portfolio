module lfsr(clk, reset, out);
	input logic clk, reset;
	output logic [8:0] out;
	
	always_ff @(posedge clk)
		if (reset)
			out <= 9'b000000000;
		else
			out <= {out[7:0], ~(out[8] ^ out[4])};
endmodule

module lfsr_testbench();
	logic clk, reset;
	logic [8:0] out;

	lsfr dut (clk, reset, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);

		$stop; // End the simulation
	end
endmodule
