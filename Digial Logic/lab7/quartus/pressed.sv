module pressed(Clock, Reset, raw, out);
	// raw - False when pressed, true when unpressed
	input logic Clock, Reset, raw;
	// True iff the last cycle was pressed and this cycle is unpressed
	output logic out;
	
	logic last;
	
	assign out = last & ~raw;
	
	always_ff @(posedge Clock)
		if (Reset)
			last <= 0;
		else
			last <= raw;
endmodule


module pressed_testbench();
	logic clk, reset, raw;
	logic out;

	pressed dut (clk, reset, raw, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; raw <= 1; // Unpressed
		@(posedge clk); // Leave unpressed
		@(posedge clk); raw <= 0; // Press & hold
		@(posedge clk);
		@(posedge clk); raw <= 1; // Release & wait
		@(posedge clk);
		@(posedge clk); raw <= 0; // Press quickly
		@(posedge clk); raw <= 1;
		@(posedge clk);
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
