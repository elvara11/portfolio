module lfsr(clk, reset, enable, out);
	input logic clk, reset, enable;
	output logic [8:0] out;
	
	logic [8:0] ps;
	
	always_ff @(posedge clk)
		if (reset) begin
			ps <= '0;
			out <= '0;
		end else if (enable) begin
			ps <= {ps[7:0], ~(ps[8] ^ ps[4])};
			out <= ps;
		end else begin
			// Keep the lfsr running, but don't copy to the output
			ps <= {ps[7:0], ~(ps[8] ^ ps[4])};
			out <= out;
		end
endmodule

module lfsr_testbench();
	logic clk, reset, enable;
	logic [8:0] out;

	lfsr dut (clk, reset, enable, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; enable <= 1;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); enable <= 1;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);

		$stop; // End the simulation
	end
endmodule
