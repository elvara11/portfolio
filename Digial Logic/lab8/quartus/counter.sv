module counter #(N = 8, parameter logic [N - 1:0] init = '0) (clk, reset, in, out);
	input logic clk, reset, in;
	output logic [N - 1:0] out;
			
	always_ff @(posedge clk)
		if (reset)
			out <= init;
		else if (in)
			out <= out + 1;
		else
			out <= out;
endmodule


module counter_testbench();
	logic clk, reset, in;
	logic [2:0] out;

	counter #(3, '0) dut (clk, reset, in, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; in <= 1'b1; // 0
		@(posedge clk); // 1
		@(posedge clk); // 2
		@(posedge clk); // 3
		@(posedge clk); in <= 0; // 4
		@(posedge clk); // 4
		@(posedge clk); // 4
		@(posedge clk); in <= 1; // 4
		@(posedge clk); // 5
		@(posedge clk); // 6
		@(posedge clk); // 7
		@(posedge clk); // 0
		@(posedge clk); // 1
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
