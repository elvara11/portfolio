onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lightFSM_testbench/clk
add wave -noupdate /lightFSM_testbench/reset
add wave -noupdate /lightFSM_testbench/enable
add wave -noupdate /lightFSM_testbench/apple
add wave -noupdate -radix decimal /lightFSM_testbench/length
add wave -noupdate /lightFSM_testbench/direction
add wave -noupdate /lightFSM_testbench/neighborhood
add wave -noupdate /lightFSM_testbench/state
add wave -noupdate /lightFSM_testbench/defeat
add wave -noupdate /lightFSM_testbench/eaten
add wave -noupdate -radix decimal /lightFSM_testbench/dut/tail_count/out
add wave -noupdate -radix decimal -childformat {{{/lightFSM_testbench/dut/tail_left[7]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[6]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[5]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[4]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[3]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[2]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[1]} -radix decimal} {{/lightFSM_testbench/dut/tail_left[0]} -radix decimal}} -subitemconfig {{/lightFSM_testbench/dut/tail_left[7]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[6]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[5]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[4]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[3]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[2]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[1]} {-height 15 -radix decimal} {/lightFSM_testbench/dut/tail_left[0]} {-height 15 -radix decimal}} /lightFSM_testbench/dut/tail_left
add wave -noupdate /lightFSM_testbench/dut/end_of_tail
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1296 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 262
configure wave -valuecolwidth 91
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 50
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4864 ps}
