module lightFSM #(parameter logic [3:0] walls, parameter logic [1:0] init)
                 (clk, reset, enable, length, direction, neighborhood, state, defeat);
	input logic clk, reset, enable;
	input logic [7:0] length;
	input logic [1:0] direction;
	input logic [3:0] neighborhood;
	
	output logic [1:0] state;
	output logic defeat;
	
	parameter logic [1:0] LEFT = 2'b00, UP = 2'b01, DOWN = 2'b10, RIGHT = 2'b11;
	parameter logic [2:0] SNAKE_HEAD = 2'b00, SNAKE_TAIL = 2'b01, EMPTY = 2'b10;
	
	logic [1:0] ps, ns;
	
	logic count_in, count_reset;
	logic [7:0] count_out;
	counter #(8, '0) tail_count(clk, count_reset, count_in, count_out);
	logic end_of_tail;
	
	// Decode direction to one hot {l, u, d, r}
	// Flip direction
	// SnakeHead: Either moving into wall or becomes tail
	
	// Decoder
	logic [3:0] one_hot_dir, flipped_ohd;
	always_comb
		case (direction)
			LEFT: begin one_hot_dir = 4'b1000; flipped_ohd = 4'b0001; end
			UP: begin one_hot_dir = 4'b0100; flipped_ohd = 4'b0010; end
			DOWN: begin one_hot_dir = 4'b0010; flipped_ohd = 4'b0100; end
			RIGHT: begin one_hot_dir = 4'b0001; flipped_ohd = 4'b1000; end
		endcase
	
	logic next_defeat;
	always_comb begin
		case (ps)
			SNAKE_HEAD: begin
				if ((one_hot_dir & walls) == 4'b0000) begin
					// We are the head but we didn't hit any walls
					next_defeat = 0;
					ns = SNAKE_TAIL;
				end else begin
					// Head and hit a wall
					next_defeat = 1;
					ns = EMPTY;
				end
			end
			SNAKE_TAIL: begin
				if ((neighborhood & flipped_ohd) != 4'b0000) begin
					// Tail and hit by head
					next_defeat = 1;
					ns = EMPTY;
				end else if (end_of_tail) begin
					// Tail end
					next_defeat = 0;
					ns = EMPTY;
				end else begin
					// Normal tail
					next_defeat = 0;
					ns = SNAKE_TAIL;
				end
			end
			EMPTY: begin
				if ((neighborhood & flipped_ohd) != 4'b0000) begin
					// Head moving in
					next_defeat = 0;
					ns = SNAKE_HEAD;
				end else begin
					next_defeat = 0;
					ns = EMPTY;
				end
			end
			default: begin
				next_defeat = 1'bX;
				ns = 2'bXX;
			end
		endcase
	end
	
	assign state = ps;
	assign count_in = (ps == SNAKE_HEAD | ps == SNAKE_TAIL) & enable;
	assign count_reset = reset | (end_of_tail & enable);
	assign end_of_tail = (count_out == length);
	
	always_ff @(posedge clk)
		if (reset) begin
			ps <= init;
			defeat <= 0;
		end else if (enable) begin
			ps <= ns;
			defeat <= next_defeat;
		end else begin
			ps <= ps;
			defeat <= defeat;
		end
endmodule

module lightFSM_testbench();
	logic clk, reset, enable;
	logic [7:0] length;
	logic [1:0] direction;
	logic [3:0] neighborhood;
	
	logic [1:0] state;
	logic defeat;

	lightFSM #(4'b0100, 2'b10) dut (clk, reset, enable, length, direction, neighborhood, state, defeat);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1; enable <= 0;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); reset <= 0; length <= 8'b00000011; direction <= 2'b00; neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); direction <= 2'b00; neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); 
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); direction <= 2'b01; neighborhood <= 4'b0010;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); direction <= 2'b01; neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); reset <= 1;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); reset <= 0; direction <= 2'b00; neighborhood <= 4'b0001;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); length <= 8'b00000100; neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b1000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); direction <= 2'b11; neighborhood <= 4'b1000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b0001;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b0000;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); neighborhood <= 4'b0010; direction <= 2'b01;
		@(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		@(posedge clk); @(posedge clk); @(posedge clk); enable <= 1; @(posedge clk); enable <= 0;
		$stop; // End the simulation
	end
endmodule

