onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /snake_testbench/clk
add wave -noupdate /snake_testbench/reset
add wave -noupdate /snake_testbench/KEY
add wave -noupdate -expand /snake_testbench/RedPixels
add wave -noupdate -expand /snake_testbench/GrnPixels
add wave -noupdate /snake_testbench/HEX1
add wave -noupdate /snake_testbench/HEX0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 209
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 50
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {6846 ps} {12430 ps}
