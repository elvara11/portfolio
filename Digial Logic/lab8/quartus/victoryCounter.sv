module victoryCounter (Clock, Reset, L, R, LL, RL, LW, RW, SomeVictory);
	input logic Clock, Reset;
	// L - True when left key (KEY[3]) is pressed
	// R - True when right key (KEY[0]) is pressed
	// LL - True when leftmost light is on
	// RL - True when rightmost light is on
	input logic L, R, LL, RL;
	
	// LW - Number of wins left has
	// RW - Number of wins right has
	output logic [3:0] LW, RW;
	
	// SomeVictory - True iff either player just won
	output logic SomeVictory;
	
	// Next state
	logic [3:0] Next_LW, Next_RW;
	logic Next_SomeVictory;
	
	always_comb
		if (L & ~R & LL) begin // Left wins
			Next_LW = LW + 4'b0001;
			Next_RW = RW;
			Next_SomeVictory = 1;
		end else if (~L & R & RL) begin // Right wins
			Next_LW = LW;
			Next_RW = RW + 4'b0001;
			Next_SomeVictory = 1;
		end else begin // Nobody wins
			Next_LW = LW;
			Next_RW = RW;
			Next_SomeVictory = 0;
		end
	
	always_ff @(posedge Clock)
		if (Reset) begin
			LW <= 4'b0000;
			RW <= 4'b0000;
			SomeVictory <= 0;
		end else begin
			LW <= Next_LW;
			RW <= Next_RW;
			SomeVictory <= Next_SomeVictory;
		end
endmodule

module victoryCounter_testbench();
	logic clk, reset, L, R, LL, RL;
	logic [3:0] LW, RW;
	logic SomeVictory;

	victory dut (clk, reset, L, R, LL, RL, LW, RW, SomeVictory);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 0; R <= 0; LL <= 0; RL <= 0; // Four cases of no victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0;
		@(posedge clk); RL <= 1; // Right victory
		@(posedge clk); R <= 0; // Three cases of almost-but-not right victory
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0; // Second right victory
		@(posedge clk); R <= 0; L <= 1; LL <= 1; RL <= 0; // Left victory
		@(posedge clk); L <= 0; // Three cases of almost-but-not left victory
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 1;
		@(posedge clk); R <= 0; // Second left victory
		@(posedge clk); L <= 0; LL <= 0; // One extra cycle for the victory to propagate
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
