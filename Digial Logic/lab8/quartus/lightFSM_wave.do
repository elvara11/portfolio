onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lightFSM_testbench/clk
add wave -noupdate /lightFSM_testbench/reset
add wave -noupdate /lightFSM_testbench/enable
add wave -noupdate -radix decimal /lightFSM_testbench/length
add wave -noupdate /lightFSM_testbench/direction
add wave -noupdate /lightFSM_testbench/neighborhood
add wave -noupdate /lightFSM_testbench/state
add wave -noupdate /lightFSM_testbench/defeat
TreeUpdate [SetDefaultTree]
WaveRestoreCursors
quietly wave cursor active 0
configure wave -namecolwidth 262
configure wave -valuecolwidth 91
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 50
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {9728 ps}
