module snake(clk, RST, RedPixels, GrnPixels, LEDR, KEY, SW, HEX1, HEX0);
	input logic clk, RST;
   input logic [3:0]  KEY;
	input logic [9:0] SW;
	output logic [6:0] HEX1, HEX0;
	output logic [9:0]  LEDR;
	output logic [15:0][15:0]RedPixels;
   output logic [15:0][15:0]GrnPixels;

	assign LEDR = '0;
	assign RedPixels[15:8] = {'0, '0, '0, '0, '0, '0, '0, '0};
	assign GrnPixels[15:8] = {'0, '0, '0, '0, '0, '0, '0, '0};
	
	// Delay the input
	 logic L, U, D, R;
	 inputDelay #(4) delay(clk, {~KEY}, {L, U, D, R});
	 //assign LEDR[3:0] = {L, U, D, R};
	 
	 logic game_reset;
	 
	 // LFSR for the apples
	 logic [8:0] lfsr_out;
	 logic lfsr_enable;
	 lfsr apple_random(clk, RST, lfsr_enable, lfsr_out);
	 logic [3:0] apple_x, apple_y;
	 assign apple_x = lfsr_out[8:6];
	 assign apple_y = lfsr_out[5:3];
	 
	 logic [7:0] length;
	 logic length_enable;
	 counter #(8, 8'b00000010) length_count(clk, game_reset, length_enable, length);
	 
	 logic [1:0] dir;
	 direction direc(clk, RST, L, U, D, R, dir);
	 //assign LEDR[9:8] = dir;
	 
	 logic [7:0][7:0][1:0] states;
	 logic [7:0][7:0] eaten;
	 logic [7:0][7:0] defeat;
	 
	 // 12 Hz enable signal
	 logic light_enable;
	 logic [11:0] light_enable_count;
	 counter #(12, '0) enable_count(clk, RST | light_enable, 1'b1, light_enable_count);
	 assign light_enable = light_enable_count == {SW[8:0], 3'b111};
	 
	 genvar i, j;
	 generate
		for (i = 0; i < 8; i = i + 1) begin : light_rows
			assign RedPixels[i][15:8] = '0;
			assign GrnPixels[i][15:8] = '0;
			for (j = 0; j < 8; j = j + 1) begin: light_col
				logic nl, nu, nd, nr, apple;
				
				if (i == 0)
					assign nu = 0;
				else
					assign nu = states[i - 1][j] == 2'b00;
				
				if (j == 0)
					assign nr = 0;
				else
					assign nr = states[i][j - 1] == 2'b00;
				
				if (j == 7)
					assign nl = 0;
				else
					assign nl = states[i][j + 1] == 2'b00;
				
				if (i == 7)
					assign nd = 0;
				else
					assign nd = states[i + 1][j] == 2'b00;
				
				assign apple = apple_x == i & apple_y == j;
				parameter walls = {j == 7, i == 0, i == 7, j == 0};
				
				if (i == 0 & j == 7)
					lightFSM #(walls, 2'b00) fsm(clk, game_reset, light_enable, length, dir, {nl, nu, nd, nr},
								                    states[i][j], defeat[i][j]);
				else
					lightFSM #(walls, 2'b10) fsm(clk, game_reset, light_enable, length, dir, {nl, nu, nd, nr},
								                    states[i][j], defeat[i][j]);
				
				assign eaten[i][j] = (states[i][j] == 2'b00) & apple;
				assign RedPixels[i][j] = apple;
				assign GrnPixels[i][j] = states[i][j] != 2'b10;
			end
		end
	 endgenerate
	 
	 assign length_enable = |eaten;
	 assign lfsr_enable = |eaten;
	 assign game_reset = (|defeat) | RST;
	 
	 logic [3:0] digit1, digit0;
	 score score_sep(clk, game_reset, length_enable, digit1, digit0);
	 
	 logic [6:0] disp1, disp0;
	 seg7 d1_seg(digit1, disp1);
	 seg7 d0_seg(digit0, disp0);
	 
	 assign HEX0 = ~disp0;
	 assign HEX1 = ~disp1;
endmodule

module snake_testbench();
	logic clk, reset;
   logic [3:0] KEY;
	logic [9:0] LEDR, SW;
	logic [6:0] HEX0, HEX1;
	logic [15:0][15:0] RedPixels, GrnPixels;

	snake dut (clk, reset, RedPixels, GrnPixels, LEDR, KEY, SW, HEX1, HEX0);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; KEY <= 4'b1110; SW[8:0] <= '0;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b1101;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b0111;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b1011;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b1110;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); 
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b1011;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk); KEY <= 4'b1101;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		$stop; // End the simulation
	end
endmodule
