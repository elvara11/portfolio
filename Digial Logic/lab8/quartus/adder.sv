module adder #(parameter N=8) (S, sub, A, B);
	output logic [N-1:0] S; // sum output bus
	
	input logic sub; // subtract signal
	input logic [N-1:0] A, B; // input busses
	
	logic [N:0] C; // carry signals between modules
	
	genvar i;
	generate
		for(i=0; i<N; i=i+1) begin : adders
			fulladd fadd (C[i+1],S[i],C[i],A[i],sub^B[i]);
		end
	endgenerate
	
	assign C[0] = sub;
endmodule

module fulladd (cout, s, cin, a, b);
	output logic cout, s;
	input logic cin, a, b;
	always_comb begin
		{cout, s} = cin + a + b;
	end
endmodule

module adder_testbench();
	logic [9:0] A, B, out;
	adder #(10) dut (out, 0, A, B);
	// Try all combinations of inputs
	integer i;
	initial begin
		A = 10'b0000000000; B = 10'b0101110001; #10; // 0 + 369 = 369
		A = 10'b0010001110;                     #10; // 142 + 369 = 511
		A = 10'b1010001111;                     #10; // -369 + 369 = 0
		A = 10'b1100000000;                     #10; // 768 + 369 = 113
		A = 10'b0111111111;                     #10; // 511 + 369 = -144
		A = 10'b1010001111; B = 10'b1010001111; #10; // -369 + -369 = 286
	end
endmodule
