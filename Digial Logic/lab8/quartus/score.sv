module score(clk, reset, enable, digit1, digit0);
	input logic clk, reset, enable;
	output logic [3:0] digit0, digit1;

	logic [7:0] count;
	counter #(8, '0) score_count(clk, reset, enable, count);
	
	assign digit0 = count % 10;
	assign digit1 = ((count - digit0) / 10) % 10;
endmodule

module score_testbench;
	logic clk, reset, enable;
	logic [3:0] digit1, digit0;

	score dut (clk, reset, enable, digit1, digit0);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; enable <= 0;
		@(posedge clk);
		@(posedge clk); enable <= 1;
		@(posedge clk); enable <= 0;
		@(posedge clk);
		@(posedge clk); enable <= 1;
		@(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); enable <= 0;
		@(posedge clk);
		@(posedge clk); enable <= 1;
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);
		@(posedge clk); @(posedge clk); @(posedge clk); @(posedge clk);

		$stop; // End the simulation
	end
endmodule
