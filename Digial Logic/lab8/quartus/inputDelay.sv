module inputDelay #(N = 1) (clk, ins, outs);
	input logic clk;
	input logic [N - 1: 0] ins;
	output logic [N - 1: 0] outs;
	
	always_ff @(posedge clk)
		outs <= ins;
endmodule

module inputDelay_testbench();
	logic clk, A, B, C;
	logic A_out, B_out, C_out;

	inputDelay #(3) dut (clk, {A, B, C}, {A_out, B_out, C_out});
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); A <= 0; B <= 0; C <= 0;
		@(posedge clk);                 C <= 1;
		@(posedge clk);         B <= 1; C <= 0;
		@(posedge clk);                 C <= 1;
		@(posedge clk); A <= 1; B <= 0; C <= 0;
		@(posedge clk);                 C <= 1;
		@(posedge clk);         B <= 1; C <= 0;
		@(posedge clk);                 C <= 1;
		@(posedge clk);
		@(posedge clk);

		$stop; // End the simulation
	end
endmodule
