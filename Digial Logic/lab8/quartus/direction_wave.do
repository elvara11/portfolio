onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /direction_testbench/clk
add wave -noupdate /direction_testbench/reset
add wave -noupdate /direction_testbench/L
add wave -noupdate /direction_testbench/U
add wave -noupdate /direction_testbench/D
add wave -noupdate /direction_testbench/R
add wave -noupdate /direction_testbench/out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2413 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 183
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 50
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {3905 ps}
