module direction(clk, reset, L, U, D, R, dir);
	input logic clk, reset, L, U, D, R;
	output logic [1:0] dir;
	
	parameter logic [1:0] LEFT = 2'b00, UP = 2'b01, DOWN = 2'b10, RIGHT = 2'b11;
	
	logic [1:0] ns;
	logic [3:0] combined;
	assign combined = {L, U, D, R};
	
	always_comb
		case (combined)
			4'b1000: ns = LEFT;
			4'b0100: ns = UP;
			4'b0010: ns = DOWN;
			4'b0001: ns = RIGHT;
			default: ns = dir; // No change on no input or multiple inputs
		endcase
	
	always_ff @(posedge clk)
		if (reset)
			dir <= RIGHT; // Default direction
		else
			dir <= ns;
endmodule

module direction_testbench();
	logic clk, reset, L, U, D, R;
	logic [1:0] out;

	direction dut (clk, reset, L, U, D, R, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; L <= 0; U <= 0; D <= 0; R <= 0;
		@(posedge clk); L <= 1;
		@(posedge clk); L <= 0; U <= 1;
		@(posedge clk); U <= 0; D <= 1;
		@(posedge clk); D <= 0; R <= 1;
		@(posedge clk); R <= 0; L <= 1;
		@(posedge clk); L <= 0; D <= 1;
		@(posedge clk); D <= 0; U <= 1;
		@(posedge clk); U <= 0; R <= 1;
		@(posedge clk); R <= 0;
		@(posedge clk);
		@(posedge clk);
		@(posedge clk); L <= 1;
		@(posedge clk); L <= 0;
		@(posedge clk); 
		@(posedge clk); L <= 1; U <= 1;
		@(posedge clk); D <= 1;
		@(posedge clk); R <= 1;
		@(posedge clk); L <= 0;
		@(posedge clk); U <= 0;
		@(posedge clk); D <= 0;
		@(posedge clk); 
		@(posedge clk); 
		@(posedge clk); R <= 0;
		@(posedge clk); 
		@(posedge clk); 

		$stop; // End the simulation
	end
endmodule
