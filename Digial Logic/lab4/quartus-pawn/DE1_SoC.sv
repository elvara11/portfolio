// Top-level module that defines the I/Os for the DE-1 SoC board
module DE1_SoC (HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, KEY, LEDR, SW);
	output logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	output logic [9:0] LEDR;
	input logic [3:0] KEY;
	input logic [9:0] SW;
	
	logic [6:0] leds5, leds4, leds3, leds2, leds1, leds0;
	assign HEX0 = ~leds0;
	assign HEX1 = ~leds1;
	assign HEX2 = ~leds2;
	assign HEX3 = ~leds3;
	assign HEX4 = ~leds4;
	assign HEX5 = ~leds5;
	
	upc_seg7 disp(SW[3], SW[2], SW[1], leds5, leds4, leds3, leds2, leds1, leds0);
	lab3 disc_stol(SW[3], SW[2], SW[1], SW[0], LEDR[1], LEDR[0]);
endmodule

module DE1_SoC_testbench();
	logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	logic [9:0] LEDR;
	logic [3:0] KEY;
	logic [9:0] SW;
	DE1_SoC dut (.HEX0, .HEX1, .HEX2, .HEX3, .HEX4, .HEX5, .KEY, .LEDR, .SW);
	// Try all combinations of inputs
	integer i;
	initial begin
		SW[9:4] = 1'b000000;
		for(i = 0; i < 16; i++) begin
			SW[3:0] = i; #10;
		end
	end
endmodule
