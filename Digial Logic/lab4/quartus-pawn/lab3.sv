module lab3 (u, p, c, m, discount, stolen);
	input logic u, p, c, m;
	output logic discount, stolen;

	logic UC, PCMNor, UNot, UNotPMNor;
	// Discounted logic
	and (UC, u, c);
	or (discount, UC, p);
	// Stolen logic
	nor (PCMNor, p, c, m);
	not (UNot, u);
	nor (UNotPMNor, UNot, p, m);
	or (stolen, UNotPMNor, PCMNor);
endmodule
