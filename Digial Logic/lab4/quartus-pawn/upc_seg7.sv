module upc_seg7 (u, p, c, leds5, leds4, leds3, leds2, leds1, leds0);
	input logic u, p, c;
	output logic [6:0] leds5, leds4, leds3, leds2, leds1, leds0;
	
	always_comb
	case ({u, p, c})
		// Light: 6543210
		
		// Watch
		3'b000: begin
						leds5 = 7'b0000000; // Blank
						leds4 = 7'b0101010; // W
						leds3 = 7'b1110111; // A
						leds2 = 7'b1111000; // T
						leds1 = 7'b0111001; // C
						leds0 = 7'b1110110; // H
				  end
				  
		// Gloves
		3'b001: begin
						leds5 = 7'b0111101; // G
						leds4 = 7'b0111000; // L
						leds3 = 7'b0111111; // O
						leds2 = 7'b1100010; // V
						leds1 = 7'b1111001; // E
						leds0 = 7'b1101101; // S
				  end
		
		// Tote
		3'b011: begin
						leds5 = 7'b0000000; // Blank
						leds4 = 7'b1111000; // T
						leds3 = 7'b0111111; // O
						leds2 = 7'b1111000; // T
						leds1 = 7'b1111001; // E
						leds0 = 7'b0000000; // Blank
				  end
		
		// Rug
		3'b100: begin
						leds5 = 7'b0000000; // Blank
						leds4 = 7'b0000000; // Blank
						leds3 = 7'b1010000; // R
						leds2 = 7'b0111110; // U
						leds1 = 7'b0111101; // G
						leds0 = 7'b0000000; // Blank
				  end
		
		// Teapot
		3'b101: begin
						leds5 = 7'b1111000; // T
						leds4 = 7'b1111001; // E
						leds3 = 7'b1110111; // A
						leds2 = 7'b1110011; // P
						leds1 = 7'b0111111; // O
						leds0 = 7'b1111000; // T
				  end
		
		// Mug
		3'b110: begin
						leds5 = 7'b0000000; // Blank
						leds4 = 7'b0000000; // Blank
						leds3 = 7'b0010101; // M
						leds2 = 7'b0111110; // U
						leds1 = 7'b0111101; // G
						leds0 = 7'b0000000; // Blank
				  end
				  
		default: begin
						leds5 = 7'bX;
						leds4 = 7'bX;
						leds3 = 7'bX;
						leds2 = 7'bX;
						leds1 = 7'bX;
						leds0 = 7'bX;
					end
	endcase
endmodule
