module runwayFSM (clk, reset, w, out);
	input  logic clk, reset;
	input logic [1:0] w;
	output logic [2:0] out;

	logic [1:0] ps; // Present State
	logic [1:0] ns; // Next State

	// State encoding
	parameter [1:0] A = 2'b00, B = 2'b01, C = 2'b10;//, D = 2'b11;
	parameter [1:0] CLM = 2'b00, R2L = 2'b01, L2R = 2'b10;
	parameter [2:0] CLM_P0 = 3'b101, CLM_P1 = 3'b010,
	                R2L_P0 = 3'b001, R2L_P1 = 3'b010, R2L_P2 = 3'b100,
						 L2R_P0 = 3'b100, L2R_P1 = 3'b010, L2R_P2 = 3'b001; 

	// Next State logic
	always_comb
		if (ps == A) begin
			ns = B;
			case (w)
				CLM: out = CLM_P0;
				R2L: out = R2L_P0;
				L2R: out = L2R_P0;
				default: out = 3'bxxx;
			endcase
		end else if (ps == B && w != CLM) begin
			ns = C;
			out = R2L_P1;
		end else begin
			ns = A;
			case (w)
				CLM: out = CLM_P1;
				R2L: out = R2L_P2;
				L2R: out = L2R_P2;
				default: out = 3'bxxx;
			endcase
		end

	// DFFs
	always_ff @(posedge clk)
		if (reset)
			ps <= A;
		else
			ps <= ns;

endmodule


module runwayFSM_testbench();
	logic clk, reset;
	logic [1:0] w;
	logic [2:0] out;

	runwayFSM dut (clk, reset, w, out);
    
	// Set up the clock
	parameter CLOCK_PERIOD=100;
	initial begin
    	clk <= 0;
		forever #(CLOCK_PERIOD/2) clk <= ~clk;
	end

	// Set up the inputs to the design. Each line is a clock cycle.
	initial begin
		@(posedge clk); reset <= 1;
		@(posedge clk); reset <= 0; w <= 2'b00; // A -> B CLM_P0
		@(posedge clk);             w <= 2'b00; // B -> A CLM_P1
		
		@(posedge clk);             w <= 2'b01; // A -> B R2L_P0
		@(posedge clk);             w <= 2'b01; // B -> C R2L_P1
		@(posedge clk);             w <= 2'b01; // C -> A R2L_P2
		
		@(posedge clk);             w <= 2'b10; // A -> B L2R_P0
		@(posedge clk);             w <= 2'b10; // B -> C L2R_P1
		@(posedge clk);             w <= 2'b10; // C -> A L2R_P2
		
		@(posedge clk);             w <= 2'b10; // A -> B L2R_P0
		@(posedge clk);             w <= 2'b10; // B -> C L2R_P1
		@(posedge clk);             w <= 2'b00; // C -> A CLM_P1

		@(posedge clk); $stop; // End the simulation
	end
endmodule
